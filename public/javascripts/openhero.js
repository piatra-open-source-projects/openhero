let powers = []; // Global variable to store the powers data

let cookieData = {
  color: "#ff8c00",
  save: true,
  showAdmonition: false,
  num_advantage: 0,
  num_disadvantage: 0
};

const green_dice =
  '<svg class="adv" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.47 6.62L12.57 2.18C12.41 2.06 12.21 2 12 2S11.59 2.06 11.43 2.18L3.53 6.62C3.21 6.79 3 7.12 3 7.5V16.5C3 16.88 3.21 17.21 3.53 17.38L11.43 21.82C11.59 21.94 11.79 22 12 22S12.41 21.94 12.57 21.82L20.47 17.38C20.79 17.21 21 16.88 21 16.5V7.5C21 7.12 20.79 6.79 20.47 6.62M11.45 15.96L6.31 15.93V14.91C6.31 14.91 9.74 11.58 9.75 10.57C9.75 9.33 8.73 9.46 8.73 9.46S7.75 9.5 7.64 10.71L6.14 10.76C6.14 10.76 6.18 8.26 8.83 8.26C11.2 8.26 11.23 10.04 11.23 10.5C11.23 12.18 8.15 14.77 8.15 14.77L11.45 14.76V15.96M17.5 13.5C17.5 14.9 16.35 16.05 14.93 16.05C13.5 16.05 12.36 14.9 12.36 13.5V10.84C12.36 9.42 13.5 8.27 14.93 8.27S17.5 9.42 17.5 10.84V13.5M16 10.77V13.53C16 14.12 15.5 14.6 14.92 14.6C14.34 14.6 13.86 14.12 13.86 13.53V10.77C13.86 10.18 14.34 9.71 14.92 9.71C15.5 9.71 16 10.18 16 10.77Z" /></svg>';
const red_dice =
  '<svg class="disadv" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.47 6.62L12.57 2.18C12.41 2.06 12.21 2 12 2S11.59 2.06 11.43 2.18L3.53 6.62C3.21 6.79 3 7.12 3 7.5V16.5C3 16.88 3.21 17.21 3.53 17.38L11.43 21.82C11.59 21.94 11.79 22 12 22S12.41 21.94 12.57 21.82L20.47 17.38C20.79 17.21 21 16.88 21 16.5V7.5C21 7.12 20.79 6.79 20.47 6.62M11.45 15.96L6.31 15.93V14.91C6.31 14.91 9.74 11.58 9.75 10.57C9.75 9.33 8.73 9.46 8.73 9.46S7.75 9.5 7.64 10.71L6.14 10.76C6.14 10.76 6.18 8.26 8.83 8.26C11.2 8.26 11.23 10.04 11.23 10.5C11.23 12.18 8.15 14.77 8.15 14.77L11.45 14.76V15.96M17.5 13.5C17.5 14.9 16.35 16.05 14.93 16.05C13.5 16.05 12.36 14.9 12.36 13.5V10.84C12.36 9.42 13.5 8.27 14.93 8.27S17.5 9.42 17.5 10.84V13.5M16 10.77V13.53C16 14.12 15.5 14.6 14.92 14.6C14.34 14.6 13.86 14.12 13.86 13.53V10.77C13.86 10.18 14.34 9.71 14.92 9.71C15.5 9.71 16 10.18 16 10.77Z" /></svg>';
const shield =
  '<svg class="shield" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,1L3,5V11C3,16.55 6.84,21.74 12,23C17.16,21.74 21,16.55 21,11V5M11,7H13V13H11M11,15H13V17H11" /></svg>';
const smalldot =
  '<svg class="smalldot" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,10A2,2 0 0,0 10,12C10,13.11 10.9,14 12,14C13.11,14 14,13.11 14,12A2,2 0 0,0 12,10Z" /></svg>';

function pluz(num) {
  const intnum = Math.floor(num);
  if (intnum >= 0) {
    return `+${intnum}`;
  } else {
    return `${intnum}`;
  }
}

// Function to load the JSON data from a cookie
function loadJsonFromCookie() {
  // Start with a clean slate and initialised data structures

  // OK we are still here - so use a cookie
  const cookieValue = document.cookie.replace(
    /(?:(?:^|.*;\s*)jsonData\s*\=\s*([^;]*).*$)|^.*$/,
    "$1"
  );
  if (cookieValue) {
    const decodedData = decodeURIComponent(cookieValue);
    try {
      const parsedData = JSON.parse(decodedData);
      if (parsedData.cookieData) {
        cookieData = parsedData.cookieData;
      }
      regeneratePage();
    } catch (error) {
      console.error("Error parsing JSON data in cookie:", error);
    }
  }
}

// Function to load powers data from powers.json and overload.json
function loadPowers() {
  return new Promise((resolve, reject) => {
    // Load data from powers.json
    fetch("../powers.json")
      .then((response) => response.json())
      .then((data) => {
        // Check if overload.json exists
        fetch("../overload.json")
          .then((response) => response.json())
          .then((overrideData) => {
            // Combine the data from the two files
            powers = combineOverrides(data, overrideData);
            resolve(); // Resolve the Promise when powers are loaded and page is built
          })
          .catch((error) => {
            // If overload.json doesn't exist, just use the data from powers.json
            console.log("overload.json not found, using only powers.json");
            powers = data;
            resolve();
          });
      })
      .catch((error) => {
        console.error("Error fetching powers.json:", error);
        reject(error); // Reject the Promise if an error occurs
      });
  });
}

// Function to combine the data from powers.json and overload.json
function combineOverrides(baseData, overrideData) {
  const combinedData = [...baseData];

  overrideData.forEach((override) => {
    const existingPower = combinedData.find((p) => p.name === override.name);
    if (existingPower) {
      Object.assign(existingPower, override);
    } else {
      combinedData.push(override);
    }
  });

  return combinedData;
}

function regeneratePage() {
  // Get the color from the cookie before drawing
  document.documentElement.style.setProperty(
    "--md-primary-fg-color",
    cookieData.color
  );
  const elementExists = !!document.querySelector("#powers_summary");
  if (elementExists) updatePowersSummary();
}

function createPowerContent(power) {
  const section = document.createElement("div");
  section.classList.add("power-section");

  const title = document.createElement("h4");
  title.classList.add("power-title");
  title.textContent = power.name;
  title.addEventListener("click", () => {
    section.classList.toggle("expanded");
  });

  const briefSection = document.createElement("div");
  briefSection.classList.add("brief-section");
  if (power.brief)
    briefSection.innerHTML = `<span class="brief-style">${power.brief}</span>`;

  const descriptionSection = document.createElement("div");
  descriptionSection.classList.add("description-section");
  if (power.description) {
    descriptionSection.innerHTML = `<div>${markdownToHtml(
      power.description
    )}</div>`;
  }

  // Create power_def_sum div and columns
  const powerDefSum = document.createElement("div");
  powerDefSum.classList.add("power_def_sum");

  const pdsImmune = document.createElement("div");
  pdsImmune.classList.add("pds_immune");

  const pdsResist = document.createElement("div");
  pdsResist.classList.add("pds_resist");

  const pdsVulnerable = document.createElement("div");
  pdsVulnerable.classList.add("pds_vulnerable");

  // Add legend at top of group
  const legend = document.createElement("p");
  legend.classList.add("table_footnote", "abs");
  legend.innerHTML =
    `<span class="twemoji tiny">${shield}</span>` +
    "=<strong>Immune</strong>: critical hit required, " +
    `<span class="twemoji tiny">${red_dice}</span>` +
    "=<strong>Resist</strong>: attackers have disadvantage, " +
    `<span class="twemoji tiny">${green_dice}</span>` +
    "=<strong>Vulnerable</strong>: attackers gain advantage";
  // Add spans for power.immune
  if (power.immune && power.immune.length > 0) {
    power.immune.forEach((item) => {
      const chillSpan = document.createElement("span");
      chillSpan.classList.add("chill", "label-cell");
      chillSpan.innerHTML = `<span class="twemoji imm">${shield}</span>${item}`;
      pdsImmune.appendChild(chillSpan);
    });
  }

  // Add spans for power.resist
  if (power.resist && power.resist.length > 0) {
    power.resist.forEach((item) => {
      const coolSpan = document.createElement("span");
      coolSpan.classList.add("cool", "label-cell");
      coolSpan.innerHTML = `<span class="twemoji resist">${red_dice}</span>${item}`;
      pdsResist.appendChild(coolSpan);
    });
  }

  // Add spans for power.vulnerable
  if (power.vulnerable && power.vulnerable.length > 0) {
    power.vulnerable.forEach((item) => {
      const warmSpan = document.createElement("span");
      warmSpan.classList.add("warm", "label-cell");
      warmSpan.innerHTML = `<span class="twemoji vuln">${green_dice}</span>${item}`;
      pdsVulnerable.appendChild(warmSpan);
    });
  }

  // Append columns to power_def_sum div if at least one column has content
  if (
    pdsImmune.children.length > 0 ||
    pdsResist.children.length > 0 ||
    pdsVulnerable.children.length > 0
  ) {
    powerDefSum.appendChild(pdsImmune);
    powerDefSum.appendChild(pdsResist);
    powerDefSum.appendChild(pdsVulnerable);
    descriptionSection.appendChild(powerDefSum);
    descriptionSection.appendChild(legend);
  }

  const footer = document.createElement("span");
  footer.classList.add("power-footer");
  footer.classList.add("hang_right");

  if (power.type) {
    const typeSpan = document.createElement("span");
    typeSpan.classList.add("type-style");
    typeSpan.textContent = power.type;
    footer.appendChild(typeSpan);
  }
  if (power.range) {
    const rangeSpan = document.createElement("span");
    rangeSpan.classList.add("range-style");
    rangeSpan.textContent = `RNG:${power.range}`;
    footer.appendChild(rangeSpan);
  }
  if (power.damage) {
    const damageSpan = document.createElement("span");
    damageSpan.classList.add("damage-style");
    damageSpan.textContent = `DAM: ${power.damage}`;
    footer.appendChild(damageSpan);
  }
  if (power.ac) {
    const acSpan = document.createElement("span");
    acSpan.classList.add("ac-style");
    acSpan.textContent = `AC: ${pluz(power.ac)}`;
    footer.appendChild(acSpan);
  }
  if (power.sr) {
    const srSpan = document.createElement("span");
    srSpan.classList.add("sr-style");
    srSpan.textContent = `SR: ${power.sr}`;
    footer.appendChild(srSpan);
  }
  if (power.dr) {
    const drSpan = document.createElement("span");
    drSpan.classList.add("dr-style");
    drSpan.textContent = `DR: ${power.dr}`;
    footer.appendChild(drSpan);
  }

  if (power.mv) {
    const mvSpan = document.createElement("span");
    mvSpan.classList.add("mv-style");
    mvSpan.textContent = `MV: ${power.mv}`;
    footer.appendChild(mvSpan);
  }
  if (power.power_cost) {
    const powerSpan = document.createElement("span");
    powerSpan.classList.add("power-style");
    powerSpan.textContent = `PR: ${power.power_cost}`;
    powerSpan.setAttribute("data-power", power.name);
    footer.appendChild(powerSpan);
  }

  section.appendChild(footer);
  section.appendChild(title);
  section.appendChild(briefSection);
  section.appendChild(descriptionSection);

  return section;
}

function updatePowersSummary() {
  const powersSummary = document.getElementById("powers_summary");
  const equipSummary = document.getElementById("equip_summary");
  const skillsSummary = document.getElementById("skills_summary");
  const magicSummary = document.getElementById("magic_summary");
  const weakSummary = document.getElementById("weakness_summary");
  const sidekickSummary = document.getElementById("sidekick_summary");

  powersSummary.innerHTML = "";
  equipSummary.innerHTML = "";
  skillsSummary.innerHTML = "";
  magicSummary.innerHTML = "";
  weakSummary.innerHTML = "";
  sidekickSummary.innerHTML = "";

  // Sort the array by name so that checkboxes are in order
  powers.sort((a, b) => {
    const aName = a.name ? a.name.toLocaleLowerCase() : "";
    const bName = b.name ? b.name.toLocaleLowerCase() : "";
    return aName.localeCompare(bName);
  });

  powers.forEach((power) => {
    const section = createPowerContent(power);
    if (!power.type) power.type = ""; // This ensures that absent type is handled well
    if (power.type.toLowerCase().includes("equip")) {
      equipSummary.appendChild(section);
    } else if (power.type.toLowerCase().includes("skill")) {
      skillsSummary.appendChild(section);
    } else if (power.type.toLowerCase().includes("magic")) {
      magicSummary.appendChild(section);
    } else if (power.type.toLowerCase().includes("weakness")) {
      weakSummary.appendChild(section);
    } else if (power.type.toLowerCase().includes("sidekick")) {
      sidekickSummary.appendChild(section);
    } else {
      powersSummary.appendChild(section);
    }
  });
}

document.addEventListener("DOMContentLoaded", function () {
  // Get all the elements with the "clickable" class
  const clickableSpans = document.getElementsByClassName("clickable");
});
