*[TICK]: Division of `TURN` into a phase about a second long.
*[TICKS]: Division of `TURN` into a phase about a second long.
*[TURN]: An encounter round representing 10 seconds.
*[TURNS]: An encounter round representing 10 seconds.
*[SR]: Surface Rating: a damage threshold that must overcome for the damage to take effect. Damage equal to, or exceeding, this amount takes effect as normal.
*[DR]: Damage Reduction: all incoming damage is reduced by the DR before application to HP.
*[PR]: Power Requirement: an action's cost to a players POWER reserve. A measure of how strenuous an activity is.
*[AR]: Armor Rating: % chance that an attack will hit armor instead of target.
*[AC]: Armor Class: a players difficulty to hit. This is the number attack rolls need to match to score a hit against you.
*[POWER]: Power Reserve: a player's maximum POWER pool at rest. Power is determined from your base stats. Actions and exertion will deplete it.
*[HP]: Hit Points: a player's capacity for personal damage. Hit points represent the player's current health.
*[GIZMO]: GIZMO: tracks a number of clues, inventions, McGuyvers and quick fixes the player may execute. Also used to construct devices and items.
*[GIZMOS]: GIZMO: tracks a number of clues, inventions, McGuyvers and quick fixes the player may execute. Also used to construct devices and items.
*[MAX_LIFT]: Maximum LIFT: the maximum weight in kilograms tht the player can lift and hold off the ground.
*[GRAPPLED]: GRAPPLED condition: -50% MOVE, Move equivalent actions only.
*[INCAPACITATED]: INCAPACITATED condition: You have no HP remaining.
*[UNCONSCIOUS]: UNCONSCIOUS condition: Knocked-out temporarily as a result of taking HP. END saves to recover.
*[FATIGUED]: FATIGUED condition: You have no POWER remaining. Movement is 10% of normal. MAX_TICKS is now 1. Additional PR comes from HP.
*[POISONED]: POISONED condition: Movement is 50% of normal. All PR doubled.
*[CONTROLLED]: CONTROLLED condition: All PR doubled (minimum PR 1).
*[DYING]: DYING condition: Describe condition.
*[DEAD]: DEAD condition: Describe condition.
*[PINNED]: PINNED condition: MOVE=0. No Physical actions except resist grapple.
*[LIFT]: LIFT: a players maximum lift.
*[BLOCK]: BLOCK: Unit of mass equivalent to approx 25kg.
*[BLOCKS]: BLOCKS: Units of mass equivalent to approx 25kg.
*[HEX]: HEX: Units of of distance on the game map. Ususally equivalent to metres.
*[HTH]: HTH: The basic Hand-To-Hand damage is calculated from you LIFT capacity.
*[WEIGHT]: WEIGHT: weight in kg.
*[DETECT]: DETECT%: your danger-sense. Percentage chance to avoid surprise or notice hidden things.
*[HEAL]: HEAL: your daily healing alloment as a percentage of your total health.
*[LEVEL]: LEVEL: your progression as you gain experience and generally become better through practise and training.
*[PRONE]: PRONE: lying on the ground. Attacks against you from within 2 HEX have +2, further than that have -4.
*[INJURY]: INJURY: A serious wound with in-game consequences. You may have up to BLOCK such INJURIES before you become.
*[MOVE]: MOVE: The characters movement in HEX per TURN.