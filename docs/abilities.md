---
title: Abilities
icon: material/lightning-bolt
template: content.html
---

<h1 class="rules_title">Getting Started...</h1>

## Step 3: Choose your abilities

<div class=" faint gaps"><h4>The Heart of OpenHero's Customization</h4></div>

In OpenHero, abilities are the driving force behind the game's flexibility and adaptability. The modular design allows Game Masters (GMs) to shape the type of campaign they want to run by tailoring the abilities available to players.

<div class="rounded gaps" markdown>
The selection and acquisition of abilities are entirely dependent on the campaign guidelines set by the GM. Players may be allowed to choose their abilities, roll for them randomly, or follow a specific accumulation process unique to the campaign.
</div>

OpenHero is built to accommodate a wide spectrum of power levels and genres, from gritty, low-level campaigns to galaxy-spanning adventures with demi-god characters. The system's smooth scaling is achieved through the customization of abilities, making it easy for GMs to manage games of varying scopes and complexities.

The following sections provide examples of abilities based on the standard superpowered rules of OpenHero. However, the system's modular nature allows for the creation of custom ability sets to fit any desired setting or genre, from post-apocalyptic wastelands to high fantasy realms, cyberpunk dystopias, space operas, and gothic horror.

Ultimately, the GM has the final say in determining the abilities available in their campaign, ensuring that everyone plays by the same rules and that the game aligns with the desires of both players and the GM alike.



=== "Super Heroes"

    <h2>Super Powers</h2>
    ![Super Powers](assets/powers.png){ loading=lazy }

    ##### Unleash Your Inner Hero: Super Powers Uncaged!

    In this game, you'll have the chance to wield incredible abilities that defy the limits of ordinary mortals. Super powers are often bestowed upon the unsuspecting by a twist of fate, and once acquired, they cannot be suppressed by any magical means. The effects of your super powers are mighty, but with great power comes great responsibility.

    To ensure a thrilling and balanced gameplay experience, you'll choose your super powers in collaboration with the Game Master. And remember, even the most formidable abilities can have their drawbacks. Consider selecting Weaknesses or placing circumstantial limitations on your powers to keep things fair and engaging. After all, a death touch that only works at night when you're wounded is far less game-breaking than an unrestricted one.

    <div id="powers_summary"></div>

    <h2>Weaknesses</h2>
    ![Weakness](assets/weakness.png){ loading=lazy }

    ##### Every Hero Has a Kryptonite: Embracing Weaknesses

    In the world of super powers, even the mightiest heroes have their vulnerabilities. Weaknesses add depth to your character and create thrilling challenges to overcome. When choosing your character's weaknesses, consider how they will impact your adventures and make your story more compelling.

    Will your character's powers fade under the light of the full moon? Do they have a debilitating fear of heights that renders them helpless in high-stakes situations? Or perhaps they have a soft spot for a particular enemy that tests their resolve?
    
    Embrace your character's weaknesses and watch as they shape your hero's journey in unexpected ways. Remember, it's not about being invincible; it's about rising above your limitations and becoming the hero you were meant to be!
    <div id="weakness_summary"></div>


    <h2>Psionics</h2>
    ![Magic/Psionics](assets/magic.png){ loading=lazy }

    ##### Tapping into the Mystical Forces

    Magical Powers are intended to be an alternate use of GIZMOS.
    They are intended for flavour, for NPCs, rather than as a source of more power for players. It fills a middle ground between super-powered enemies and normal mortals in technical gear.

    For example, a spell that duplicates an aspect of **Flame Power A**, should be able to achieve only one aspect of the power, either flight or flame defence or flame attack. In this case, the spell equivalent is either more costly to PR, takes longer to cast than the equivalent power, or lacks some additional feature of the power it duplicates.

    Note that spells can also be **dispelled** or **counterspelled** unlike powers.
    For spells that have a PR greater than 5, another limitation is to restrict usage of the spell (only at night, under the moon, etc), require material components or need an advanced ritual requiring hours or days.

    <div id="magic_summary"></div>


    <h2>Skills</h2>

    ![Skill](assets/skills.png){ loading=lazy }
    ##### Mastery Through Dedication: Honing Your Skills

    In a world where super powers reign supreme, skills acquired through hard work and dedication can be just as valuable. As your character progresses through the game, they will have the opportunity to accumulate and level up a wide array of skills. From the mundane to the extraordinary, these skills will shape your character's unique identity and play a crucial role in their journey.

    Will your character be a master of Hyperdimensional Mathematics, able to manipulate the fabric of reality with their mind? Or perhaps they will delve into the mysteries of Qabbalistic Magical Theory, unlocking ancient secrets and wielding mystical powers? For those with a taste for adventure, the skill of Piloting Spacecraft may be the key to exploring the vast reaches of the universe.

    Your character's skills can also serve as a foundation for their backstory, adding depth and context to their motivations and experiences. As you advance through the game's narrative, certain skills may become prerequisites for accessing new storylines, unlocking hidden paths, and shaping your character's destiny.

    Choose your skills wisely, for they will be your tools to overcome challenges, unravel mysteries, and leave your mark on this super-powered world. Get ready to embark on a journey of growth, discovery, and mastery!
    <div id="skills_summary"></div>


    <h2>Equipment</h2>

    ![Equipment](assets/equipment.png){ loading=lazy }

    ##### Gear Up and Tech Out: Mastering the Equipment Section

    In a world where super powers reign supreme, technology refuses to be left behind. The Equipment section is your gateway to an array of gadgets and tools that will enhance your abilities, provide protection, and unlock new ways to tackle challenges.

    From power-boosting exoskeletons to high-tech weaponry, the right equipment can bridge the gap between ordinary mortals and the super-powered elite. As you progress through the game, you'll discover that many of the awe-inspiring feats of super powers have their technological counterparts waiting to be mastered.

    Choose your gear wisely, for it will be your trusted companion in the face of danger and adversity. Whether you're a tech-savvy strategist or a gadget-loving adventurer, the Equipment section has something to offer. Embrace the cutting-edge and gear up for the journey ahead!

    <div id="equip_summary"></div>


    <h2>Sidekicks</h2>

    ![Sidekicks](assets/sidekick.png){ loading=lazy }

    ##### Sidekicks and Symbiosis: Unleashing Dynamic Duos

    In the world of heroes and villains, no one should have to face the challenges alone. The Sidekick section is your gateway to forging unbreakable bonds and unleashing the power of symbiotic relationships.

    Whether you're a hero seeking a loyal companion or a master of the arcane looking to summon an astral servant, the possibilities are endless. From robot dogs to zombie compatriots, your sidekick will be more than just an ally; they'll be an extension of your very being.

    <div id="sidekick_summary"></div>

    <h2>Vehicles</h2>
    <div id="vehicle_summary"></div>
   
=== "Swords & Spells"

    <h2>Feats</h2>![X](assets/vehicle_ss.png){loading=lazy}<div id="powers-ss"></div>
    <h2>Spells</h2>![X](assets/vehicle_ss.png){loading=lazy}<div id="magic-ss"></div>
    <h2>Skills</h2>![X](assets/vehicle_ss.png){loading=lazy}<div id="skill-ss"></div>
    <h2>Equipment</h2><div id="equip-ss"></div>
    <h2>Followers</h2>![X](assets/sidekick_ss.png){ loading=lazy }<div id="sidekick_ss"></div>
    <h2>Mounts</h2>![X](assets/vehicle_ss.png){loading=lazy}<div id="vehicle_ss"></div>
    
    
    
    <div id="vehicle-ss"></div>
=== "Tech-Punk"

    <h2>Cybernetics</h2>
    <h2>Psionics</h2>
    <h2>Skills</h2>
    <h2>Tech-gear</h2>
    <h2>Contacts</h2>
    ![Sidekick](assets/vehicle_tp.png){ loading=lazy }
    <div id="vehicle-tp"></div>


=== "Space-Monk"

    <h2>Force Powers</h2><div id="powers-sm"></div>
    <h2>Weaknesses</h2><div id="weakness-sm"></div>
    <h2>Skills</h2><div id="skills-sm"></div>
    <h2>Spaceships</h2><div id="vehicle-sm"></div>
    <h2>Skills</h2><div id="equip-sm"></div>
    <h2>Droids</h2>
    ![X](assets/vehicle_sm.png){ loading=lazy }
    <div id="sidekick-sm"></div>

=== "Post-Nuke"

    <h2>Mutations</h2>
    <h2>Psi Power</h2>
    <h2>Skills</h2>
    <h2>Gear</h2>
    <h2>Rides</h2>
    ![X](assets/vehicle_pn.png){ loading=lazy }
    <div id="vehicle-pn"></div>

=== "Goth Horror"

    <h2>Super Powers</h2>
    <h2>Weaknesses</h2>
    <h2>Cunning</h2>
    <h2>Minions & Thralls</h2>
    ![X](assets/vehicle_goth.png){ loading=lazy }
    <div id="vehicle-goth"></div>
