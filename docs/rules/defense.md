---
icon: material/shield-outline
template: nocontent.html
---
<h1 class="rules_title">OpenHero Role playing game</h1>

## Defenses

![Defenses](../assets/defenses.png)
### Immunity :material-shield-alert:

Immunity is a powerful condition that renders your character unlikely to be affected with damage of this type.

- **Environmental effects**: Non targeted effects of this type are unable to damage you. 
- **Targeted attacks**: If there is a *roll to resolve an attack* then characters with immunity will only be hit on a **Critical Success**. Even then, then the immunity will protect the character from any **Wounds** or **Maximized Effect** the crit may cause. If the attack roll does not resolve to a **Critical Success** then the character may ignore the attack.
<div class="faint gaps" markdown>
A character with **Thermal** immunity can walk across lava, enter a burning house (provided they can still breathe), and so on, without experiencing any of the environmental or area damage of that type.
</div>

### Resistance :material-dice-d20: {.disadvantage}

Resistance renders your character resilient to damage of this type.

- **Environmental effects**: Non targeted effects of this type do **half** HP rounded down. 
- **Targeted attacks**: If there is a *roll to resolve an attack* then characters with resistance have 1 *disadvantage* die added to the roll to resolve the attack.

### Vulnerability :material-dice-d20: {.advantage}

Vulnerability means your character is disproportionally affected by type of damage.

- **Environmental effects**: Non targeted effects of this type do **double** HP. 
- **Targeted attacks**: If there is a *roll to resolve an attack* then characters with vulnerability have 1 *advantage* die added to the roll to resolve the attack.

### Armor Rating AR

- Represents the percentage chance of armor intercepting an attack.
- Tested with a d100 roll; if the roll is below the AR, the armor absorbs the damage instead of the target.
- AR reduces by the amount of damage taken.
- Cover works similarly, but its effectiveness is binary (either effective or not) and damage to cover is not tracked.
> **Special Attacks** to bypass armor or cover are possible, 
     [AR <= 75:material-dice-d20:{.disadvantage}][AR > 75 :material-dice-d20:{.disadvantage}:material-dice-d20:{.disadvantage}]

### Surface Rating/Scratch Resistance SR

- Represents the hardness of an object or player's surface.
- Damage below the SR value is ignored.
- If damage equals or exceeds the SR, all of it is applied normally.
- Applies to **Physical** damage only


### Damage Reduction DR

- A rare defensive property that should not exceed a value of 5.
- Reduces incoming damage by its value before applying it to the target's HP 
- Essentially acts as a "damage soak."
