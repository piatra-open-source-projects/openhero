---
icon: material/target
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![mods](../assets/mods.png)

## Combat Modifiers

### Facing/Awareness/Prone

A character's facing affects their ability to attack and be attacked. Attacks from the front are unmodified, while attacks from the side or rear incur penalties or bonuses. Prone characters are harder to hit from range but easier to hit in melee. Awareness of threats can allow characters to change facing outside their turn at a power cost.

Facing modifiers range from 0 front to +4 rear as per normal V&V rules, However, these should be thought of as Awareness modifiers for the target. Being blinded, or somehow oblivious to an attack gives the maximum awareness modifier of +4. It does not also stack penalties with facing.
Invisibility is now handled differently. Characters with Invisibility are always -4 to be hit due to the maximum Awareness/Facing modifier. That does not change over time. It still applies when the attacker knows where (approximately) the invisible target is. This is usually the result of a successful Detect Hidden roll, or the invisible target has done something else to give their location away, such as attack with a visible power, leave foot prints, etc.
Additionally, Invisible characters are able to snipe (see 5.6 SNIPING/LINING UP THE SHOT) whilst moving and acting provided they remain undetected. If an invisible character is located, all sniping bonuses are temporarily lost but not the -4 Awareness modifier. The sniping bonus for invisible characters may only be applied to one opponent at a time.
Invisible characters are transparent only to normal light; not to infrared. ultraviolet. etc. 


### Range Modifiers

Attack rolls are modified based on the distance between the attacker and target. Consult the range modifier table to determine the appropriate adjustments to the hit chance.

### Cover

When a target is partially hidden behind an obstruction, determine the % of the target that is covered. If a hit is scored, roll percent dice: if the roll is less than or equal to the % of the target that is covered, the attack hits the cover instead of the target. Roll damage against the cover, and any damage that gets past the cover still hits the target. An attacker can avoid the interference of cover by making a special attack against any part of the target that is sticking out. In other words, cover acts like a situational `AR` awarded by the GM for making use of the environment.

> Note: Targets behind cover can be targeted using Special Attacks [#FIL], just as armored characters can.

### Velocity Modifiers

Extreme differences in *relative* velocity will negatively effect hit rolls.
In most cases, the difference in velocity will apply as an increase into the targets Armor Class `AC`.
Consult the table.

### Sniping

Characters may gain a cumulative bonus for **sniping** a shot. Firstly a target is selected by the sniper and announced to the GM. The character must forfeit movement and physical actions and powers. For each TICK forfeited in this manner, the character gains a cumulative +1 bonus to hit the target, up to a maximum of +10. The character must be able to see or otherwise detect the target for the entire duration of the snipe. Once broken, by losing sight or by attacking the target the sniping bonus is removed.

### Weakness Detection

[Please explain how characters can identify and exploit weaknesses in their targets.]

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod, nisi vel bibendum consectetur, nisl nisi aliquam eros, eget tincidunt nisl nunc eget lorem.

### Weakspots

[Please provide information on targeting specific weak points on a character or object and the effects of doing so.]

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod, nisi vel bibendum consectetur, nisl nisi aliquam eros, eget tincidunt nisl nunc eget lorem.

