---
title: Abilities
icon: material/clock-fast
template: nocontent.html
---
<h1 class="rules_title">OpenHero Role playing game</h1>

![Encounter](../assets/encounter.png)

## Game Play

### Encounters

Gameplay is usually free flowing and conversational until the game enters into an encounter where multiple events vie to happen at once and contests emerge. In these circumstances we enter into the **Encounter** game mode, which is turn-based. Actions, movement and sequence of turns are all tracked and coordinated.

When the game is running in **Encounter** mode, all combatants are organised into a queue, and each combatant takes their turn in order, resolving each go, and the consequences of it before passing on to the next combatant.

First some terminology:
<div class="faint gaps" markdown>
#### TURN
- Each game round is called a TURN that represents 10 seconds.
- Depending on their  total **Initiative** (below), players may get to act more than once on a TURN.
</div><div class="faint gaps" markdown>
#### TICK
Game TURNS are further divided into a variable number of TICKS. Think of a TICK as chunk of time about half a second or so - but it is necessarily flexible.

- There is no fixed number of TICKS in a TURN
- There is no fixed time between TICKS.

They are just a convenient slicing of the turn into further segments as needed.
</div>
### Initiative Order

At the start of each encounter, all characters involved roll 1d10 and add their Agility score to determine their initiative. Characters act in descending order of their initiative scores, with ties split by highest innate Agility (further ties split by level). Characters can delay their actions to any later point in the TURN, but cannot save actions for future TURNS or perform more than one action per TICK.

```
INITIATIVE = A + LEVEL + d10
```
A character is able to move and then take an action on their TICK, or even move take an action and move some more. Movement is able to be freely spent over the course of a players TICKS within a TURN, however they can only move (or take movement equivalent actions) up to their total for the TURN at which point they may find themselves flat-footed and stuck in a disadvantageous position. Caught out in the open, as it were.

There is a strict limit to only one action on a players TICK. Some slight exceptions are multiple attacks or carrier attacks, which are technically still a type of action - they are just actions with a few more swings in them.

<div class="faint gaps" markdown>
#### MAX TICKS

Characters are also limited to a maximum number of effective TICKS they may act on, irrespective of their **Agility** score. This is a game balance requirement otherwise characters with **Heightened Agility** powers not only gain all the benefits of that power, but also get 50% or more extra actions over all other players at the table. Not cool.

- Characters may act on 2 TICKS max at level 1.
- Every 5 levels thereafter gain additional TICK per TURN.

</div>

### Actions

The GM may determine that certain uses of movement may require dice rolls to succeed. For example, dropping a controlled car using Magnetic Powers is movement only, however, dropping the car onto an opponent requires an attack roll to hit. The general guideline is that  whenever the player is attempting to achieve a particular outcome, then the GM should consider the movement as an action, possibly as part of a multiple action.

Sometimes, it is possible that the use of a power or ability should be considered movement, rather than needing an action. This is normally the case if the target is inanimate or non-defending.
For example, Dark Sun uses fists to punch through a wall as he is running down a corridor and continues into the room. Or Super Nova fires random flame blasts into buildings as he flies down Main St wreaking terror.
Power cost apply as per normal, and the actions use up a fraction of the characters movement. Half of a characters movement if he has two actions per turn, A third if they have 3 actions per turn. A quarter of movement for 4 actions per turn. So on.

### Surprise and Ambush

When a successful ambush occurs, only the attackers roll for initiative on the first turn. Defenders are allowed no normal actions during the first turn, but may perform any free actions as normal.

Characters should be allowed to roll their DETECT% when being ambushed. Passing this check would allow them to act normally in the surprise turn.

### Between Turns

Power expenditures and other between-turn occurrences are resolved before new initiative rolls are made for the next turn. Environmental effects are also resolved at this time.

### Free actions


Some actions are permitted out of turn. Conversation and table talk is usually one. However, if discussons and planning are taking away from the flow of the game then the GM should remind players that a TURN represents 10 seconds only and any indecision in taking actions may rightly be interpreted as a delayed action.

Other free actions outside combat are allowed:

- diving PRONE
- dropping items in hand
- flinching
- short exclamaitions

