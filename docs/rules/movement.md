---
icon: material/run-fast
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![Movement](../assets/movement.png)

## Movement

OpenHero is a tabletop role-playing game that can be played entirely in the theater of the mind. However, when encounters begin and the game shifts into turn-based mode, it's highly recommended to use a visual representation of the battlefield to ensure everyone is on the same page.

### Battle Maps HEX

When setting up the play area, you have the option to divide the table space into either a grid or hexes (HEX). This choice is a matter of personal preference. Typically, one HEX corresponds to one meter in-game, but this scale can be adjusted based on the encounter's location:

 >   **Crowded Spaces**: In tight quarters, a HEX might represent a shorter distance to accommodate the confined area.

 >   **Outdoor Encounters**: When battles take place in vast, open spaces, a HEX could correspond to larger distances to keep the play area manageable.

### Tokens

If you choose to use counters or tokens to represent characters on the battlefield, be sure to indicate each character's facing direction. This information is crucial because there are bonuses for striking unaware characters or attacking from behind. 

Paying attention to facing is an option rule, but adds a layer of tactical depth to encounters. If the table prefers however, game play may be simplified by dropping it without unbalancing encounters.

### Movement equivalent actions

In OpenHero, movement encompasses a wide range of activities beyond simply traversing the battlefield. Characters can move before and/or after their action on their TICK, but their total movement during a TURN cannot exceed their MOVE attribute. This means that a character who exhausts their movement could find themselves exposed and vulnerable until their next opportunity to seek cover or escape the line of fire.

Movement incorporates actions such as:

- Opening locks
- Drawing/reloading weapons
- Lifting manhole covers
- Opening car doors
- Flipping through a wallet
- Shoving objects out of the way if you dont care where they end up

The time required to complete these tasks is deducted from a character's movement. Keep in mind that a game TURN represents 10 seconds of in-game time. The Game Master (GM) is responsible for estimating the fraction of movement consumed by any particular task, whether or not they inform the player beforehand. A task that takes 3 seconds will use up 30% of the characters MOVE for the TURN.
<div class="gaps faint" markdown>

**Example: Superhero Seeking Cover**

Imagine a superhero, *Blazer*, engaged in a intense battle on a city street.

> On their first TICK, the hero decides to move to a nearby car for cover. The player describes the hero dashing to the vehicle, yanking the door open, and ducking behind it.

On the battle map, the car is about 14 metres (HEX) away. The GM determines that opening the car and crouching behind it takes about 2 seconds (or 20% of *Blazers* 66 MOVE).

> The player says they reload their nail-gauntlet and pop out of cover to make a **Physical** attack at *BadDude* and crouch back down behind the door.

GM determines the reload would be another 3 seconds to accomplish (30% MOVE). Before popping out of cover to make his shot ACTION, then dropping back into cover (1 HEX each). The GM resolves the attack, and the hero's go concludes, leaving them in a more advantageous position for their next TICK of the encounter.

In the first TICK *Blazer* has used 49 HEX of their MOVE (14+13+20+1+1).  *Blazer* gets a second TICK in 10 phases, but will have only 17 MOVE remaining for the TURN and could find their options on that next TICK limited. 
</div>

### Jumping, leaping, vaulting

Characters can jump a distance based on their Carrying Capacity and body weight. The maximum distance and time spent in the air are calculated using specific formulas. Agility saves are required to achieve the intended trajectory.

### Vertical jumps

[Please provide rules for determining the height and duration of vertical jumps.]

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod, nisi vel bibendum consectetur, nisl nisi aliquam eros, eget tincidunt nisl nunc eget lorem.

### Other movement modes

[Please clarify any additional movement modes, such as flying, swimming, or climbing, and their respective rules.]

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod, nisi vel bibendum consectetur, nisl nisi aliquam eros, eget tincidunt nisl nunc eget lorem.

### Bonus to movement

[Please explain any factors that can provide bonuses or penalties to movement, such as powers, abilities, or environmental conditions.]

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod, nisi vel bibendum consectetur, nisl nisi aliquam eros, eget tincidunt nisl nunc eget lorem.

