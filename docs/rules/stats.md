---
title: Basic Stats
icon: material/dice-multiple
template: nocontent.html
---

<h1 class="rules_title">Getting Started...</h1>

## Step 1: Assign Personal Details

<div class="rounded gaps" markdown>
### Character Name

Choose a `NAME` for your character that fits the campaign setting.
</div>
<div class="rounded gaps" markdown>
### Player Name

Record the `IDENTITY` of the player controlling the character.
</div>
<div class="rounded gaps" markdown>
### Character Weight

A character's WEIGHT is important for the BLOCK system, where everything is considered in 25L blocks of material. It does not need to be exact, but you will find there are advantages to being both small and large.
</div>

<div class="rounded gaps" markdown>
### Level

A character's LEVEL is starts at level 1. "Professional" non-player characters and wild animals are usually considered at least level 4. Particular paragons of a species or unique individual NPCs will have level determined by the GM.

Note that LEVEL brings proficiency in attacking, defending and also makes you slightly more resilient to damage.
</div>
##   Step 2: Assign Basic Stats

Basic stats represent a character's innate abilities and are used to calculate derived values and perform various actions in the game.

Character follow the common d20 SRD representation of 3d6. So average is a spread from 9-11 and the minimum of 3 or maximum of 18 represent less than one percent of the population. The method of attribute determination is left to the GM, but we recommend that if randomly determined the method of *ROLL 4 TAKE BEST 3* allows players to start slightly ahead of the curve.

<div class="rounded gaps" markdown>
### Strength

- physical power
- muscle mass
- ability to exert force relative to weight

Examples of characters with high `STRENGTH` include weightlifters, powerlifters, and strongman competitors.
![X](../assets/str.png)
</div>
<div class="rounded gaps" markdown>
### Endurance

- stamina
- resilience
- withstand physical strain
- recover quickly

Examples of characters with high `ENDURANCE` include marathon runners, triathletes, and endurance athletes.
![X](../assets/end.png)
</div>
<div class="rounded gaps" markdown>
### Agility

- coordination
- balance
- physical dexterity
- aim

Examples of characters with high `AGILITY` include gymnasts, acrobats, and parkour practitioners.
![X](../assets/agil.png)
</div>
<div class="rounded gaps" markdown>
### Intelligence

- wisdom
- knowledge
- reason
- mental resolve

It represents a character's cognitive abilities, problem-solving skills, and mental acuity. Examples of characters with high `INTELLIGENCE` include scientists, strategists, and scholars.
![X](../assets/int.png)
</div>
<div class="rounded gaps" markdown>
### Charisma

- conviction
- strength of personality
- personal aura
- influence

It encompasses a character's ability to influence and inspire others. Examples of characters with high `CHARISMA` include leaders, diplomats, and motivational speakers. It is not indicative of physical attractiveness which has no bearing on reactions on its own.
![X](../assets/cha.png)
</div>
