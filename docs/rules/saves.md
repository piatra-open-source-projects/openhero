---
icon: material/scale-unbalanced
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![Saves](../assets/saves.png)

## Saving Throws

### When to make saving throws

Saving throws are a test against the characters basic stat scores. The standard OpenHero rules are intended to allow the GM a little more flexibility. Saving throws are based on classes of difficulty.

### Difficulty of saving throws

- **Easy Tests of Skill (d20)**: These tests of skill are for occasions where competence needs to be tested. A normal person has a 50-50 chance of pulling a feat like this off. For a talented hero, this should be considered easy, but worth a roll. The GM may determine that after a certain number of successful checks at this level the task becomes trivial and no longer requires a roll.
- **Moderate Test of Skill (d40)**: Equivalent to 3 x Ability on d100. Usually used for when Easy things get hard, or when characters start to get a handle on things after a True test of skill.
- **True Test of Skill (d60)**: Equivalent to 2 x Ability on d100. This is a very difficult challenge for all but the most freakish heroes. A success here can make things easier later, modifying future checks down a category.
- **Awesome Test of Skill (d100)**: Equivalent to rolling under Ability on d100. This is a very difficult challenge for all but the most freakish heroes. 
- **Godlike Test of Skill (d200)**: These tests of skill are touch and go even for the most able of heroes. Even galactic beings struggle to pull off such stunts without fear of failure. Roll under half ability on d100.


### Consequences of failing saves

Failing a **saving throw** can result in various negative effects, such as taking damage, suffering a condition, or being unable to avoid a hazard. The specific consequences depend on the nature of the save and are determined by the GM.

Failing a **test of skill** the GM may allow the players to recover in subsequent actions, with each fail increasing the category of difficulty, and each success allowing a new roll at an easier level of difficulty until the GM determines a result OR the Task has become Trivial or Impossible.

### Types of saving throws

Int saves to discver or invent. Use of GIZMOS requires a Moderate (I*3)% check
Endurance saves to wake up from incapaication (E%). Similar checks to overcome blindness and other incapatications.
Strength saves to lift heavy weiht of themselves or push through bars etc.

