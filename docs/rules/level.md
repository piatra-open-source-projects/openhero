---
icon: material/stairs-up
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![levels](../assets/levels.png)

## Gaining levels

### Fame

Characters gain fame every time they go up a LEVEL. This is a natural bonnus to **Charisma** as word of their deeds spreads, or their demeanor encompasses the breadth of their experience. In any case, everytime the palyer goes up a level they roll d20. If the number is greater than their Charisma score then they immediately gain +1 **Charisma** as a fame bonus. This check is made at each LEVEL but **Charisma** can not be rasied past 20 in this fashion. Charisma can however be  trained just like any other basic stat.

### Gizmos

GIZMOS are a problem solving mechanic to open up solutions in game. They are like a combination of techniques, tools, adaptations, ideas, and devices that make the characters better or more unique in some way.

There are two basic uses for your GIZMOS, which are an intelligence scaling resource.


GIZMOS may be used to create inventions that require 4 weeks of game time to spend *or* 2 weeks without adventuring.
This would be a device - workshopped in advance with the GM - that provided some small modifier or benefit.
Successful implementation is not guaranteed. The success or failure is only known after the investment of time. Roll a INT*3 (Moderate) saving throw in most cases. GM may adjust difficulty of the roll but should have reasons for doing so and announce the new target before rolling.

Invention GIZMOS may be concatenated into development chains that yield something akin to a ability of the Equipment type. Work with the GM. They will determine how many GIZMO points the build requires, what the difficulty and timeline of checks are, consequences of failure. No benefit should generally be received until the GIZMO is completed.

For characters with appropriate skills or abilities GIZMOS could be used to advance those. Characters with a PE for a sidekick could spend GIZMOS to advance the pet or teach skills. Characters with magical understanding could use GIZMOs to develop new spells instead of devices, for example.

Multiple GIZMOS can be worked on simultaneously, limited to a number equal to LEVELs.

There is an alternate **narrative** use of GIZMOS to charge or shape a power in a manner unintended under the rules. This costs a GIZMO, and also requires a successful INT save (GM determines difficulty). If the roll fails, the GIZMO is not used, but the action is attempted nevertheless. The GM will determine the unintended effects and PR cost, if applicable.

Examples include, using Vibratory Powers to phase a section of Earth under an opponent trapping them, or using Heightened Agility to perform some awesome stunt to snatch a key item. Since this is a one-off use of a valuable GIZMO, the players should come up with something good, and the GM should allow it if reasonable.


### Training stats

Characters can improve their basic attributes through training. Characters can raise any single basic stat by 1 point every time they increase a level. Note this as level training.

### Ability Progression

Just as the selection of abilities shapes the foundation of an OpenHero campaign, the progression of those abilities as characters level up is equally crucial in defining the game's pace and style. The rate and method of ability progression are determined by the Game Master (GM), tailoring the experience to their desired campaign.
Options for ability progression include:

1. New Abilities:

    - Players select a new ability every level or every few levels.
    - This approach allows characters to continually expand their repertoire and gain new powers as they grow.


2. Ability Improvements:

    - Instead of acquiring new abilities, players enhance their existing ones.
    - Improvements can include reducing Power Rating (PR) costs, increasing range or To Hit modifiers, or broadening the ability's effect.
    - This method focuses on honing and mastering the abilities characters already possess.


3. Limited Progression:

    - The GM may choose to limit players to their initial abilities and redirect progression down other paths.
    - However, given the broad definition of "ability" in OpenHero, which encompasses gear, equipment, and skills, there should always be a subset of abilities for players to acquire as they level up.
    - This approach encourages players to find creative ways to utilize their existing abilities while still providing avenues for growth.



Regardless of the progression method chosen, it is essential for the GM and players to have a clear understanding of the game's parameters from the outset. The decision-making process should be collaborative rather than dictatorial, ensuring that everyone is on the same page and invested in the campaign's direction.

By carefully considering ability progression, GMs can create a game that evolves alongside the characters, keeping the experience engaging and rewarding throughout the journey.




