---
icon: material/sprout
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![environment](../assets/environment.png)

## Environmental Effects

### Objects and toughness SR

Objects have a Structural Rating (SR) that determines the damage needed to break or destroy them. Damage equal to or greater than the SR damages the object, while lesser damage has no effect. Characters attacking objects may take damage if their attack fails to overcome the object's SR.

### Smashing through

[Please provide rules for characters attempting to smash through barriers or objects.]

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod, nisi vel bibendum consectetur, nisl nisi aliquam eros, eget tincidunt nisl nunc eget lorem.

### Falling

Characters and objects take damage from falls based on the distance fallen and their own weight. Factors such as air resistance, landing surface, and abilities can modify the damage taken. Conscious characters can attempt Agility saves to reduce fall damage.

### Grappled/Restrained/Pinned

You are GRAPPLED, which means you are held by some other character or force and no longer control your movement. Your movement drops to zero. You are still able to take actions.

To **break a grapple** requires an action and a HTH *wrestling* check. This is an opposed `Base HTH` damage roll. Each party in the grapple rolls their damage. If you equal or escape your grapplers amount then you have broken the grapple and you are free to move.

The condition `restrained` is an escalation of GRAPPLED. You are unable to move and you also are unable to take actions requiring your body. The GM will adjuducate to allow purely mental actions which should not be affected by merely being grabbed.

The condition PINNED is a further, final escalation of the `restrained` state. You are unable to move or take actions. You are additionally unable to speak, see or otherwise have your senses obscured. In this state, the only options left for most characters is "the getaway".

### Throwing

Characters can throw objects based on their Carrying Capacity and the object's weight. The maximum distance thrown is determined by a formula involving these factors. Damage from thrown objects is resolved similarly to falling damage.

### Catching

To catch a falling object, a character must succeed on an Agility save and have a Carrying Capacity greater than the object's weight.
- Catching an object in midair negates falling damage
- Catching it at ground level reduces the damage based on the catcher's Carrying Capacity as follows:
  If the catcher succeeds on their Agility save (difficulty determined by the GM before rolling) then they roll their HTH dammage and subtract that from the falling damage. If this roll is greater than the falling damage then the catch was successful. If the falling damage is greater than this, then the difference is shared between the cather and the falling object.
