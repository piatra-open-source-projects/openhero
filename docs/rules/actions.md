---
icon: material/sword-cross
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![Actions](../assets/actions.png)

## Actions

### Evasion

Evasion is a defensive posture where you focus on being defensive in place of some other action. Taking Evasion is an ACTION that costs 1 PR.

Any character that Evades get a bonus to their AC equal to **one tenth** of their **current** POWER reserve. This bonus to AC lasts until their first TICK on the next game TURN.

 > *Bonestrike* falls through the ceiling of the restaurant and lands right in the middle of the mobster's meeting surprising everyone. *Bonestrike* acts first in the opening TURN (with 70 POWER) and decides the best thing to do is Evade and tumble for cover at the bar. *Bonestrike* spends 1 PR to evade adding +6 to his AC for the rest of this TURN. GM determines that the tumble off the table will require an EASY Agility Save - especially easy for *Bonestrike*'s high agility.


### Special Attacks

Also known as "called shots". This is when the player accepts a higher level of difficulty to try to cause a specific effect as a result of their action. Aiming for the head to attempt a knockout
Aiming for the hand or weapon to disarm their foe
Aiming for the weak point in the dracomoths hide
Shooting at someone behind cover or inside protective armor.




### Carrier Attacks

Carrier attacks are attacks comprised of a *primary* attack that delivers a secondary *payload* attack. Examples would be a flaming fist, or a sword that releases a pyschic smite when it tastes blood.

Carrier attacks require two attack rolls to resolve. The *primary* attack is resolved normally, and if successful, allows the *payload* attack to be resolved. If the carrier attack misses, or fails to overcome the targets defense and actualy damage HP the *payload* attack is not resolved.

> Note: The target's protection still applies against the carrier attack which is resolved and costs PR as normal.


### Multiple Attacks

Characters can attack multiple targets in a single tick, provided they can see all targets and do not move between attacks. The attacker must declare the number of attacks, targets, and damage type beforehand. If any attack roll misses, all attacks miss. Power is spent for each attack up to and including the first miss. Additional attacks after the first always accumulate a disadvantage die to the attack dice pool.

### Overcharging Attacks

Sometimes, a character finds his or her limits and needs to push through them at personal cost in order to save the day.
Characters can perform heroic feats or enhanced power usage at personal cost. 
Overcharging can be announced after the dice pool is rolled. 

When overcharging, the character pays double the power cost for a one-time 50% increase to one aspect of the power. The crit range for the attack also expands by one for the attack which could potentially turn a 19 on a die into a critical hit. 

However overcharging comes at some personal risk.
The character must take HP damage equal to the PR cost (minimum 1).
This damage cannot be rolled with, and unconsciousness must be checked for.
If the character fails this check, they will become knocked out at the end of the current TICK.


### Pulling the Punch

After rolling an attack, but before rolling damage an attacker can declare they are pulling their punch, reducing the damage dealt. The character may substitute any die in their HTH damage dice for a smaller die.
(d20>d12>d10>d8>d6>d4>d2>1). Exceptionally power characters, with HTH attack pools comprising many dice, may still be left with a minimum slap capable of inflicting serious damage.

> Note that this reduction only applies to **Physical** attack types.


### Flinging Attacks

A flinging attack is type of **Physical** attack intended to send the opponent flying backwards at the expense of damage. The attack must be announced prior to rolling damage. Damage is rolled as normal - except the knockback distance is calculated from the entire damage in excess of the targets BLOCKS.  `AR`, SR and DR are all ignored for the purpose of this calculation. Half the roll is applied as incoming damage. Defenses against this apply as normal. It is possible that the knockback will cause additional damage.

The actual distance thrown is covered under Knock Back [#FIL]

### Grappling

This manoeuvre requires a normal roll to hit, but inflicts no damage. Instead, the attacker grabs hold of their target and may hang on until their grip is broken. This imposes the GRAPPLED condition upon the target [#FIL], unless the target has a MAX_LIFT that would enable to carry their attacker *without encumberance*. A normal grab allows a character to hold on to their target's "middle". To grab a specific body part (throat, hand, etc.) requires a **Special Attack** to hit. This combat move cannot be combined with other attacks on a single TICK unless the character has an **Ability** that specifically allows it.

If you have someone GRAPPLED you may take an action to squeeze them. This does one **half** rounded-down (even possibly to zero) of Base HTH damage. It does not require an attack roll, however. It also causes no knock back.

Alternately, a GRAPPLED state can be escalated into `restrained`. It is resolved

Breaking out of a grapple is explained under Combat Effects[#FIL]



### Brawling weapons

Brawling weapons include improvised weapons like chairs or debris. Their damage is based on the object's weight. The chance to hit and effective range are also determined by the object's properties.

