---
icon: material/dice-d20-outline
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![Rolls](../assets/roll.png)

## Resolving Attacks

To resolve an attack, follow these steps:

The attacking player announces the attack is coming indicating the target(s) of the attack and, importantly, the damage type(s) (**Physical**, **Disruption**, etc) and their modifiers.

The target of the attack announces any defenses that apply for them to such damage types and their current AC. Activating defenses may cost PR, paid at this point.

The GM tells the attacking player about any relevant enviromental modifiers to the roll. 


<div class="gaps rounded" markdown>
:material-dice-d20:
Roll a d20: The attacker rolls a 20-sided die (d20) to determine the attack's success.
</div><div class="gaps rounded" markdown>
Apply modifiers: Add any relevant attack modifiers to the d20 roll.
</div><div class="gaps rounded" markdown>
Compare to AC: If the total of the d20 roll plus modifiers equals or exceeds the target's Armor Class (AC), the attack hits.
</div>

### Critical Hits and Fails:

- A natural 20 on the d20 (before modifiers) is a critical hit.
- A natural 1 on the d20 (before modifiers) is a critical fail.

### Advantage and Disadvantage:

- When you have advantage or disadvantage, roll additional d20s accordingly.
- Advantage and disadvantage do not cancel each other out.
- Roll all dice together, forming two pools: advantage dice (0+) and disadvantage dice (0+), plus your "prime" die.
- Defense, enviromental conditions and abilities are all potential sources of advantage and disadvantage to an attack roll.

### Crit Fishing:

Check for critical results in the dice pools:

- Advantage dice cannot provide critical fails.
- Disadvantage dice cannot provide critical hits.
- The prime die can produce both critical hits and fails normally.


Compare the number of critical hits and fails:

- If there are more critical hits than fails, the attack is a CRITICAL HIT (treat as a roll of 20).
- If there are more critical fails than hits, the attack is a CRITICAL FAIL (treat as a roll of 1).
- If there are an equal number of critical hits and fails, a DRAMATIC EVENT occurs (explained later).


If no critical results occur:

- Cancel out advantage and disadvantage dice in pairs.
- If the disadvantage pool is larger, the result is the lowest remaining die (including the prime die).
- If the advantage pool is larger, the result is the highest remaining die (including the prime die).
- If the pools are equal, the result is the prime die.



Remember:

Always apply modifiers **AFTER** determining the raw d20 result.
The prime die is treated differently than the advantage/disadvantage pools for critical hits and fails.

### Critical Hits

A critical hit always strikes the target, even if the character has immunity to the attack type. Immunity will prevent the critical effects below, however.

A critical hit also does one (only) of the following effects. Choose (unless the GM determines a target is not subject to INJURY).

#### Critical Effects

##### Maximum Damage

On a critical hit, the base damage dice of the attack are maximized. This only includes the damage component for the attack that resolved to the CRIT. Payload attacks are not automatically maximised.

##### INJURIES

If a critical hit deals hit point damage to the target, they also suffer an INJURY which imposes penalties to the character's actions and abilities. The number of wounds a character can sustain before becoming incapacitated is based on their BLOCKS.

### Critical Fails

Critical Fails are misses so bad that the effects of it cant be ignored. The GM will choose the outcome:

##### Wild shots

The shot goes stray and hits an unintended target with narrative consequences - be it innocent bystander, vulnerable gas-tank, necessary infrastructure.

##### Jammed Weapon

The GM may determine that a critical failure interrupts the flow of attacks in some determinental way. These are not meant to be too debilitating, since the potential for critical fails from a large dice pool is quite high. The jammed weapon in the worst case should only require movement to clear (not an action) or automatically resolve itself in **between turns**.



### Dramatic Events

In the heat of battle, when the dice fall just right (or wrong), a Dramatic Event occurs, offering the Game Master (GM) a unique opportunity to reshape the battlefield and inject a burst of excitement into the encounter. These moments are not meant to favor or hinder either side but rather to create a thrilling and dynamic combat experience for everyone involved.

> Dramatic Events are the perfect opportunity for an enemy to make a get-away

When a Dramatic Event is triggered, the GM momentarily pauses the action to introduce an unexpected twist that alters the landscape of the battle. The possibilities are endless, limited only by the GM's imagination and the context of the encounter. Here are some ideas to get your creative juices flowing:
<div class="gaps" markdown>
**Collapsing Structures**: A stray blast or the sheer intensity of the battle causes a nearby building, bridge, or tree to crumble, forcing combatants to scramble for new positions or risk being caught in the debris.
</div><div class="gaps" markdown>
**Elemental Interference**: A sudden burst of elemental energy erupts in the midst of the fray. A wall of fire, a localized earthquake, or a whirlwind of air disrupts the battle, creating new obstacles or opportunities for clever tactics.
</div><div class="gaps" markdown>
**Unexpected Arrivals**: A meteor crashes nearby, a portal opens, or a ship runs aground, introducing a new element to the battle. This could be a potential ally, a fearsome creature, or simply a distraction that changes the dynamic of the encounter.
</div><div class="gaps" markdown>
**Shifting Terrain**: The ground beneath the combatants' feet suddenly shifts, revealing hidden traps, unstable footing, or a previously unseen escape route. Players must adapt to the new terrain or risk losing their advantage.
</div><div class="gaps" markdown>
**Equipment Malfunction**: A weapon jams, a magical item short-circuits, or a piece of technology goes haywire, forcing characters to improvise or find alternative means of attack or defense.
</div>
When introducing a Dramatic Event, the GM should keep the following in mind:
<div class="gaps" markdown>
**Balance**: The event should not significantly advantage or disadvantage either side but rather create an opportunity for both parties to respond and adapt.
</div><div class="gaps" markdown>
**Relevance**: The event should make sense within the context of the encounter and the overall story, adding depth and immersion to the experience.
</div><div class="gaps" markdown>
**Opportunity**: Dramatic Events are a chance for players to showcase their creativity and quick thinking. Encourage them to seize the moment and come up with inventive ways to navigate the new challenge.
</div>
Remember, Dramatic Events are not meant to be a constant occurrence but rather a spice that adds flavor to combat when the dice dictate. Use them sparingly and strategically to keep players on their toes and to create truly memorable moments in your OpenHero campaign!