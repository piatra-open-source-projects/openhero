---
icon: material/nuke
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![](../assets/types.png)

## Damage Types

Every attack in the game falls into at least of these categories.
It is possible for an attack to fall into multiple categories. If the damage type specifically indicates "A OR B", then the character will have made the choice regarding damage type at character creation. If the damage type indicates "A/B", that means **both** types apply. Apply all Defenses for the attack types together.

<div class=" gaps" markdown>
For example, the Ice Blasts have type **Physical/Thermal** to account for the impact of ice and their freezing temperatures.

An atomic hand-grenade could be doing a concussive explosion of ionising radiation that is **Physical/Thermal/Photonic**.
</div>
Various powers, abilities and equipment can provide Defense against attacks of a particular type. A character's accumulated Defenses are collected and displayed on the character sheet for easy reference during the game.

Also, any attacks available to your character are summarised on your character sheet, including computed ranges, damage and PR cost where available. These attacks will also indicate which attack types are part of the attack.


<div class="rounded gaps" markdown>
### Physical

Kinetic energy, physical blows and direct impacts. **Physical** attacks account for any attempt to damage a creature or object by kinetic force, whether there is a touch occuring or not. Strong Gales, telekinetic blows, etc are all **Physical** attacks in nature.
![Physical](../assets/physical.png)

</div><div class="rounded gaps" markdown>
### Mental

**Mental** attacks encompass domination, probing and psychically exerting influence over other sentient minds. Objects are immune to **Mental**-type effects.
![Mental](../assets/mental.png)
</div><div class="rounded gaps" markdown>
### Spiritual

**Spiritual** attacks affect the subtle body and energy systems of a creature. This aspect of creatures is closely related to a character's PR reserve. Inanimate objects are immune to **Spiritual**-type attacks, however the GM may determine that some **Spiritual** attacks may influence the power source of animate objects.
![Spiritual](../assets/spiritual.png)
</div><div class="rounded gaps" markdown>
### Photonic

**Photonic** attacks are light as opposed to heat. This includes radiation in the extremes of the spectrum. Various sensors that detect light (such as night vision goggles) are typically **Vulnerable** to **Photonic**-type attacks.

![photonic](../assets/photonic.png)
</div><div class="rounded gaps" markdown>

### Electric

**Electric** attacks apply specifically to streams of electrons or positron, and bolts of lightning. Massive electric discharges have consequences for electromechanical devices. Such devices are typically **Vulnerable** to **Electric**-type attacks.
![electric](../assets/electric.png)
</div><div class="rounded gaps" markdown>
### Magnetic

**Magnetic** type attacks affect only ferro-magnetic materials in most cases. Certain materials may be engineered to ignore **Magnetic**-type attacks, although this comes at great cost.
Massive magnetic discharges have consequences for electromechanical devices. Such devices are typically **Vulnerable** to **Magnetic**-type attacks.
![magnetic](../assets/magnetic.png)
</div><div class="rounded gaps" markdown>
### Thermal

**Thermal** attacks are energy attacks involving the consequences of excessive heat or the absence thereof. The GM may determine that becoming extremely hot or cold may reduce the SR of some materials, through either melting or brittleness.
![thermal](../assets/thermal.png)
</div><div class="rounded gaps" markdown>
### Vibratory

Vibratory attacks are attacks that utilise harmonic frequencies and vibrations to induce effects in the targeted object by resonance. Resonances are particularly damaging to rigid or brittle objects.
![vibratory](../assets/vibratory.png)
</div><div class="rounded gaps" markdown>
### Disruption

Disruption attacks the bonds of molecules, the underlying nature of reality, the integrity of structures. It is an entropy attack.
![disruption](../assets/disruption.png)
</div>