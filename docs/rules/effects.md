---
icon: material/skull
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![Effects](../assets/effects.png)

## Effects and Conditions

### Knock Back

When a character takes enough **Physical** damage to their HP they may be knocked back. Divide the amount of damage taken (after AR and DR reductions) by two and then again by their BLOCK value round down. This is the number of HEX knocked back. The calculator panel in the character sheet will make a note of this value for you... ignore it if the incoming attack wasn't **Physical**.
```
FLOOR(DAM/2/BLOCK)
```

If a **flinging** attack is announced before the roll, the **entire** damage (before AR and SR) is used for the knock-back calculation, however the attack only delivers half the HP as actual damage (before applying any AR, SR or DR).
```
FLOOR(DAM/BLOCK)
```

#### Knock Back /Fall Damage

Creatures and objects knocked back take damage equivalent to a fall of half the distance.
This damage can only be rolled with if a character makes a **Agility** save. It is not possible to roll with falling damage.

 | Knock-back (HEX)   |   Fall (`m`) | Damage per BLOCK|
 |------------:|:-----------------:|:--------------|
 | 2   |  1       |    nil|
 | 4   |  2       | 1|
 | 8   |  4       |    1d2|
 | 16  |  8       |    1d4|
 | 32  |  16      |     1d6|
 | 64  | 32       |   1d8|
 | 128 |64        |     1d10|
 | 256 |  128     |    2d10|
 | 512 |  256     |     3d10|
         
### Knockout checks

Whenever a character takes HP damage, there is a chance they will fall UNCONSICOUS based on the damage taken. The percentage chance is equal to the damage received.

A **Special Attack** that targets a creatures head will increase this chance of scoring a knock-out blow by **four**. So taking 10 HP damage to the head will result in a knock-out 40% of the time. Creatures with the UNCONSCIOUS condition make saves vs **END** to rouse themselves in **between turns**. An action taken to rouse other UNCONSCIOUS creatures offers another save check on that TICK.

### Fleeing

A participant in melee combat (i.e., a fight between two characters where at least one of them attempted a melee attack against the other as their last Action) may only move away if: 

- They are able to move in a direction which their opponent cannot follow (for example, Flight allows a character to escape a melee opponent who cannot fly in most cases).
  
    **OR**
  
    They are able to move without being detected by their opponent (for example, Invisible characters can move away from melee opponents who cannot see invisible things)
  
    **OR**
  
    They have a movement speed that allows them to outpace potential pursuers.

- They are not GRAPPLED. 

If the two combatants agree to part company, then they may both leave. 



### Conditions

Here are a list of conditions that can apply to a character and these conditions will affect what actions and movements a character is able to perform.

<div class="faint gaps" markdown>
#### GRAPPLED

A GRAPPLED creature is one held in place by another character or some environmental effect.
A creature that is GRAPPLED:

- Loses 50% of its `BASE MOVE`. However, none of this remaining movement is available to actually change location - only take move-equivalent actions
- Actual location is determined by the grappler
- Attacks against any target other than the grappler have disadvantage.

</div>
<div class="faint gaps" markdown>
#### PINNED

A PINNED creature has been further restrained by an attacker beyond merely GRAPPLED. In addition to the negative effects of being GRAPPLED a creature that is PINNED also:

- Loses 100% of `BASE MOVE`.
- Unable to take any **Physical** movement or actions on their turn except **resist the grapple**. 

A PINNED character may take purely **Mental** actions or use other Abilities as allowed by the GM.


</div>

<div class="faint gaps" markdown>
#### UNCONSCIOUS

An UNCONSCIOUS creature is one that has teporarily lost the ability to act:

- MOVE 0%
- No ACTION may be taken
- Drops any held items.
- Falls to the ground prone. Airborne creatures take appropriate falling damage.
- Considered to be resting and will accumulate PR at the normal rate.

UNCONSCIOUS creatures make saves vs **Endurance** each **between turns** to end the effect. Passing the save allows normal actions on their TICK in the subsequent `TURN`. Other players may attempt to rouse an UNCONSCIOUS creature by spending an action to do so. This allows another save vs **Endurance**. If roused in this way, the creature will act in the subsequent `TURN` as usual.
</div><div class="faint gaps" markdown>
#### POISONED

A POISONED creature:

- has their movement cut by 50%
- and all usages of PR cost **double**.
</div><div class="faint gaps" markdown>

#### CONTROLLED

A CONTROLLED creature or object is under the opposed influence or manipulation of someone else:

- Actions cost double PR (minimum 1 PR) as it struggles against the domination. 
</div><div class="faint gaps" markdown>
#### FATIGUED

A character with no PR remaining is FATIGUED

- Their movement drops to 10%.
- They may make use of only 1 TICK each `TURN`.
- Actions requiring PR damages HP the instead.
</div><div class="faint gaps" markdown>

#### INCAPACITATED

A character with no HP remaining is INCAPACITATED:

- MOVE 0%
- No ACTION may be taken
- Drops any held items.
- Falls to the ground prone. Airborne creatures take appropriate falling damage.
- Additional damage is taken to their PR.

An incapacitated character is essentially in a comatose condition until it is provided medical aid.

This medical aid could be a save vs **Endurance** initiated after some other character spends an action on the check. If successful, the INCAPACITATED character regains 1 HP and has the POISONED condition until proper recovery is possible.
</div><div class="faint gaps" markdown>

#### DYING

A character with no HP or PR remaining is DYING. 

A DYING character is unable to be revived without medical attention or some ability that heals damage.

A full days medical intervention will allow the DYING character to become INCAPACITATED instead.

Without medical attention a DYING character will expire in 4d4 hours.

</div><div class="faint gaps" markdown>
#### DEAD

Additional damage to a DYING character that exceeds their BLOCKS will kill them beyond recovery.

Once DEAD only miraculous powers or GM intervention can save the character.
</div>

