---
title: Derived Stats
template: nocontent.html
icon: material/calculator
---

<h1 class="rules_title">Getting Started...</h1>

## Derived Stats

The basic stats that have already been entered, plus the abilities your character possesses combine to determine many other derived quantities.

For example, your strength, endurance and weight will determine how much you are able to lift and this, in turn, will determine the amount of damage your punches do. Similarly, there are computations to determine how far characters can jump, how fast they move, how well they are liked, and so on.

All these other stats and capabilities - derived from the basic stats and your powers - are calculated for you by the character builder and presented on the character sheet. There is no need for you to track these numbers or recalculate should your stats change. For completeness, as well as a glimpse under the hood at some of the game balance issues, the method of determining all these derived stats are given here:


<div class="rounded gaps" markdown>
### Power Reserve
Your **Power Reserve** (PR) is a broad measure of your characters energy, motivation and fatigue levels. PR is a resource consumed to take actions. Strenuous actions will cost more PR than easy ones. Throwing a punch costs 1 PR. Changing your facing or position out of turn takes 2 PR. Blasting a super-powered energy beam from your eyes could take  1 PR or higher depending on the ability used.

Your PR replenishes quickly through rest [FIL]. When you are out of PR your actions are extremely limited and you become FATIGUED [FIL]. Your PR is the sum of all basic stats except **Charisma**.
```
S + E + A + I
```
</div>
<div class="rounded gaps" markdown>
### Hit Points
Your **Hit Points** (HP) are the amount of damage your character can take before becoming INCAPACITATED [FIL]. The calculation is given as
```
2 * WEIGHT/25 
* ADJ(E, 1) * ADJ(S, 1/2) * ADJ(A, 1/3) * ADJ(I>9, 1/4) 
* ([LEVEL - 1] * 0.1 + 1)
```

where 
```
ADJ(X, Y) = (Y * (X - 10)/10) + 1
```

Let's break it down... `WEIGHT/25` represents how many 25 kg chunks of meat make up the characters body. Each of those meat BLOCKS will start with 2 HP.

There are scaling adjustments for `S`, `E`, `A` and `I`. **Charisma** does not affect HP. These scalings use the `ADJ(stat, rate)` function. This is a linear scaling centered on `stat=10`, which will always have the value of 1. Increasing the `stat` will increase the scaling based on the `rate`. **Endurance** has the most pronounced influence upon the HP. The scaling will double by `E=20`. **Strength** has the next highest influence, doubled by `S=30`. **Agility** will double HP by `A=40`. Similarly, **Intelligence** will double HP at `I=50`.

> Low stats (typically below 9) may result in a reduction to total HP, except **Intelligence**. Even creatures of animal intelligence and non-sentient creatures are able to focus their available mental resources on self preservation.

The final modification to HP is from your character's LEVEL. This is a small boost, only 10% of your HP for every level increase past the first.
</div>
<div class="rounded gaps" markdown>
### Max Lift
Your LIFT is the amount of mass in kilograms that your character can lift and hold. For a normal human this is equivalent to their **Weight** `W`. High **Endurance** enables greater lifts that normal. High **Strength** however has a substantially greater weighted effect.
```
WEIGHT * [(S/10)^3 + (E/10)] / 2
```

### Encumberance
Characters are able to move freely whilst carrying things up to 50% of their MAX_LIFT.  This number is recorded in BLOCKS on the character sheet. Up to this point they may move freely without restrictions. Above this threshold, however, and the character begins to accumulate movement and jumping penalties.

</div>
<div class="rounded gaps" markdown>
### Base HTH Damage
Your `BASE HTH` is the amount of **Physical** damage you do with your fists and strikes. It is calculated on a sliding scale based on the characters LIFT value. Consult the following table for the increments. You will notice that the growth is geometric, that the number and size of the dice both scale.

|  LIFT kg | Base HTH  |
|-----------:|-----------|
| 10         | 1         |
| 25         | 1d2       |
| 50         | 1d4       |
| 100        | 1d6       |
| 250        | 1d8       |
| 500        | 1d10      |
| 1000       | 2d6       |
| 2500       | 2d8       |
| 5000       | 2d10      |
| 10000      | 4d8       |
| 25000      | 4d10      |
| 50000      | 4d12      |
| 100000     | 6d10      |
| 250000     | 6d12      |
| 500000     | 6d20      |
| 1000000    | 8d12      |
| 2500000    | 8d20      |
| 5000000    | 8d20+d12  |
| 10000000   | 10d20     |
| 25000000   | 10d20+d12 |
| 50000000   | 10d20+2d12|
| 100000000  | 12d20     |

</div>
<div class="rounded gaps" markdown>
### Blocks
Your BLOCKS is a convenient number that represents the number of mass/volume units of your body. It is used in-game to determine certain things like how far you get flung by massive blows, how easy you are to catch, how much damage you take from falling, etc.

Fractions round up.
```
CEIL(WEIGHT/25);
```
</div>
<div class="rounded gaps" markdown>
### Melee TH
Your `MELEE TH` is a "to hit" bonus applied when rolling to your melee hand-to-hand **Physical** attacks that comes from extreme **Strength** (`S`) scores. It represents the ability to redirect forceful blows and push past defenses. The character sheet will include this value in your attack profile for you.
```
ROUND(ADJ(S, 3/4) - 1);
```
</div>
<div class="rounded gaps" markdown>
### Range TH
Your `RANGE TH` is a "to hit" bonus applied when rolling to your ranged  **Physical** attacks that comes from extreme **Agility** (`A`) scores. It represents the ability to hold aim and exercise fine-motor control. The character sheet will include this value in your attack profile for you.
```
ROUND(ADJ(A, 3/4) - 1);
```
</div>
<div class="rounded gaps" markdown>
### HTH Bonus
The `HTH BONUS` is rare bonus to the damage of all **Physical** attacks. Normally damage rolls are left unaffected by flat bonuses because HP do not steeply scale with levels. This bonus damage represents precision damage available due to high **Intelligence**.

The character with this bonus may always choose whether to apply it or not, unless it is a damage penalty (in which case it always applies). Consult the following table to determine the bonus (or penalty) if any:

| Intelligence |     `HTH BONUS` |
|---------:|------------|
| Nonsentient   |–         |
| 1-2       | -2         |
| 3-8       | -1         |
| 9-20       | –        |
| 21-38      | +1        |
| 39-59      | +1d2        |
| 60-80      | +1d4       |
| 81+     | +1d6       |

</div>
<div class="rounded gaps" markdown>
### Gizmos
The GIZMO number is the number of devices, solutions and macguyvered fixes your character is able to improvise. See GIZMOS [FIL] on what they are and how they are used.
```
I / 10 * LEVEL
```
</div>
<div class="rounded gaps" markdown>
### Vibe
Your character's `VIBE` is a representation of how well they will usually get along with people who consider them to be on the same "team". It is an indication of how happy a person teammate is to be on the same team as you.  Equivalently the vibe check swaps for people on the other "team", so a strong positive reaction from allies will illicit a strong negative reaction from enemies. Naturally, this is determined by your **Charisma**.

There is no modifier for characters with average (9-11) **Charisma**. The penalties for low **Charisma** are disproportionately harsher than the rewards for high **Charisma**. Such is life. The `VIBE` increases by +1 for every three points outside this range.
```
C < 9: ROUND(ADJ(C, 5) - 1)
C >=9: ROUND(ADJ(C, 3) - 1)
```
</div>
<div class="rounded gaps" markdown>
### Detect %

Your DETECT represents your danger sense and the chance to notice hidden things. The GM may ask for this roll to see if you are able to spot and ambush or get handed a free clue. Normal chance for such things is 10%, but characters with high **Intelligence** will have a higher chance to notice things. According to the formula below a character with 25 **Intelligence** would have a DETECT% of 20%, linearly scaling with higher values.

```
ROUND(ADJ(I, 2/3)*10)
```
</div>
<div class="rounded gaps" markdown>
### Heal
Your character's HEAL is the percentage of total HP that a character recovers as part of a standard healing allotment after a nights rest. Normal character will receive 10% of their total HP per allotment, however characters with exceptional **Endurance** heal faster. According to the formula below a character with 25 **Endurance** would have a HEAL of 20%, linearly scaling with higher values.
```
ADJ(E, 2/3) * 10;
```
</div>
<div class="rounded gaps" markdown>
### Max TICKS
Your character's `MAX TICKS` is the number of TICKS in a `TURN` that they may take advantage of given sufficiently high **Agilitiy**. However there is a hard limit of **TWO** TICKS per character at low levels. This expands to allow an extra TICK to be exploited at Level 5, 10, 15, so on...
```
FLOOR(LEVEL/5) + 2
```
</div>