---
icon: material/bottle-tonic-plus-outline
template: nocontent.html
---

<h1 class="rules_title">OpenHero Role playing game</h1>

![Healing](../assets/heal.png)

## Healing and Recovery

### Recovering power

Characters regain POWER through rest. For each minute of rest, a character regains one POWER, up to their maximum score. Strenuous activity prevents POWER recovery, and any activity that has a PR cost is likely to prevent the gains of rest for that minute.

One exception is maintaing abilities that require 1 PR per minute (or less) to maintaint. These activities are not considered strenuous and do not prevent the gains of rest. This means that the 1 POWER gained can be passed across to pay the maintenace PR for the ability instead of repleneishing the characters POWER.

Characters who remain active past normal sleep costs 2 PR which can only be regained by sleep.

### Healing HP

Characters naturally heal a number of HP per day based on their Endurance. Medical treatment or abilities can increase this healing rate.

### Repairing INJURIES

INJURIES require extended time to heal, typically one week per INJURY, unless advanced medical care is available. Regenerative abilities may allow for faster INJURY recovery.

### Repairing or healing armor

Biological Armour regrows at the rate specified under the power that provides it.. Normal 3 times the healing allotment. Technical armour requires $100 per point of armour and n days to fix.

Repairing constructs takes simailr taime.

Android Body, Robot Body and Bionics have their own repair costs.

