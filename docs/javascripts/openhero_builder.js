let powers = []; // Global variable to store the powers data
let currentIndex = 0; // which of mulitple is shown
let libraryData = [];
let hasValidImage = false;
let is_evading = false;

var currentX;
var currentY;
var initialX;
var initialY;
var xOffset = 0;
var yOffset = 0;
var activeModal = null;

let client;
let mqtt_connected = false;
let reconnectInterval;
let reconnectAttempts = 0;
let heartbeatInterval;
const HEARTBEAT_INTERVAL = 30000; // 30 seconds

let damage_queue = [];

let networkSettings = {
  broker: "broker.emqx.io",
  port: "8084",
  username: "player",
  password: "OpenHero_v1.0",
  game_id: "",
  player_id: ""
};

const gameSettings = {
  min_effective_weight: 25,
  max_effective_weight: 100000,
  hp_level_scaling: 0.1,
  kilo_per_block: 25,
  hp_per_flesh_block: 2,
  base_ac: 10,
  base_hth: -2,
  crit_range: 1,
  crit_fishing: true,
  max_dice_pool: 4,
  dis_adv_negate: false
};
const tableTemplate = [
  {
    id: "checkboxTable",
    tag: null,
    class: "power-checkbox",
    title: "Utility Powers"
  },
  {
    id: "weakCheckboxTable",
    tag: "weak",
    class: "weak-checkbox",
    title: "Weaknesses"
  },
  {
    id: "equipCheckboxTable",
    tag: "equip",
    class: "equip-checkbox",
    title: "Gear"
  },
  {
    id: "skillCheckboxTable",
    tag: "skill",
    class: "skill-checkbox",
    title: "Skills"
  }
];

function get_vibe(ability) {
  if (ability < 9) return Math.round(adj(ability, 5) - 1);
  return Math.round(adj(ability, 3) - 1);
}

/**
 * Gets the agility hit point modifier based on the given ability.
 * @param {number} ability - The agility ability score.
 * @returns {number} - The agility acceleration modifier.
 */
function get_agil_hp_mod(ability) {
  return adj(ability, 1 / 3);
}

/**
 * Gets the agility accuracy modifier based on the given ability.
 * @param {number} ability - The agility ability score.
 * @returns {number} - The agility acceleration modifier.
 */

function get_acc_mod(ability) {
  return Math.round(adj(ability, 3 / 4) - 1);
}

/**
 * Gets the strength hit point modifier based on the given ability.
 * @param {number} ability - The agility ability score.
 * @returns {number} - The agility acceleration modifier.
 */
function get_str_hp_mod(ability) {
  return adj(ability, 1 / 2);
}

/**
 * Gets the endurance hit point modifier based on the given ability.
 * @param {number} ability - The agility ability score.
 * @returns {number} - The agility acceleration modifier.
 */
function get_end_hp_mod(ability) {
  return adj(ability, 1);
}

/**
 * Gets the endurance healing modifier based on the given ability.
 * @param {number} ability - The agility ability score.
 * @returns {number} - The agility acceleration modifier.
 */

function get_end_heal(ability) {
  return adj(ability, 2 / 3) * 10;
}

/**
 * Gets the intelligence hit point modifier based on the given ability.
 * @param {number} ability - The agility ability score.
 * @returns {number} - The agility acceleration modifier.
 */
function get_int_hp_mod(ability) {
  if (ability < 9) return 1;
  return adj(ability, 1 / 4);
}

/**
 * Gets the intelligence damage modifier based on the given ability.
 * @param {number} ability - The agility ability score.
 * @returns {number} - The agility acceleration modifier.
 */
function get_int_dam_mod(ability) {
  const abilityModifierData = [
    { minAbility: -1, maxAbility: 20, int_dam_mod: "" },
    { minAbility: 21, maxAbility: 38, int_dam_mod: "+1" },
    { minAbility: 39, maxAbility: 59, int_dam_mod: "+d2" },
    { minAbility: 60, maxAbility: 80, int_dam_mod: "+d4" },
    { minAbility: 81, maxAbility: -1, int_dam_mod: "+d6" }
  ];

  const result = abilityModifierData.find(
    (range) => ability >= range.minAbility && ability <= range.maxAbility
  );
  return result ? result.int_dam_mod : "";
}

/**
 * Gets the detect hidden percentage based on the given ability.
 * @param {number} ability - The agility ability score.
 * @returns {number} - The agility acceleration modifier.
 */
function get_detect(ability) {
  return Math.round(adj(ability, 2 / 3) * 10);
}

const weightModifierData = [
  {
    min: 0,
    max: 10,
    agil_adj: 4,
    base_hth: "1",
    range_mod: 0,
    brawl_acc_mod: 0,
    range: "A",
    velocity_dam: "+1",
    brawl_wt_dam: ""
  },
  {
    min: 11,
    max: 25,
    agil_adj: 2,
    base_hth: "1",
    range_mod: -1,
    brawl_acc_mod: 0,
    range: "A * 2",
    velocity_dam: "+1d2",
    brawl_wt_dam: "+1d4"
  },
  {
    min: 26,
    max: 50,
    agil_adj: 1,
    base_hth: "1d2",
    range_mod: -2,
    brawl_acc_mod: 1,
    range: "A * 3",
    velocity_dam: "+1d4",
    brawl_wt_dam: "+1d8"
  },
  {
    min: 51,
    max: 100,
    agil_adj: 0,
    base_hth: "1d4",
    range_mod: -3,
    brawl_acc_mod: 1,
    range: "A * 4",
    velocity_dam: "+1d6",
    brawl_wt_dam: "+2d8"
  },
  {
    min: 101,
    max: 250,
    agil_adj: -1,
    base_hth: "1d6",
    range_mod: -4,
    brawl_acc_mod: 2,
    range: "A * 5",
    velocity_dam: "+1d8",
    brawl_wt_dam: "+2d10"
  },
  {
    min: 251,
    max: 500,
    agil_adj: -2,
    base_hth: "1d8",
    range_mod: -5,
    brawl_acc_mod: 2,
    range: "A * 6",
    velocity_dam: "+1d10",
    brawl_wt_dam: "+3d10"
  },
  {
    min: 501,
    max: 1000,
    agil_adj: -3,
    base_hth: "1d10",
    range_mod: -6,
    brawl_acc_mod: 3,
    range: "A * 7",
    velocity_dam: "+2d6",
    brawl_wt_dam: "+4d10"
  },
  {
    min: 1001,
    max: 2500,
    agil_adj: -4,
    base_hth: "2d6",
    range_mod: -7,
    brawl_acc_mod: 3,
    range: "A * 8",
    velocity_dam: "+2d8",
    brawl_wt_dam: "+5d10"
  },
  {
    min: 2501,
    max: 5000,
    agil_adj: -5,
    base_hth: "2d8",
    range_mod: -8,
    brawl_acc_mod: 4,
    range: "A * 9",
    velocity_dam: "+2d10",
    brawl_wt_dam: "+6d10"
  },
  {
    min: 5001,
    max: 10000,
    agil_adj: -6,
    base_hth: "2d10",
    range_mod: -9,
    brawl_acc_mod: 4,
    range: "A * 10",
    velocity_dam: "+4d8",
    brawl_wt_dam: "+7d10"
  },
  {
    min: 10001,
    max: 25000,
    agil_adj: -7,
    base_hth: "4d8",
    range_mod: -10,
    brawl_acc_mod: 5,
    range: "A * 11",
    velocity_dam: "+4d10",
    brawl_wt_dam: "+8d10"
  },
  {
    min: 25001,
    max: 50000,
    agil_adj: -8,
    base_hth: "4d10",
    range_mod: -11,
    brawl_acc_mod: 5,
    range: "A * 12",
    velocity_dam: "+4d12",
    brawl_wt_dam: "+9d10"
  },
  {
    min: 50001,
    max: 100000,
    agil_adj: -9,
    base_hth: "4d12",
    range_mod: -12,
    brawl_acc_mod: 6,
    range: "A * 13",
    velocity_dam: "+6d10",
    brawl_wt_dam: "+10d10"
  },
  {
    min: 100001,
    max: 250000,
    agil_adj: -10,
    base_hth: "6d10",
    range_mod: -13,
    brawl_acc_mod: 6,
    range: "A * 14",
    velocity_dam: "+6d12",
    brawl_wt_dam: "+11d10"
  },
  {
    min: 250001,
    max: 500000,
    agil_adj: -11,
    base_hth: "6d12",
    range_mod: -14,
    brawl_acc_mod: 7,
    range: "A * 15",
    velocity_dam: "+6d20",
    brawl_wt_dam: "+12d10"
  },
  {
    min: 500001,
    max: 1000000,
    agil_adj: -12,
    base_hth: "6d20",
    range_mod: -15,
    brawl_acc_mod: 7,
    range: "A * 16",
    velocity_dam: "+8d12",
    brawl_wt_dam: "+13d10"
  },
  {
    min: 1000001,
    max: 2500000,
    agil_adj: -13,
    base_hth: "8d12",
    range_mod: -16,
    brawl_acc_mod: 8,
    range: "A * 17",
    velocity_dam: "+8d20",
    brawl_wt_dam: "+14d10"
  },
  {
    min: 2500001,
    max: 5000000,
    agil_adj: -14,
    base_hth: "8d20",
    range_mod: -17,
    brawl_acc_mod: 8,
    range: "A * 18",
    velocity_dam: "+8d20+d12",
    brawl_wt_dam: "+15d10"
  },
  {
    min: 5000001,
    max: 10000000,
    agil_adj: -15,
    base_hth: "8d20+d12",
    range_mod: -18,
    brawl_acc_mod: 9,
    range: "A * 19",
    velocity_dam: "+10d20",
    brawl_wt_dam: "+16d10"
  },
  {
    min: 10000001,
    max: 25000000,
    agil_adj: -16,
    base_hth: "10d20",
    range_mod: -19,
    brawl_acc_mod: 9,
    range: "A * 20",
    velocity_dam: "+10d20+d12",
    brawl_wt_dam: "+17d10"
  },
  {
    min: 25000001,
    max: 50000000,
    agil_adj: -17,
    base_hth: "10d20+d12",
    range_mod: -20,
    brawl_acc_mod: 10,
    range: "A * 21",
    velocity_dam: "+10d20+2d12",
    brawl_wt_dam: "+18d10"
  },
  {
    min: 50000001,
    max: 100000000,
    agil_adj: -18,
    base_hth: "10d20+2d12",
    range_mod: -21,
    brawl_acc_mod: 10,
    range: "A * 22",
    velocity_dam: "+12d20",
    brawl_wt_dam: "+19d10"
  }
];

/**
 * Gets the base hand-to-hand damage based on the given weight.
 * @param {number} weight - The weight value.
 * @returns {string} - The base hand-to-hand damage value or 'nil' if not found.
 */

function get_base_hth(weight) {
  const modifier = weightModifierData.find(
    (range) => weight >= range.min && weight < range.max + 1
  );
  return modifier ? modifier.base_hth : "";
}

function get_agil_adj(weight) {
  const modifier = weightModifierData.find(
    (range) => weight >= range.min && weight < range.max
  );
  return modifier ? modifier.agil_adj : 0;
}

function get_range_mod(distance) {
  const modifier = weightModifierData.find(
    (range) => distance >= range.min && distance < range.max
  );
  return modifier ? modifier.range_mod : 0;
}

function get_max_range(distance) {
  const modifier = weightModifierData.find(
    (range) => distance >= range.min && distance < range.max
  );
  return modifier ? modifier.range : "A";
}

function get_brawl_acc_mod(distance) {
  const modifier = weightModifierData.find(
    (range) => distance >= range.min && distance < range.max
  );
  return modifier ? modifier.brawl_acc_mod : 0;
}
function get_brawl_wt_dam(distance) {
  const modifier = weightModifierData.find(
    (range) => distance >= range.min && distance < range.max
  );
  return modifier ? modifier.brawl_wt_dam : "";
}

function get_vel_dam_mod(velocity) {
  const modifier = weightModifierData.find(
    (range) => velocity >= range.min && velocity < range.max
  );
  return modifier ? modifier.velocity_dam : "";
}

// Define your JSON schema
const schema = {
  type: "object",
  properties: {
    name: { type: "string" },
    identity: { type: "string" },
    agility: { type: "number" },
    endurance: { type: "number" },
    strength: { type: "number" },
    spent_power: { type: "number" },
    armor_rating: { type: "number" },
    health: { type: "number" },
    intelligence: { type: "number" },
    charisma: { type: "number" },
    weight: { type: "number" },
    level: { type: "number" },
    powers: { type: "array", items: { type: "string" } },
    overrides: { type: "array", items: { type: "object" } },
    background: { type: "string" },
    notes: { type: "string" }
  },
  required: [
    "name",
    "agility",
    "endurance",
    "strength",
    "intelligence",
    "charisma",
    "weight"
  ]
};

// Initialize the Ajv validator
const ajv = new Ajv();
const validate = ajv.compile(schema);

// Global character data structure
let initialData = {
  name: "default",
  identity: "default",
  agility: 10,
  endurance: 10,
  strength: 10,
  intelligence: 10,
  charisma: 10,
  weight: 65,
  level: 1,
  spent_power: 0,
  armor_rating: 0,
  health: 100.0,
  powers: [],
  background: "",
  notes: "",
  overrides: [],
  image: "default.png"
};

let modData = {
  agility: 0,
  endurance: 0,
  strength: 0,
  intelligence: 0,
  charisma: 0,
  mv: 0,
  ac: 0,
  ar: 0,
  sr: 0,
  dr: 0,
  wt: "",
  attacks: [],
  immune: [],
  resist: [],
  vulnerable: []
};

let current = {
  STRENGTH: 0,
  ENDURANCE: 0,
  AGILITY: 0,
  INTELLIGENCE: 0,
  CHARISMA: 0,
  BLOCKS: 1,
  WEIGHT: 1,
  MAX_HP: 0,
  MAX_POWER: 0,
  MAX_LIFT: 0,
  MOVE: 0
};

let cookieData = {
  color: "#ff8c00",
  save: true,
  showAdmonition: false,
  num_advantage: 0,
  num_disadvantage: 0
};

// Function to add a single row to the attack array table
function buildAttackRow(attack) {
  const { name, type, damage, range, pr, th } = attack;

  // Append the attack data to modData.attacks array
  modData.attacks.push({
    name: name,
    th: th,
    damage: damage,
    range: range,
    pr: pr,
    type: type
  });

  const tableBody = document.querySelector("#attack_array tbody");
  const row = document.createElement("tr");

  const powerNameCell = document.createElement("td");
  powerNameCell.textContent = name;
  const attackModCell = document.createElement("td");
  attackModCell.textContent = th;

  const typeCell = document.createElement("td");
  typeCell.textContent = type;

  const damageCell = document.createElement("td");
  damageCell.textContent = damage;

  const rangeCell = document.createElement("td");
  rangeCell.textContent = range;

  const powerCostCell = document.createElement("td");
  powerCostCell.classList.add("clickable-cell");
  powerCostCell.textContent = formatPower(pr);
  powerCostCell.setAttribute("data-power", name);
  powerCostCell.addEventListener("click", handleClickableCellClick);

  row.appendChild(powerNameCell);
  row.appendChild(typeCell);
  row.appendChild(attackModCell);
  row.appendChild(damageCell);
  row.appendChild(rangeCell);
  row.appendChild(powerCostCell);

  tableBody.appendChild(row);
}

function formatPower(inpuz) {
  // return parseInt(inpuz).toFixed(0)
  return inpuz;
}
// Function to generate checkboxes using the global powers data

function buildCheckboxes() {
  const buildCheckboxesContainer = document.getElementById("build-checkboxes");
  buildCheckboxesContainer.innerHTML = ""; // Clear the container

  // Sort the array by name
  powers.sort((a, b) => {
    const aName = a.name ? a.name.toLocaleLowerCase() : "";
    const bName = b.name ? b.name.toLocaleLowerCase() : "";
    return aName.localeCompare(bName);
  });

  tableTemplate.forEach(({ id, tag, class: className, title }) => {
    const tableDiv = document.createElement("div");
    tableDiv.id = id;

    const titleElement = document.createElement("h3");
    titleElement.textContent = title;
    tableDiv.appendChild(titleElement);

    const tableElement = document.createElement("table");
    tableElement.classList.add("checkbox-table");

    const filteredPowers = powers.filter((power) => {
      const powerType = power.type ? power.type.toLowerCase() : "";
      return tag === null || powerType.includes(tag);
    });

    filteredPowers.forEach((power, index) => {
      if (index % 4 === 0) {
        let row = document.createElement("tr");
        row.classList.add("check-row");
        tableElement.appendChild(row);
      }

      let cell = document.createElement("td");
      cell.classList.add("check-row");

      let checkbox = document.createElement("input");
      checkbox.type = "checkbox";
      checkbox.value = index.toString(16).padStart(2, "0");
      checkbox.setAttribute("data-label", power.name);
      checkbox.onchange = function () {
        handleCheckboxChange(this);
      };
      checkbox.classList.add(className);

      let label = document.createElement("label");
      label.classList.add("check-cell");
      label.textContent = power.name;

      cell.appendChild(checkbox);
      cell.appendChild(label);

      const currentRow = tableElement.lastElementChild;
      currentRow.appendChild(cell);
    });

    tableDiv.appendChild(tableElement);
    buildCheckboxesContainer.appendChild(tableDiv);
  });
}
function debounce(func, delay) {
  let timeoutId;
  return function (...args) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => func.apply(this, args), delay);
  };
}

const green_dice =
  '<svg class="adv" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.47 6.62L12.57 2.18C12.41 2.06 12.21 2 12 2S11.59 2.06 11.43 2.18L3.53 6.62C3.21 6.79 3 7.12 3 7.5V16.5C3 16.88 3.21 17.21 3.53 17.38L11.43 21.82C11.59 21.94 11.79 22 12 22S12.41 21.94 12.57 21.82L20.47 17.38C20.79 17.21 21 16.88 21 16.5V7.5C21 7.12 20.79 6.79 20.47 6.62M11.45 15.96L6.31 15.93V14.91C6.31 14.91 9.74 11.58 9.75 10.57C9.75 9.33 8.73 9.46 8.73 9.46S7.75 9.5 7.64 10.71L6.14 10.76C6.14 10.76 6.18 8.26 8.83 8.26C11.2 8.26 11.23 10.04 11.23 10.5C11.23 12.18 8.15 14.77 8.15 14.77L11.45 14.76V15.96M17.5 13.5C17.5 14.9 16.35 16.05 14.93 16.05C13.5 16.05 12.36 14.9 12.36 13.5V10.84C12.36 9.42 13.5 8.27 14.93 8.27S17.5 9.42 17.5 10.84V13.5M16 10.77V13.53C16 14.12 15.5 14.6 14.92 14.6C14.34 14.6 13.86 14.12 13.86 13.53V10.77C13.86 10.18 14.34 9.71 14.92 9.71C15.5 9.71 16 10.18 16 10.77Z" /></svg>';
const red_dice =
  '<svg class="disadv" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.47 6.62L12.57 2.18C12.41 2.06 12.21 2 12 2S11.59 2.06 11.43 2.18L3.53 6.62C3.21 6.79 3 7.12 3 7.5V16.5C3 16.88 3.21 17.21 3.53 17.38L11.43 21.82C11.59 21.94 11.79 22 12 22S12.41 21.94 12.57 21.82L20.47 17.38C20.79 17.21 21 16.88 21 16.5V7.5C21 7.12 20.79 6.79 20.47 6.62M11.45 15.96L6.31 15.93V14.91C6.31 14.91 9.74 11.58 9.75 10.57C9.75 9.33 8.73 9.46 8.73 9.46S7.75 9.5 7.64 10.71L6.14 10.76C6.14 10.76 6.18 8.26 8.83 8.26C11.2 8.26 11.23 10.04 11.23 10.5C11.23 12.18 8.15 14.77 8.15 14.77L11.45 14.76V15.96M17.5 13.5C17.5 14.9 16.35 16.05 14.93 16.05C13.5 16.05 12.36 14.9 12.36 13.5V10.84C12.36 9.42 13.5 8.27 14.93 8.27S17.5 9.42 17.5 10.84V13.5M16 10.77V13.53C16 14.12 15.5 14.6 14.92 14.6C14.34 14.6 13.86 14.12 13.86 13.53V10.77C13.86 10.18 14.34 9.71 14.92 9.71C15.5 9.71 16 10.18 16 10.77Z" /></svg>';
const shield =
  '<svg class="shield" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,1L3,5V11C3,16.55 6.84,21.74 12,23C17.16,21.74 21,16.55 21,11V5M11,7H13V13H11M11,15H13V17H11" /></svg>';
const smalldot =
  '<svg class="smalldot" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,10A2,2 0 0,0 10,12C10,13.11 10.9,14 12,14C13.11,14 14,13.11 14,12A2,2 0 0,0 12,10Z" /></svg>';

function adj(X, Y) {
  return (Y * (X - 10)) / 10 + 1;
}

function drawDefenseMatrixByType(powerArray, prefix, containerElement) {
  if (Array.isArray(powerArray)) {
    powerArray.forEach((p) => {
      const powerName = p.split(" ")[0].toLowerCase();
      const elementId = `${prefix}_${powerName}`;
      const element = containerElement.querySelector(`#${elementId}`);

      if (element) {
        let svgSource;
        let fillColor;

        // const isSlateMode = document.body.classList.contains('slate')
        if (prefix === "imm") {
          svgSource = shield;
        } else if (prefix === "vuln") {
          svgSource = green_dice;
        } else {
          svgSource = red_dice;
        }
        element.innerHTML = `<span class="twemoji ${prefix}">${svgSource}</span>`;

        // const svgBlob = new Blob([modifiedSvgSource], { type: 'image/svg+xml' })
        // const svgUrl = URL.createObjectURL(svgBlob)

        // const img = document.createElement('img')
        // img.classList.add(prefix)
        // img.src = svgUrl

        // element.replaceChildren(img)
      }
    });
  }
}

function evaluateExpression(exp) {
  // console.warn(`Incoming expression is ${exp}`)
  // Regular expression pattern for allowed characters
  const allowedChars = /^[a-zA-Z0-9\s+\-*/%(){}[\].,]+$/;

  // Check if the expression contains only allowed characters
  if (!allowedChars.test(exp)) {
    console.warn(`Invalid characters. Returning the original: ${exp}`);
    return exp;
  }

  const variableMap = {
    A: current.AGILITY,
    a: Math.floor(current.AGILITY / 10),
    E: current.ENDURANCE,
    e: Math.floor(current.ENDURANCE / 10),
    S: current.STRENGTH,
    s: Math.floor(current.STRENGTH / 10),
    I: current.INTELLIGENCE,
    i: Math.floor(current.INTELLIGENCE / 10),
    C: current.CHARISMA,
    c: Math.floor(current.CHARISMA / 10),
    L: initialData.level,
    B: current.BLOCKS,
    W: current.WEIGHT,
    M: current.MOVE,
    P: current.MAX_POWER - initialData.spent_power,
    H: gameSettings.base_hth + get_acc_mod(current.STRENGTH),
    R: gameSettings.base_hth + get_acc_mod(current.AGILITY)
  };

  // Validate that all values in the variableMap are numbers
  const areAllValuesNumbers = Object.values(variableMap).every(
    (value) => typeof value === "number"
  );

  if (!areAllValuesNumbers) {
    console.error("Error: All values in the variableMap must be numbers.");
    return 0;
  }
  const expString = String(exp).replace(/x/g, "*");
  // console.debug(`Replace x with *: ${expString}`)

  // Replace variables in the expression with their corresponding values
  const evaluableExpression = expString.replace(
    /[A-Za-z]+/g,
    (match) => variableMap[match]
  );
  // console.debug(`Output after variable replace: ${evaluableExpression}`)
  try {
    const result = eval(evaluableExpression);
    // console.debug(`evaluates to: ${result}`)
    return result;
  } catch (error) {
    console.error("Error parsing JSON data:", error);
    return exp;
  }
}

function evaluateNewExpression(exp) {
  console.warn(`Incoming expression is ${exp}`);
  // Regular expression pattern for allowed characters
  const allowedChars = /^[a-zA-Z0-9\s+\-*/%(){}[\].,]+$/;

  // Check if the expression contains only allowed characters
  if (!allowedChars.test(exp)) {
    console.warn(
      "Expression contains invalid characters. Returning the original string."
    );
    return exp;
  }

  const variableMap = {
    A: current.AGILITY,
    a: Math.floor(current.AGILITY / 10),
    E: current.ENDURANCE,
    e: Math.floor(current.ENDURANCE / 10),
    S: current.STRENGTH,
    s: Math.floor(current.STRENGTH / 10),
    I: current.INTELLIGENCE,
    i: Math.floor(current.INTELLIGENCE / 10),
    C: current.CHARISMA,
    c: Math.floor(current.CHARISMA / 10),
    L: initialData.level,
    B: current.BLOCKS,
    W: current.WEIGHT,
    M: current.MOVE,
    P: current.MAX_POWER - initialData.spent_power,
    H: gameSettings.base_hth + get_acc_mod(current.STRENGTH),
    R: gameSettings.base_hth + get_acc_mod(current.AGILITY)
  };

  // Validate that all values in the variableMap are numbers
  const areAllValuesNumbers = Object.values(variableMap).every(
    (value) => typeof value === "number"
  );

  if (!areAllValuesNumbers) {
    console.error("Error: All values in the variableMap must be numbers.");
    return 0;
  }
  const expString = String(exp).replace(/x/g, "\\*");
  // console.debug(`Replace x with *: ${expString}`)

  // Replace variables in the expression with their corresponding values
  const evaluableExpression = expString.replace(
    /[A-Za-z]+/g,
    (match) => variableMap[match]
  );
  // console.debug(`Output after variable replace: ${evaluableExpression}`)
  const operators = ["-", "+", "/", "*", "%"];
  const pairs = evaluableExpression.match(/[*/%+-]?[0-9]+(\.[0-9]+)?/g);

  if (pairs === null) {
    return evaluableExpression;
  }

  const leadingNumber = pairs[0].match(/^[0-9]+(\.[0-9]+)?/)
    ? parseFloat(pairs.shift())
    : 0;

  const sortedPairs = pairs.sort((a, b) => {
    const operatorA = a.charAt(0);
    const operatorB = b.charAt(0);
    const operatorIndexA = operators.indexOf(operatorA);
    const operatorIndexB = operators.indexOf(operatorB);

    if (operatorIndexA !== operatorIndexB) {
      return operatorIndexA - operatorIndexB;
    }

    const numberA = parseFloat(a.slice(1));
    const numberB = parseFloat(b.slice(1));
    return numberA - numberB;
  });

  let result = leadingNumber;

  sortedPairs.forEach((pair) => {
    const operator = pair.charAt(0);
    const number = parseFloat(pair.slice(1));

    switch (operator) {
      case "-":
        result -= number;
        break;
      case "+":
        result += number;
        break;
      case "/":
        result /= number;
        break;
      case "*":
        result *= number;
        break;
      case "%":
        result %= number;
        break;
    }
  });

  return result;
}

function pluz(num) {
  const intnum = Math.floor(num);
  if (intnum >= 0) {
    return `+${intnum}`;
  } else {
    return `${intnum}`;
  }
}

// Function to handle background and notes input change
function handleBackgroundAndNotesChange(event) {
  const inputName = event.target.name;
  initialData[inputName] = event.target.value;
  regeneratePage();
  debouncedSaveJsonToCookie();
}

// Function to handle checkbox change
function handleCheckboxChange(checkbox) {
  const power = checkbox.getAttribute("data-label");
  if (checkbox.checked) {
    if (!initialData.powers.includes(power)) {
      initialData.powers.push(power);
    }
  } else {
    const index = initialData.powers.indexOf(power);
    if (index !== -1) {
      initialData.powers.splice(index, 1);
    }
  }

  saveJsonToCookie();
  regeneratePage();
}

function handleLoadClick() {
  const fileInput = document.createElement("input");
  fileInput.type = "file";
  fileInput.webkitdirectory = true;
  fileInput.onchange = async function (event) {
    const files = event.target.files;

    try {
      // Load JSON file
      const jsonFile = await loadJsonFile(files);
      if (jsonFile) {
        const parsedData = JSON.parse(jsonFile);
        if (Array.isArray(parsedData)) {
          libraryData = parsedData;
          initialData = mergeData(libraryData[0]);
        } else {
          libraryData = [parsedData];
          initialData = mergeData(parsedData);
        }
        currentIndex = 0;
      } else {
        alert("No JSON file found in the selected folder.");
      }
      console.log(initialData);
      // Clear existing image divs
      const imageContainer = document.getElementById("image-container");
      imageContainer.innerHTML = "";

      // Create default image div
      const defaultImageDiv = document.createElement("div");
      defaultImageDiv.id = "default";
      defaultImageDiv.classList.add("floating-image");
      defaultImageDiv.style.display =
        initialData.imageSrc === "default.png" ? "block" : "none";
      const defaultImage = document.createElement("img");
      defaultImage.src = "../assets/default.png";
      defaultImageDiv.appendChild(defaultImage);
      imageContainer.appendChild(defaultImageDiv);

      // Load images
      await loadImages(files, imageContainer);

      // Regenerate the page after all loading is completed
      regeneratePage();
      initializeNetworkSettings();
      saveJsonToCookie();
      toggleCharacterBuilder();
    } catch (error) {
      console.error("Error loading files:", error);
    }
  };
  fileInput.click();
}

function handleViewClick() {
  const jsonViewer = document.getElementById("viewerPanel");
  jsonViewer.style.display =
    jsonViewer.style.display === "none" ? "block" : "none";
}

function loadJsonFile(files) {
  return new Promise((resolve) => {
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      if (file.type === "application/json") {
        const reader = new FileReader();
        reader.onload = function (e) {
          resolve(e.target.result);
        };
        reader.readAsText(file);
        return;
      }
    }
    resolve(null);
  });
}

function loadImages(files, imageContainer) {
  const imagePromises = [];

  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    if (file.type.startsWith("image/")) {
      const imagePromise = new Promise((resolve) => {
        const reader = new FileReader();
        reader.onload = function (e) {
          const imageDiv = document.createElement("div");
          imageDiv.id = file.name.split(".")[0];
          imageDiv.classList.add("floating-image");
          imageDiv.style.display =
            initialData.imageSrc === file.name ? "block" : "none";
          const image = document.createElement("img");
          image.src = e.target.result;
          imageDiv.appendChild(image);
          imageContainer.appendChild(imageDiv);
          resolve();
        };
        reader.readAsDataURL(file);
      });
      imagePromises.push(imagePromise);
    }
  }

  return Promise.all(imagePromises);
}

function showImageBySource(image) {
  const imageDivs = document.querySelectorAll(".floating-image");
  imageDivs.forEach((div) => {
    console.debug(
      `div name ${div.id} the same as from file ${image.split(".")[0]}`
    );
    if (div.id === image.split(".")[0]) {
      div.style.display = "block";
    } else {
      div.style.display = "none";
    }
  });
}

function mergeData(data) {
  const mergedData = { ...initialData };
  for (const key in data) {
    if (mergedData.hasOwnProperty(key)) {
      mergedData[key] = data[key];
    }
  }
  return mergedData;
}

// Function to handle saving the JSON data to a file
function handleSaveClick() {
  const jsonData = document.getElementById("jsonViewer").value;
  const blob = new Blob([jsonData], { type: "application/json" });
  const url = URL.createObjectURL(blob);
  const link = document.createElement("a");
  link.href = url;
  const characterName = initialData.name.replace(/\s/g, "");
  link.download = `${characterName}.json`;
  link.click();
}

// Function to handle stats input change
function handleStatsInputChange(event) {
  const inputName = event.target.name;
  const inputValue = event.target.value;
  initialData[inputName] =
    inputName === "name" || inputName === "identity"
      ? inputValue
      : parseInt(inputValue);
  regeneratePage();
  // handleSubmit(new Event('submit'))
  debouncedSaveJsonToCookie();
}

// Function to handle stats input change
function handleNetworkChange(event) {
  const inputName = event.target.name;
  const inputValue = event.target.value;
  networkSettings[inputName] = inputValue;
  debouncedSaveJsonToCookie();
}

function handleCookieCheckbox(event) {
  console.log(`Use Cookies?: ${event.target.checked}`);
  cookieData.save = event.target.checked;
  // Update cookie and refresh
  saveJsonToCookie();
}

function handleHealing() {
  // clearFeedback()
  flashScreen("heal");
  const heal_factor = parseFloat(get_end_heal(current.ENDURANCE));

  initialData.health += heal_factor;
  const gain = Math.round((heal_factor * current.MAX_HP) / 100) || 1;
  appendFeedback(`Apply daily healing`, "note");
  appendFeedback(`+${gain} HP`, "heal");

  if (initialData.health > 100) initialData.health = 100;
  // Update cookie and refresh
  saveJsonToCookie();
  regeneratePage();
}

function handleRestore() {
  clearFeedback();
  flashScreen("heal");
  appendFeedback("Full health restored", "note");
  appendFeedback(`+${current.MAX_HP} HP`, "heal");

  initialData.health = 100;
  // Update cookie and refresh
  saveJsonToCookie();
  regeneratePage();
}

function handleRest() {
  clearFeedback();
  flashScreen("heal");

  appendFeedback("Full POWER restored", "note");
  appendFeedback(`+${current.MAX_POWER} POW`, "heal");

  initialData.spent_power = 0;
  // Update cookie and refresh
  saveJsonToCookie();
  regeneratePage();
}

// Function to handle stats input change
function handleARInputChange(event) {
  const inputValue = event.target.value;
  updatePowersDef();
}

function handleClickableCellClick(event) {
  const clickedCell = event.currentTarget;
  const powerName = clickedCell.getAttribute("data-power");
  const powerCost = clickedCell.textContent;

  // Extract the first separate integer from the powerCost
  const match = powerCost.match(/\d+/);
  if (match) {
    const value = Number(match[0]);
    initialData.spent_power += value;
    flashScreen("power");
    appendFeedback(`Using ${powerName} for ${value} PR`, "note");
    appendFeedback(`-${value} PR`, "pr");
  } else {
    appendFeedback(`${powerName} has no usable PR cost`, "alert");
    console.log("Invalid value in clickable cell:", powerCost);
  }

  saveJsonToCookie();
  regeneratePage();
}

function attachClickHandlersToCells() {
  const clickableCells = document.querySelectorAll(
    "#attack_array .clickable-cell"
  );
  clickableCells.forEach((cell) => {
    cell.addEventListener("click", handleClickableCellClick);
  });
}

// Function to handle form submission
function handleSubmit(event) {
  event.preventDefault();

  const jsonData = document.getElementById("jsonViewer").value;
  const backgroundData = document.getElementById("background").value;
  const notesData = document.getElementById("notes").value;

  try {
    const parsedData = JSON.parse(jsonData);
    if (Array.isArray(parsedData)) {
      libraryData = parsedData;
    } else {
      libraryData = [parsedData];
    }
    initialData = { ...libraryData[0], ...libraryData[currentIndex] };
    initialData.background = backgroundData;
    initialData.notes = notesData;

    regeneratePage();

    document.getElementById(
      "output"
    ).innerHTML = `<span class="twemoji"><svg viewBox="0 0 24 24">
      <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z" /></svg></span>`;
    saveJsonToCookie();
  } catch (error) {
    document.getElementById(
      "output"
    ).innerHTML = `<span class="twemoji"> <svg viewBox="0 0 24 24">
    <path fill="#880000" d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z" /></svg></span>`;
  }
}

function handleSwapButtonClick() {
  currentIndex = (currentIndex + 1) % libraryData.length;
  initialData = { ...libraryData[0], ...libraryData[currentIndex] };
  regeneratePage();
}

function toggleCharacterBuilder() {
  const imageDiv = document.getElementById("image-container");
  const builderDiv = document.getElementById("character-builder");

  if (builderDiv.style.display === "none" || builderDiv.style.display === "") {
    builderDiv.style.display = "flex";
    cookieData.showAdmonition = true;
    // If showing the settings panel then clear other distractions
    imageDiv.style.display = "none";
    document.getElementById("calculatorPanel").style.opacity = "0";
  } else {
    builderDiv.style.display = "none";
    cookieData.showAdmonition = false;
    // If theres an image then show it after we leave
    if (hasValidImage) imageDiv.style.display = "block";
  }
  //  We willsave the cookie becasue we track admonition display for nowß
  saveJsonToCookie();
}

// Function to load the JSON data from a cookie
function loadJsonFromCookie() {
  // Start with a clean slate and initialised data structures
  loadDefaultData();

  // Check if data exists in sessionStorage prefer it
  const sessionData = sessionStorage.getItem("open_hero_character");
  if (sessionData) {
    try {
      const parsedData = JSON.parse(sessionData);
      // Move session Data into libraryData and initialData
      if (parsedData.libraryData && parsedData.libraryData.length > 0) {
        libraryData = parsedData.libraryData;
        initialData = { ...libraryData[0], ...libraryData[currentIndex] };
      }
      if (parsedData.cookieData) {
        cookieData = parsedData.cookieData;
      }
      document.getElementById("useCookie").checked = cookieData.save;
      regeneratePage();
      initializeNetworkSettings();
      return; // Exit the function if data is loaded from sessionStorage
    } catch (error) {
      console.error("Error parsing JSON data in sessionStorage:", error);
    }
    // Let JSON Viewer know changes
    pushDataToJsonViewer();
    return;
  }

  // OK we are still here - so use a cookie
  const cookieValue = document.cookie.replace(
    /(?:(?:^|.*;\s*)jsonData\s*\=\s*([^;]*).*$)|^.*$/,
    "$1"
  );
  if (cookieValue) {
    const decodedData = decodeURIComponent(cookieValue);
    try {
      const parsedData = JSON.parse(decodedData);
      if (parsedData.libraryData && parsedData.libraryData.length > 0) {
        libraryData = parsedData.libraryData;
        initialData = { ...libraryData[0], ...libraryData[currentIndex] };
      }
      if (parsedData.cookieData) {
        cookieData = parsedData.cookieData;
      }
      document.getElementById("useCookie").checked = cookieData.save;
      regeneratePage();
    } catch (error) {
      console.error("Error parsing JSON data in cookie:", error);
    }
    // Let JSON Viewer know changes
    pushDataToJsonViewer();
    initializeNetworkSettings();
    return;
  }
  pushDataToJsonViewer();
  initializeNetworkSettings();
}

function displaySwapButton() {
  const swapButton = document.getElementById("swapButton");
  if (libraryData.length > 1) {
    swapButton.style.display = "block";
  } else {
    swapButton.style.display = "none";
  }
}

// Function to load the default data
function loadDefaultData() {
  initialData = {
    name: "default",
    identity: "default",
    agility: 10,
    endurance: 10,
    strength: 10,
    intelligence: 10,
    charisma: 10,
    weight: 75,
    level: 1,
    background: "",
    notes: "",
    spent_power: 0,
    armor_rating: 0,
    health: 100,
    powers: [],
    overrides: [],
    image: "default.png"
  };

  libraryData = [initialData];
  currentIndex = 0;
  // regeneratePage(); called here forces load of powers before read...
  document.getElementById("background").textContent = initialData.background;
  document.getElementById("notes").textContent = initialData.notes;
}

// Function to load powers data from powers.json and overload.json
function loadPowers() {
  return new Promise((resolve, reject) => {
    // Load data from powers.json
    fetch("../powers.json")
      .then((response) => response.json())
      .then((data) => {
        // Check if overload.json exists
        fetch("../overload.json")
          .then((response) => response.json())
          .then((overrideData) => {
            // Combine the data from the two files
            powers = combineOverrides(data, overrideData);
            resolve(); // Resolve the Promise when powers are loaded and page is built
          })
          .catch((error) => {
            // If overload.json doesn't exist, just use the data from powers.json
            console.log("overload.json not found, using only powers.json");
            powers = data;
            resolve();
          });
      })
      .catch((error) => {
        console.error("Error fetching powers.json:", error);
        reject(error); // Reject the Promise if an error occurs
      });
  });
}

// Function to combine the data from powers.json and overload.json
function combineOverrides(baseData, overrideData) {
  const combinedData = [...baseData];

  overrideData.forEach((override) => {
    const existingPower = combinedData.find((p) => p.name === override.name);
    if (existingPower) {
      Object.assign(existingPower, override);
    } else {
      combinedData.push(override);
    }
  });

  return combinedData;
}

// Function to push the default data to the JSON input field
function pushDataToJsonViewer() {
  const defaultJsonData = JSON.stringify(initialData, null, 2);
  document.getElementById("jsonViewer").value = defaultJsonData;
}

// Function to apply overrides to the powers array
function pushOverridesToPowers() {
  if (initialData.overrides) {
    initialData.overrides.forEach((override) => {
      const existingPower = powers.find((p) => p.name === override.name);
      if (existingPower) {
        Object.assign(existingPower, override);
      } else {
        powers.push(override);
      }
    });
  }
}
function togglePasswordVisibility() {
  const passwordInput = document.getElementById("password");
  const passwordToggleIcon = document.querySelector(".password-toggle-icon");

  if (passwordInput.type === "password") {
    passwordInput.type = "text";
    passwordToggleIcon.classList.add("visible");
  } else {
    passwordInput.type = "password";
    passwordToggleIcon.classList.remove("visible");
  }
}

function initializeNetworkSettings() {
  // Set player_id based on the character name
  if (initialData.name) {
    networkSettings.player_id = initialData.name
      .toLowerCase()
      .replace(/[^a-z0-9]/g, "");
  } else {
    networkSettings.player_id = "defaultplayer";
  }
  const currentDate = new Date();
  networkSettings.game_id = currentDate.toISOString().split("T")[0];
  console.log("Network settings initialized:", networkSettings);
}

function pushDataToBuilderInputs() {
  document.getElementById("name").value = initialData.name;
  document.getElementById("identity").value = initialData.identity;
  document.getElementById("agility").value = initialData.agility;
  document.getElementById("endurance").value = initialData.endurance;
  document.getElementById("strength").value = initialData.strength;
  document.getElementById("intelligence").value = initialData.intelligence;
  document.getElementById("charisma").value = initialData.charisma;
  document.getElementById("weight").value = initialData.weight;
  document.getElementById("level").value = initialData.level;
  document.getElementById("spent_power").value = initialData.spent_power;
  document.getElementById("armor_rating").value = initialData.armor_rating;
  document.getElementById("useCookie").checked = cookieData.save;
  document.getElementById("broker").value = networkSettings.broker;
  document.getElementById("port").value = networkSettings.port;
  document.getElementById("username").value = networkSettings.username;
  document.getElementById("password").value = networkSettings.password;
  document.getElementById("player_id").value = networkSettings.player_id;
  document.getElementById("game_id").value = networkSettings.game_id;
}

function regeneratePage() {
  // Get the color from the cookie before drawing
  document.documentElement.style.setProperty(
    "--md-primary-fg-color",
    cookieData.color
  );
  // FIL file picker implement to like to users computer for image
  // FIL saveCookie save should stay checked but resets
  pushOverridesToPowers();
  buildCheckboxes();
  pushDataToBuilderInputs();
  updateLibraryData();
  updateJsonViewer();
  updatePowerCheckboxes();
  updateDerivedStats();
  updateMovement();
  updateSheetDetails();
  updateAttackArray();
  attachClickHandlersToCells();
  updateDefenseMatrix();
  updateBackgroundAndNotes();
  updatePowersSummary();
  updatePowersDef();
  displaySwapButton();
  showImageBySource(initialData.image);
  drawAdvantagePool();
  drawDisadvantagePool();
  updateEvadeAnimation();

  // visibility of settings panel and image are opposite
  document.getElementById("character-builder").style.display =
    cookieData.showAdmonition ? "flex" : "none";
  document.getElementById("image-container").style.display =
    cookieData.showAdmonition ? "none" : "block";

  document.getElementById("evading").checked = is_evading;
  // Initialise color picker
  const colorPickerButton = document.getElementById("colorPickerButton");
  if (colorPickerButton) {
    colorPickerButton.value = cookieData.color;
    Coloris(colorPickerButton);
  }

  publishPulse();
}

// Function to clear the attack array table
function resetAttackArray() {
  const tableBody = document.querySelector("#attack_array tbody");
  tableBody.innerHTML = ""; // Clear the contents of the table body
  modData.attacks = [];
}

function resetDefenseMatrix() {
  const matrixElements = document.querySelectorAll(
    "#imm_physical, #resist_physical, #vuln_physical, " +
      "#imm_mental, #resist_mental, #vuln_mental, " +
      "#imm_spiritual, #resist_spiritual, #vuln_spiritual, " +
      "#imm_photonic, #resist_photonic, #vuln_photonic, " +
      "#imm_electric, #resist_electric, #vuln_electric, " +
      "#imm_magnetic, #resist_magnetic, #vuln_magnetic, " +
      "#imm_thermal, #resist_thermal, #vuln_thermal, " +
      "#imm_vibration, #resist_vibration, #vuln_vibration, " +
      "#imm_disruption, #resist_disruption, #vuln_disruption"
  );

  matrixElements.forEach((element) => {
    element.innerHTML = `<span class="twemoji imm">${smalldot}</span>`;
  });
  modData.immune = [];
  modData.resist = [];
  modData.vulnerable = [];
}

const debouncedSaveJsonToCookie = debounce(saveJsonToCookie, 500); // Adjust the delay as needed

// Function to save the JSON data to a cookie
function saveJsonToCookie() {
  // console.debug(`saveJsonToCookie() called with ${cookieData.save}`)

  let sessionJson;
  let storedJson;
  let storedData;
  let sessionData;
  if (cookieData.save) {
    sessionData = { libraryData, cookieData, networkSettings };
    storedData = { libraryData, cookieData, networkSettings };

    // Lets encde this and quickly test for size before proceeding
    // It is likely too big so we revert to cookieData alone
    storedJson = JSON.stringify(storedData, null, 2);
    const encodedData = encodeURIComponent(storedJson);
    if (encodedData.length >= 4096) {
      console.warn(`Cookie TOO BIG ${encodedData.length} save only cookieData`);
      storedData = { cookieData, networkSettings };
    }
  } else {
    sessionData = { cookieData, networkSettings };
    storedData = { cookieData, networkSettings };
  }
  // Write data to session storage
  sessionJson = JSON.stringify(sessionData, null, 2);
  sessionStorage.setItem("open_hero_character", sessionJson);

  // Write to cookie
  storedJson = JSON.stringify({ storedData }, null, 2);
  const encodedData = encodeURIComponent(storedJson);
  const expirationDate = new Date();
  expirationDate.setDate(expirationDate.getDate() + 60);
  document.cookie = `jsonData=${encodedData}; expires=${expirationDate.toUTCString()}; path=/; SameSite=Lax;`;
}

function toggleCalcPanel() {
  const calcPanel = document.getElementById("calculatorPanel");

  // Check the current visibility state of the panel
  if (calcPanel.style.opacity === "0" || calcPanel.style.opacity === "") {
    // Panel is currently invisible or in its initial state, make it visible and animate the transition
    calcPanel.style.opacity = "1";
    calcPanel.style.visibility = "visible";
  } else {
    calcPanel.style.opacity = "0";
    setTimeout(() => {
      calcPanel.style.visibility = "hidden";
    }, 300);
  }
}

function appendMovementMode(name, value, units, id = false) {
  // Get handle to parent move_box
  const moveBoxElement = document.getElementById("move_box");
  // Build movement "title"
  const framedLabelElement = document.createElement("span");
  framedLabelElement.classList.add("framed_label");
  framedLabelElement.textContent = name;
  moveBoxElement.appendChild(framedLabelElement);
  // Build movement value - add id if present
  const framedNumElement = document.createElement("span");
  if (id) framedNumElement.id = id;
  framedNumElement.classList.add("framed_num", "move");
  const movementValue = value ? evaluateExpression(value) : "?";
  framedNumElement.textContent = movementValue;
  moveBoxElement.appendChild(framedNumElement);
  // Build units tag
  const unframedLabelElement = document.createElement("span");
  unframedLabelElement.classList.add("unframed_label", "orange");
  unframedLabelElement.textContent = units || "hex/turn";
  moveBoxElement.appendChild(unframedLabelElement);
}

function updateMovement() {
  // Clear the move_box
  const moveBoxElement = document.getElementById("move_box");
  moveBoxElement.innerHTML = "";
  // Append the BASE MOVE movement item
  appendMovementMode("BASE MOVE", current.MOVE, "hex/turn", "movement");

  powers.forEach((power) => {
    if (initialData.powers.includes(power.name) && power.movement) {
      const movementArray = Array.isArray(power.movement)
        ? power.movement
        : [power.movement];

      movementArray.forEach((movementItem) => {
        if (movementItem.value) {
          appendMovementMode(
            movementItem.name,
            movementItem.value,
            movementItem.units
          );
        }
      });
    }
  });
}
// Function to update the background and notes fields
function updateBackgroundAndNotes() {
  const backgroundDiv = document.getElementById("background");
  const notesDiv = document.getElementById("notes");
  const bkgOutDiv = document.getElementById("bkg_out");
  const notesOutDiv = document.getElementById("notes_out");

  // Clear the existing content
  backgroundDiv.textContent = initialData.background;
  notesDiv.textContent = initialData.notes;
  bkgOutDiv.innerHTML = "";
  notesOutDiv.innerHTML = "";

  // Check if initialData.background is defined and not empty or containing only whitespace
  if (initialData.background && initialData.background.trim()) {
    // Add the h3 heading to bkgOutDiv
    const bkgHeading = document.createElement("h3");
    bkgHeading.textContent = "Background";
    bkgOutDiv.appendChild(bkgHeading);

    // Append the markdown-generated content to bkgOutDiv
    const bkgContent = document.createElement("div");
    bkgContent.classList.add("bkg_style");
    bkgContent.innerHTML = markdownToHtml(initialData.background);
    bkgOutDiv.appendChild(bkgContent);

    // Show the bkgOutDiv
    bkgOutDiv.style.display = "block";
  } else {
    // Hide the bkgOutDiv if initialData.background is empty or contains only whitespace
    bkgOutDiv.style.display = "none";
  }

  // Check if initialData.notes is defined and not empty or containing only whitespace
  if (initialData.notes && initialData.notes.trim()) {
    // Add the h3 heading to notesOutDiv
    const notesHeading = document.createElement("h3");
    notesHeading.textContent = "Notes";
    notesOutDiv.appendChild(notesHeading);

    // Append the markdown-generated content to notesOutDiv
    const notesContent = document.createElement("div");
    notesContent.classList.add("bkg_style");
    notesContent.innerHTML = markdownToHtml(initialData.notes);
    notesOutDiv.appendChild(notesContent);

    // Show the notesOutDiv
    notesOutDiv.style.display = "block";
  } else {
    // Hide the notesOutDiv if initialData.notes is empty or contains only whitespace
    notesOutDiv.style.display = "none";
  }
}

function get_level_mod() {
  return Math.floor(initialData.level / 2);
}
// Function to update checkboxes based on JSON data
function updatePowerCheckboxes() {
  const checkboxes = document.querySelectorAll('input[type="checkbox"]');

  checkboxes.forEach((checkbox) => {
    const power = checkbox.getAttribute("data-label");
    const isChecked = initialData.powers && initialData.powers.includes(power);
    checkbox.checked = isChecked;
  });
}

// Function to generate the attack array table
function updateAttackArray() {
  resetAttackArray(); // Clear the table before regenerating it

  const range = get_max_range(current.MAX_LIFT);
  const current_HTH =
    get_base_hth(current.MAX_LIFT) + get_int_dam_mod(current.INTELLIGENCE);
  const meleeATT =
    gameSettings.base_hth + get_level_mod() + get_acc_mod(current.STRENGTH);
  const rangedATT =
    gameSettings.base_hth + get_level_mod() + get_acc_mod(current.AGILITY);

  buildAttackRow({
    name: "Hand-to-Hand: Basic Melee",
    th: pluz(meleeATT),
    damage: current_HTH,
    range: "–",
    pr: "1",
    type: "Physical"
  });

  const num_range = evaluateExpression(range);
  buildAttackRow({
    name: "Hand-to-Hand: Ranged",
    th: pluz(rangedATT),
    damage: current_HTH,
    range: num_range ? Math.ceil(num_range) : range,
    pr: "1",
    type: "Physical"
  });
  powers.forEach((power) => {
    if (initialData.powers.includes(power.name) && power.attacks) {
      const attackArray = Array.isArray(power.attacks)
        ? power.attacks
        : [power.attacks];

      attackArray.forEach((attack) => {
        const calculatedRange = evaluateExpression(attack.range);
        const newTH = evaluateExpression(attack.th);
        buildAttackRow({
          name: attack.name,
          th: pluz(newTH + get_level_mod()),
          damage: formatDamage(attack.damage),
          range: !isNaN(calculatedRange)
            ? Math.ceil(calculatedRange)
            : attack.range,
          pr: attack.pr,
          type: attack.type
        });
      });
    }
  });
  // initialData.powers.forEach((power) => {
  //   console.debug(power.name);
  //   const powerData = powers.find((p) => p.name === power);
  //   if (powerData && powerData.attacks) {
  //     powerData.attacks.forEach((attack) => {
  //       console.debug(attack.name);
  //       const calculatedRange = evaluateExpression(attack.range);
  //       const newTH = evaluateExpression(attack.th);
  //       buildAttackRow({
  //         name: attack.name,
  //         th: pluz(newTH),
  //         damage: formatDamage(attack.damage),
  //         range: !isNaN(calculatedRange)
  //           ? Math.ceil(calculatedRange)
  //           : attack.range,
  //         pr: attack.pr,
  //         type: attack.type
  //       });
  //     });
  //   }
  // });
}

function formatDamage(damage_string) {
  const HTH =
    get_base_hth(current.MAX_LIFT) + get_int_dam_mod(current.INTELLIGENCE);
  const L = initialData.level;
  // Replace all occurrences of 'HTH' within `damage_string` with the value of HTH
  if (damage_string) {
    let polished = damage_string.replace(/HTH/g, HTH);
    polished = polished.replace(/L/g, L);
    return polished;
  }
  return "";
}

// Function to calculate the defense matrix based on the selected powers
function updateDefenseMatrix() {
  resetDefenseMatrix();
  const defenseCol1 = document.getElementById("DC1");
  const defenseCol2 = document.getElementById("DC2");
  const defenseCol3 = document.getElementById("DC3");

  initialData.powers.forEach((power) => {
    const powerData = powers.find((p) => p.name === power);
    if (powerData) {
      drawDefenseMatrixByType(powerData.immune, "imm", defenseCol1);
      drawDefenseMatrixByType(powerData.resist, "resist", defenseCol1);
      drawDefenseMatrixByType(powerData.vulnerable, "vuln", defenseCol1);
      drawDefenseMatrixByType(powerData.immune, "imm", defenseCol2);
      drawDefenseMatrixByType(powerData.resist, "resist", defenseCol2);
      drawDefenseMatrixByType(powerData.vulnerable, "vuln", defenseCol2);
      drawDefenseMatrixByType(powerData.immune, "imm", defenseCol3);
      drawDefenseMatrixByType(powerData.resist, "resist", defenseCol3);
      drawDefenseMatrixByType(powerData.vulnerable, "vuln", defenseCol3);
      //Update modData with defenses
      // Update modData with defenses
      if (powerData.immune) {
        modData.immune = [...new Set([...modData.immune, ...powerData.immune])];
      }
      if (powerData.resist) {
        modData.resist = [...new Set([...modData.resist, ...powerData.resist])];
      }
      if (powerData.vulnerable) {
        modData.vulnerable = [
          ...new Set([...modData.vulnerable, ...powerData.vulnerable])
        ];
      }
    }
  });
}

// Function to update JSON input based on character data
function updateJsonViewer() {
  const jsonData = JSON.stringify(libraryData, null, 2);
  document.getElementById("jsonViewer").value = jsonData;
}

function updateLibraryData() {
  const currentData = libraryData[currentIndex];
  const updatedData = { ...currentData, ...initialData };

  // Apply specific properties to libraryData[0]
  libraryData[0].level = initialData.level;
  libraryData[0].identity = initialData.identity;
  libraryData[0].spent_power = initialData.spent_power;
  libraryData[0].armor_rating = initialData.armor_rating;
  libraryData[0].health = initialData.health;
  libraryData[0].background = initialData.background;
  libraryData[0].notes = initialData.notes;

  // Remove specific properties from updatedData if currentIndex is not 0
  if (currentIndex !== 0) {
    delete updatedData.level;
    delete updatedData.identity;
    delete updatedData.spent_power;
    delete updatedData.armor_rating;
    delete updatedData.health;
    delete updatedData.background;
    delete updatedData.notes;
  }

  // Update the current index data in libraryData
  libraryData[currentIndex] = updatedData;
}

function resetModData() {
  modData.agility = 0;
  modData.endurance = 0;
  modData.strength = 0;
  modData.intelligence = 0;
  modData.charisma = 0;
  modData.mv = "";
  modData.ac = 0;
  modData.ar = 0;
  modData.sr = 0;
  modData.dr = 0;
  modData.wt = "";
  modData.immune = [];
  modData.resist = [];
  modData.vulnerable = [];
  modData.attacks = [];
}

function updateModsFromPowers() {
  // reset mods
  resetModData();
  // loop through powers and update changes
  powers.forEach((power) => {
    if (initialData.powers.includes(power.name)) {
      if (power.agility) modData.agility += parseInt(power.agility);
      if (power.strength) modData.strength += parseInt(power.strength);
      if (power.endurance) modData.endurance += parseInt(power.endurance);
      if (power.intelligence)
        modData.intelligence += parseInt(power.intelligence);
      if (power.charisma) modData.charisma += parseInt(power.charisma);

      if (power.mv) modData.mv += power.mv; // Just concatenate together and eval later
      if (power.ac) modData.ac += parseInt(power.ac);
      if (power.ar && power.ar > modData.ar) modData.ar = power.ar;
      if (power.sr && power.sr > modData.sr) modData.sr = power.sr;
      if (power.dr && power.dr > modData.dr) modData.dr = power.dr;

      if (power.wt) modData.wt += power.wt; // Just concatenate together and eval later
    }
  });
}

// Function to compute HP Final
function updateDerivedStats() {
  // This important calc requires the latest information
  updateModsFromPowers();
  // Weight needed for AGIL and STR mods
  current.WEIGHT = calculateWeightAdj(initialData.weight, modData.wt);

  current.AGILITY =
    initialData.agility + modData.agility + get_agil_adj(current.WEIGHT);
  current.STRENGTH =
    initialData.strength + modData.strength - get_agil_adj(current.WEIGHT);

  current.ENDURANCE = initialData.endurance + modData.endurance;
  current.INTELLIGENCE = initialData.intelligence + modData.intelligence;
  current.CHARISMA = initialData.charisma + modData.charisma;

  current.BLOCKS = Math.ceil(current.WEIGHT / gameSettings.kilo_per_block);
  current.MAX_HP = Math.ceil(
    ((get_str_hp_mod(current.STRENGTH) *
      get_end_hp_mod(current.ENDURANCE) *
      get_agil_hp_mod(current.AGILITY) *
      get_int_hp_mod(current.INTELLIGENCE) *
      Math.max(
        Math.min(current.WEIGHT, gameSettings.max_effective_weight),
        gameSettings.min_effective_weight
      )) /
      gameSettings.kilo_per_block) *
      gameSettings.hp_per_flesh_block *
      (1 + gameSettings.hp_level_scaling * (initialData.level - 1))
  );
  current.MAX_POWER =
    current.STRENGTH +
    current.ENDURANCE +
    current.AGILITY +
    current.INTELLIGENCE;
  current.MAX_LIFT =
    ((Math.pow(current.STRENGTH / 10, 3) + current.ENDURANCE / 10) / 2) *
    Math.max(current.WEIGHT, gameSettings.min_effective_weight);
  current.MOVE = calculateMoveAdj(
    current.STRENGTH + current.AGILITY + current.ENDURANCE,
    modData.mv
  );
}

function reformatModStr(str) {
  const operators = ["*", "/", "+", "-", "%"];
  const pairs = str.match(/[*/%+-]?[0-9]+(\.[0-9]+)?/g);
  if (pairs === null) {
    return str;
  }

  const leadingNumber = pairs[0].match(/^[0-9]+(\.[0-9]+)?/)
    ? pairs.shift()
    : "";

  const sortedPairs = pairs.sort((a, b) => {
    const operatorA = a.charAt(0);
    const operatorB = b.charAt(0);
    const operatorIndexA = operators.indexOf(operatorA);
    const operatorIndexB = operators.indexOf(operatorB);

    if (operatorIndexA !== operatorIndexB) {
      return operatorIndexA - operatorIndexB;
    }

    const numberA = parseFloat(a.slice(1));
    const numberB = parseFloat(b.slice(1));
    return numberA - numberB;
  });

  const reformattedString = leadingNumber + sortedPairs.join("");
  return reformattedString;
}

function calculateMoveAdj(base, modstring) {
  // console.warn(`calculateMoveAdj:${modstring}`)
  let newMods = modstring.replace(/x/g, "\\*");
  newMods = reformatModStr(newMods);
  // console.warn(`calculateMoveAdjNEW:${newMods}`)
  const oldmove = current.STRENGTH + current.AGILITY + current.ENDURANCE;
  // console.warn(`Base movement: ${oldmove}`)
  // console.warn(`${oldmove}${newMods}`)
  const newmove = evaluateExpression(`${oldmove}${newMods}`);
  // console.warn(newmove)

  return newmove;
}

function flashScreen(type) {
  const overlay = document.getElementById("flash-overlay");

  // Define colors for different types
  const colors = {
    warn: "rgba(255, 255, 0, 0.5)", // yellow
    error: "rgba(255, 0, 0, 0.5)", // red
    action: "rgba(0, 255, 0, 0.5)", // green
    heal: "rgb(224, 213, 172)",
    damage: "#c005",
    power: "#55f5"
  };

  // Set the color based on the type
  const color = colors[type] || colors.warn;
  overlay.style.animation = "none";
  overlay.offsetHeight;
  overlay.style.backgroundColor = color;
  overlay.style.animation = "flash-animation 0.3s ease-in-out";

  // Reset the overlay after the animation
  setTimeout(() => {
    overlay.style.backgroundColor = "transparent";
    overlay.style.animation = "none";
  }, 300);
}

function calculateWeightAdj(base, modstring) {
  let newMods = modstring.replace(/x/g, "\\*");
  newMods = reformatModStr(newMods);
  const new_weight = evaluateExpression(`${base}${newMods}`);
  return new_weight;
}

function updateSheetDetails() {
  const current_power = current.MAX_POWER - initialData.spent_power;
  document.getElementById("hpFinal").textContent =
    "MAX: " + current.MAX_HP.toFixed(0);

  document.getElementById("hpRemains").textContent = Math.round(
    current.MAX_HP * (initialData.health / 100)
  ).toFixed(0);

  document.getElementById("blocks").textContent = current.BLOCKS.toFixed(0);

  document.getElementById("ticks").textContent = Math.floor(
    initialData.level / 5 + 2
  ).toFixed(0);

  document.getElementById("powerScore").textContent =
    "MAX: " + current.MAX_POWER.toFixed(0);

  document.getElementById("powerRemains").textContent =
    current_power.toFixed(0);

  document.getElementById("attAMod").textContent = pluz(
    get_acc_mod(current.AGILITY)
  );
  document.getElementById("attSMod").textContent = pluz(
    get_acc_mod(current.STRENGTH)
  );

  document.getElementById("damMod").textContent = get_int_dam_mod(
    current.INTELLIGENCE
  );

  document.getElementById("healRate").textContent =
    get_end_heal(current.ENDURANCE).toFixed(0) + "%";

  document.getElementById("detect").textContent =
    get_detect(current.INTELLIGENCE) + "%";

  document.getElementById("carry").textContent = current.MAX_LIFT.toFixed(0);

  document.getElementById("baseHTHdam").textContent = get_base_hth(
    current.MAX_LIFT
  );

  document.getElementById("reactionMod").textContent = pluz(
    get_vibe(current.CHARISMA)
  );

  document.getElementById("character_name").textContent = initialData.name;
  // const ellipsisSpans = document.querySelectorAll("span.md-ellipsis");
  // ellipsisSpans.forEach((span) => {
  //   span.textContent = initialData.name;
  // });
  document.title = initialData.name;

  document.getElementById("agil_out").textContent = current.AGILITY;
  document.getElementById("str_out").textContent = current.STRENGTH;
  document.getElementById("end_out").textContent = current.ENDURANCE;
  document.getElementById("int_out").textContent = current.INTELLIGENCE;
  document.getElementById("cha_out").textContent = current.CHARISMA;
  document.getElementById("weight_out").textContent =
    parseFloat(current.WEIGHT).toFixed(0) + "kg";

  document.getElementById("id_out").textContent = initialData.identity;
  document.getElementById("level_out").textContent = initialData.level;

  document.getElementById("ip_out").textContent = Math.floor(
    (current.INTELLIGENCE / 10) * initialData.level
  ).toFixed(0);
}

function get_current_ac() {
  let evade = 0;
  if (is_evading)
    evade = Math.floor((current.MAX_POWER - initialData.spent_power) / 10);
  return [gameSettings.base_ac + get_level_mod() + modData.ac + evade, evade];
}

function updatePowersDef() {
  // LOOP calculates nothing, merely adds labels to AC_POWERS
  document.getElementById("AC_POWERS").innerHTML = "";
  powers.forEach((power) => {
    if (initialData.powers.includes(power.name)) {
      if (power.ac) {
        document.getElementById("AC_POWERS").innerHTML +=
          pluz(power.ac) + " " + power.name + "<br>";
      }
    }
  });

  let [AC_total, evade] = get_current_ac();
  document.getElementById("AC_out").textContent = AC_total;
  document.getElementById("AC_EVADE").textContent = pluz(evade);
  document.getElementById("AC_LVL").innerHTML = pluz(initialData.level / 2);
  document.getElementById("AC_BASE").innerHTML = gameSettings.base_ac;

  // Hide unused AR
  const AR_total = initialData.armor_rating;
  if (modData.ar != 0 && initialData.armor_rating === 0) {
    initialData.armor_rating = Number(modData.ar);
  }
  document.getElementById("AR_out").textContent = AR_total;
  if (AR_total > 0) document.getElementById("AR_box").style.opacity = "1";
  else document.getElementById("AR_box").style.opacity = "0";

  // Hide usused SR
  document.getElementById("SR_out").textContent = modData.sr;
  if (modData.sr > 0) document.getElementById("SR_box").style.opacity = "1";
  else document.getElementById("SR_box").style.opacity = "0";
}

function createPowerContent(power) {
  const section = document.createElement("div");
  section.classList.add("power-section");

  const title = document.createElement("h4");
  title.classList.add("power-title");
  title.textContent = power.name;
  title.addEventListener("click", () => {
    section.classList.toggle("expanded");
  });

  const briefSection = document.createElement("div");
  briefSection.classList.add("brief-section");
  if (power.brief)
    briefSection.innerHTML = `<span class="brief-style" >${markdownToHtml(
      power.brief
    )}</span>`;

  const descriptionSection = document.createElement("div");
  descriptionSection.classList.add("description-section");
  if (power.description) {
    descriptionSection.innerHTML = `<div>${markdownToHtml(
      power.description
    )}</div>`;
  }

  // Create power_def_sum div and columns
  const powerDefSum = document.createElement("div");
  powerDefSum.classList.add("power_def_sum");

  const pdsImmune = document.createElement("div");
  pdsImmune.classList.add("pds_immune");

  const pdsResist = document.createElement("div");
  pdsResist.classList.add("pds_resist");

  const pdsVulnerable = document.createElement("div");
  pdsVulnerable.classList.add("pds_vulnerable");

  // Add spans for power.immune
  if (power.immune && power.immune.length > 0) {
    power.immune.forEach((item) => {
      const chillSpan = document.createElement("span");
      chillSpan.classList.add("chill", "label-cell");
      chillSpan.innerHTML = `<span class="twemoji imm">${shield}</span>${item}`;
      pdsImmune.appendChild(chillSpan);
    });
  }

  // Add spans for power.resist
  if (power.resist && power.resist.length > 0) {
    power.resist.forEach((item) => {
      const coolSpan = document.createElement("span");
      coolSpan.classList.add("cool", "label-cell");
      coolSpan.innerHTML = `<span class="twemoji resist">${red_dice}</span>${item}`;
      pdsResist.appendChild(coolSpan);
    });
  }

  // Add spans for power.vulnerable
  if (power.vulnerable && power.vulnerable.length > 0) {
    power.vulnerable.forEach((item) => {
      const warmSpan = document.createElement("span");
      warmSpan.classList.add("warm", "label-cell");
      warmSpan.innerHTML = `<span class="twemoji vuln">${green_dice}</span>${item}`;
      pdsVulnerable.appendChild(warmSpan);
    });
  }

  // Append columns to power_def_sum div if at least one column has content
  if (
    pdsImmune.children.length > 0 ||
    pdsResist.children.length > 0 ||
    pdsVulnerable.children.length > 0
  ) {
    powerDefSum.appendChild(pdsImmune);
    powerDefSum.appendChild(pdsResist);
    powerDefSum.appendChild(pdsVulnerable);
    descriptionSection.appendChild(powerDefSum);
  }

  const footer = document.createElement("span");
  footer.classList.add("power-footer");
  footer.classList.add("hang_right");

  if (power.range) {
    const rangeSpan = document.createElement("span");
    rangeSpan.classList.add("range-style");
    rangeSpan.textContent = `RNG:${power.range}`;
    footer.appendChild(rangeSpan);
  }
  if (power.damage) {
    const damageSpan = document.createElement("span");
    damageSpan.classList.add("damage-style");
    damageSpan.textContent = `DAM: ${power.damage}`;
    footer.appendChild(damageSpan);
  }
  if (power.ac) {
    const acSpan = document.createElement("span");
    acSpan.classList.add("ac-style");
    acSpan.textContent = `AC: ${pluz(power.ac)}`;
    footer.appendChild(acSpan);
  }
  if (power.sr) {
    const srSpan = document.createElement("span");
    srSpan.classList.add("sr-style");
    srSpan.textContent = `SR: ${power.sr}`;
    footer.appendChild(srSpan);
  }
  if (power.dr) {
    const drSpan = document.createElement("span");
    drSpan.classList.add("dr-style");
    drSpan.textContent = `DR: ${power.dr}`;
    footer.appendChild(drSpan);
  }

  if (power.mv) {
    const mvSpan = document.createElement("span");
    mvSpan.classList.add("mv-style");
    mvSpan.textContent = `MV: ${power.mv}`;
    footer.appendChild(mvSpan);
  }
  if (power.power_cost) {
    const powerSpan = document.createElement("span");
    powerSpan.classList.add("power-style");
    powerSpan.classList.add("clickable");
    powerSpan.textContent = `PR: ${power.power_cost}`;
    powerSpan.setAttribute("data-power", power.name);
    powerSpan.addEventListener("click", handleClickableCellClick);
    footer.appendChild(powerSpan);
  }

  section.appendChild(footer);
  section.appendChild(title);
  section.appendChild(briefSection);
  section.appendChild(descriptionSection);

  return section;
}

function updatePowersSummary() {
  const powersSummary = document.getElementById("powers-summary");
  const equipSummary = document.getElementById("equip-summary");
  const skillsSummary = document.getElementById("skills-summary");
  const magicSummary = document.getElementById("magic-summary");
  const weakSummary = document.getElementById("weakness-summary");
  const sidekickSummary = document.getElementById("sidekick-summary");
  const vehicleSummary = document.getElementById("vehicle-summary");

  powersSummary.innerHTML = "";
  equipSummary.innerHTML = "";
  skillsSummary.innerHTML = "";
  magicSummary.innerHTML = "";
  weakSummary.innerHTML = "";
  sidekickSummary.innerHTML = "";
  vehicleSummary.innerHTML = "";

  let hasPowers = false;
  let hasEquip = false;
  let hasSkills = false;
  let hasMagic = false;
  let hasWeakness = false;
  let hasSidekick = false;
  let hasVehicle = false;

  powers.forEach((power) => {
    if (initialData.powers.includes(power.name)) {
      const section = createPowerContent(power);
      if (!power.type) power.type = ""; // This ensures that absent type is handled well
      if (power.type.toLowerCase().includes("equip")) {
        if (!hasEquip) {
          const equipTitle = document.createElement("h3");
          equipTitle.textContent = "Equipment";
          equipSummary.appendChild(equipTitle);
          hasEquip = true;
        }
        equipSummary.appendChild(section);
      } else if (power.type.toLowerCase().includes("skill")) {
        if (!hasSkills) {
          const skillsTitle = document.createElement("h3");
          skillsTitle.textContent = "Skills";
          skillsSummary.appendChild(skillsTitle);
          hasSkills = true;
        }
        skillsSummary.appendChild(section);
      } else if (power.type.toLowerCase().includes("magic")) {
        if (!hasMagic) {
          const magicTitle = document.createElement("h3");
          magicTitle.textContent = "Magic";
          magicSummary.appendChild(magicTitle);
          hasMagic = true;
        }
        magicSummary.appendChild(section);
      } else if (power.type.toLowerCase().includes("weakness")) {
        if (!hasWeakness) {
          const weakTitle = document.createElement("h3");
          weakTitle.textContent = "Weaknesses";
          weakSummary.appendChild(weakTitle);
          hasWeakness = true;
        }
        weakSummary.appendChild(section);
      } else if (power.type.toLowerCase().includes("sidekick")) {
        if (!hasSidekick) {
          const sidekickTitle = document.createElement("h3");
          sidekickTitle.textContent = "Sidekicks";
          sidekickSummary.appendChild(sidekickTitle);
          hasSidekick = true;
        }
        sidekickSummary.appendChild(section);
      } else if (power.type.toLowerCase().includes("vehicle")) {
        if (!hasVehicle) {
          const vehicleTitle = document.createElement("h3");
          vehicleTitle.textContent = "Vehicles";
          vehicleSummary.appendChild(vehicleTitle);
          hasVehicle = true;
        }
        vehicleSummary.appendChild(section);
      } else {
        if (!hasPowers) {
          const powersTitle = document.createElement("h3");
          powersTitle.textContent = "Powers";
          powersSummary.appendChild(powersTitle);
          hasPowers = true;
        }
        powersSummary.appendChild(section);
      }
    }
  });
}

function directToPower(PR) {
  // console.debug(`------ directToPower(${PR})`)

  if (PR > getCurrentPower()) {
    appendFeedback(`-${getCurrentPower()} PR`, "pr");
    // spent_power is set to total max adjusted power (ie all spent)
    initialData.spent_power = getMaxPower();
    appendFeedback("Out of Power!", "error");
  } else {
    initialData.spent_power += PR;
    appendFeedback(`-${PR} PR`, "pr");
  }
}

function srToHitPoints(DAM) {
  // console.debug(`---- srToHitPoints(${DAM})`)

  const SR = modData.sr;
  const DR = modData.dr;

  // health tracks percent of maximum to account for weight changes
  if (DAM >= SR) {
    // console.debug(`------ DAM ${DAM} beats SR (${SR})`)
    if (DAM <= DR) {
      appendFeedback(`All damage ${DAM} stopped by DR ${DR}`, "note");
      appendFeedback(`${DAM}<DR`, "sr");
    } else {
      directToHitPoints(DAM);
    }
  } else {
    appendFeedback(`All damage ${DAM} stopped by SR ${SR}`, "note");
    appendFeedback(`${DAM}<SR`, "sr");

    // console.debug(`All damage ${DAM} stopped by SR ${SR}`)
  }
}

function appendFeedback(message, type) {
  console.warn(message);

  // Create a new speech bubble element
  const feedbackBubble = document.createElement("div");
  feedbackBubble.className = "process-bubble";

  // Set the class based on the message type
  feedbackBubble.classList.add(type);

  // Set the text content of the speech bubble
  feedbackBubble.textContent = message;

  // Append the speech bubble to the feedback window
  const feedbackWindow = document.getElementById("combat_info");
  feedbackWindow.appendChild(feedbackBubble);

  // Scroll to the bottom of the feedback window
  feedbackWindow.scrollTop = feedbackWindow.scrollHeight;
}

function clearFeedback() {
  document.getElementById("combat_info").innerHTML = "";
}

function directToHitPoints(DAM) {
  // console.debug(`---- directToHitPoints(${DAM})`)

  // Excess damage goes to POWER
  if (DAM >= getCurrentHP()) {
    appendFeedback(`Damage ${DAM} exceeds hitpoints ${getCurrentHP()}`, "note");
    appendFeedback(`-${getCurrentHP()} HP`, "hp");
    var overflow = DAM - getCurrentHP();
    initialData.health = 0;
    directToPower(overflow);
    appendFeedback("HP: 0 [UNCONSCIOUS]", "error");
    // alert('Unconscious - 0 HP')
    return;
  } else {
    var percent = (DAM * 100) / getMaxHP();
    initialData.health -= percent;
    if (initialData.health < 0) {
      initialData.health = 0;
      appendFeedback("HP: (0)[UNCONSCIOUS]", "error");
      // alert('Unconscious - 0 HP')
      return;
    }
    appendFeedback(`-${DAM} HP`, "hp");

    // Roll unconsciousness
    appendFeedback(`PLAYERS: roll KO% vs ${DAM}`, "alert");

    const d100 = Math.floor(Math.random() * 100) + 1;
    // console.debug(`---- d% is ${d100} vs ${DAM} for KO`)

    // if (d100 <= DAM) appendFeedback(`NPCS: KO ${d100}% under ${DAM}`, "error");
    // else appendFeedback(`NPCS: OK ${d100}% over ${DAM}`, "relax");
  }
}

function hurtArmor(DAM) {
  // console.debug(`-- hurtArmor(${DAM})`)
  const AR = initialData.armor_rating;
  if (DAM > AR) {
    appendFeedback("Armor is depleted!", "alert");
    appendFeedback(`-${AR} ARMOR`, "ar");
    initialData.armor_rating = 0;
    srToHitPoints(DAM - AR);
  } else {
    initialData.armor_rating -= DAM;
    appendFeedback(`-${DAM} ARMOR`, "ar");
  }
  document.getElementById("armor_rating").value = initialData.armor_rating;
}

function getCurrentPower() {
  return getMaxPower() - initialData.spent_power;
}

function getMaxPower() {
  return current.MAX_POWER;
}

function getMaxHP() {
  return current.MAX_HP;
}

function getCurrentHP() {
  return Math.ceil(getMaxHP() * initialData.health);
}

function handleApplyDamage() {
  // The damage in the calc window
  const DAM = parseInt(document.getElementById("damage").value) || 0;
  const DAM_IN_QUEUE = damage_queue[0] || 0;

  // If we are taking the top value of damage queue then clear it
  if (DAM && DAM === DAM_IN_QUEUE) {
    damage_queue.shift();
    redrawDamageQueue();
  }

  // User feedback
  appendFeedback(`${DAM} HP`, "damage");
  appendFeedback(`Taking ${DAM} damage direct to HP`, "note");

  // Simple logic
  directToHitPoints(DAM);

  // Finally clear the input box and refresh move to next queue value or 0
  document.getElementById("damage").value = damage_queue[0] || 0;
  saveJsonToCookie();
  regeneratePage();
}

function handleRollWDamage() {
  // The damage in the calc window
  const DAM = parseInt(document.getElementById("damage").value) || 0;
  const DAM_IN_QUEUE = damage_queue[0] || 0;

  // If we are taking the top value of damage queue then clear it
  if (DAM && DAM === DAM_IN_QUEUE) {
    damage_queue.shift();
    redrawDamageQueue();
  }

  // Let the user know whats about to happens
  appendFeedback(`${DAM} HP`, "damage");
  appendFeedback(`Rolling with ${DAM} damage`, "note");

  // Damage and overflow logic all here
  var ROLLS_WITH = Math.floor(getCurrentPower() / 10);
  if (DAM <= ROLLS_WITH) directToPower(DAM);
  else {
    directToPower(ROLLS_WITH);
    testToHitPoints(DAM - ROLLS_WITH);
  }

  // Finally clear the input box and refresh move to next queue value or 0
  document.getElementById("damage").value = damage_queue[0] || 0;
  saveJsonToCookie();
  regeneratePage();
}

function testToHitPoints(DAM) {
  const d100 = Math.floor(Math.random() * 100) + 1;
  const AR = parseInt(document.getElementById("armor_rating").value) || 0;
  if (d100 <= AR) {
    appendFeedback(`Armor wins ${d100}% under AR ${AR}`, "note");
    hurtArmor(DAM);
  } else {
    if (AR > 0) appendFeedback(`Armor loses ${d100}% over AR ${AR}`, "note");
    srToHitPoints(DAM);
  }
}

function handleTestedDamage() {
  // The damage in the calc window
  const DAM = parseInt(document.getElementById("damage").value) || 0;
  const DAM_IN_QUEUE = damage_queue[0] || 0;

  // If we are taking the top value of damage queue then clear it
  if (DAM && DAM === DAM_IN_QUEUE) {
    damage_queue.shift();
    redrawDamageQueue();
  }

  // User feedback
  appendFeedback(`${DAM} HP`, "damage");
  appendFeedback(`Taking ${DAM} damage to defenses`, "note");

  // logic here trivial
  testToHitPoints(DAM);

  // Finally clear the input box and refresh move to next queue value or 0
  document.getElementById("damage").value = damage_queue[0] || 0;
  saveJsonToCookie();
  regeneratePage();
}

// Drag modal
function dragStart(e) {
  var modalContent = document.querySelector(".modal-content");

  // Check if the target element is a button or an input
  if (
    e.target.tagName.toLowerCase() !== "button" &&
    e.target.tagName.toLowerCase() !== "input"
  ) {
    initialX = e.clientX - xOffset;
    initialY = e.clientY - yOffset;

    if (e.target === modalContent || modalContent.contains(e.target)) {
      activeModal = modalContent;
    }
  }
}

function dragEnd(e) {
  initialX = currentX;
  initialY = currentY;

  activeModal = null;

  // Restore text selection behavior
  document.body.style.userSelect = "auto";
}

function drag(e) {
  if (activeModal) {
    e.preventDefault();

    // Disable text selection while dragging
    document.body.style.userSelect = "none";

    if (e.type === "mousemove") {
      currentX = e.clientX - initialX;
      currentY = e.clientY - initialY;

      xOffset = currentX;
      yOffset = currentY;

      setTranslate(currentX, currentY, activeModal);
    }
  }
}

function setTranslate(xPos, yPos, el) {
  el.style.transform = `translate3d(${xPos}px, ${yPos}px, 0)`;
}

document.addEventListener("DOMContentLoaded", function () {
  // Get all the elements with the "clickable" class
  const clickableSpans = document.getElementsByClassName("clickable");

  document.getElementById("background").addEventListener("input", handleSubmit);
  document.getElementById("notes").addEventListener("input", handleSubmit);
  document
    .getElementById("saveButton")
    .addEventListener("click", handleSaveClick);
  document
    .getElementById("loadButton")
    .addEventListener("click", handleLoadClick);
  document
    .getElementById("viewButton")
    .addEventListener("click", handleViewClick);
  document
    .getElementById("trashButton")
    .addEventListener("click", loadDefaultData);
  document
    .getElementById("connectButton")
    .addEventListener("click", networkPanel);
  document
    .getElementById("joinButton")
    .addEventListener("click", connectToMqttBroker);

  document
    .getElementById("hideCharacterBuilderBtn")
    .addEventListener("click", toggleCharacterBuilder);
  document
    .getElementById("hideCharacterBuilderBtn")
    .addEventListener("click", handleSubmit);
  document
    .getElementById("toggleCalcPanelBtn")
    .addEventListener("click", toggleCalcPanel);

  document
    .getElementById("applyDamageBtn")
    .addEventListener("click", handleApplyDamage);
  document
    .getElementById("rollDamageBtn")
    .addEventListener("click", handleRollWDamage);
  document
    .getElementById("testDamageBtn")
    .addEventListener("click", handleTestedDamage);
  document.getElementById("healBtn").addEventListener("click", handleHealing);
  document
    .getElementById("restoreBtn")
    .addEventListener("click", handleRestore);
  document.getElementById("evading").addEventListener("click", handleEvade);
  document.getElementById("restBtn").addEventListener("click", handleRest);

  document
    .getElementById("useCookie")
    .addEventListener("change", handleCookieCheckbox);

  document
    .querySelector(".modal-content")
    .addEventListener("mousedown", dragStart);
  document.querySelector(".modal-content").addEventListener("mouseup", dragEnd);
  document.querySelector(".modal-content").addEventListener("mousemove", drag);

  Coloris.setInstance(".instance2", {
    theme: "polaroid",
    alpha: false,
    format: "hex"
  });

  document
    .getElementById("colorPickerButton")
    .addEventListener("change", handleColorChange);
});

function handleEvade() {
  is_evading = !is_evading;
  const evadingCheckbox = document.getElementById("evading");
  const acBox = document.getElementById("AC_box");

  evadingCheckbox.checked = is_evading;
  if (is_evading) {
    acBox.classList.add("evading");
    initialData.spent_power += 1;
    flashScreen("power");
    appendFeedback(`Using Evade for 1 PR`, "note");
    appendFeedback(`-1 PR`, "pr");
  } else {
    acBox.classList.remove("evading");
  }

  console.warn(`Evading is ${is_evading}`);
  regeneratePage();
}

function updateEvadeAnimation() {
  const acBox = document.getElementById("AC_box");
  if (is_evading) {
    acBox.classList.add("evading");
  } else {
    acBox.classList.remove("evading");
  }
}

function attemptReconnect() {
  console.log("Attempting to reconnect to MQTT broker...");
  connectToMqttBroker();
}

function scheduleReconnect() {
  clearInterval(reconnectInterval);

  if (reconnectAttempts < 5) {
    // Attempt reconnection every 3 seconds for the first 5 attempts
    reconnectInterval = setInterval(attemptReconnect, 3000);
  } else if (reconnectAttempts < 8) {
    // Attempt reconnection every 10 seconds for the next 3 attempts
    reconnectInterval = setInterval(attemptReconnect, 10000);
  } else {
    // Attempt reconnection every minute after 8 attempts
    reconnectInterval = setInterval(attemptReconnect, 60000);
  }

  reconnectAttempts++;
}

function connectToMqttBroker() {
  let time_to_go = false;
  if (mqtt_connected) {
    disconnectFromMqttBroker();
    return;
  }
  const clientId = "mqttjs_" + Math.random().toString(16).substr(2, 8);
  document.getElementById("con_state").innerText = "...";
  const topic_stub = `${networkSettings.game_id}/${networkSettings.player_id}`;
  const host = `wss://${networkSettings.broker}:${networkSettings.port}/mqtt`;
  const options = {
    keepalive: 60,
    clientId: clientId,
    protocolId: "MQTT",
    protocolVersion: 4,
    username: networkSettings.username,
    password: networkSettings.password,
    clean: true,
    reconnectPeriod: 1000,
    connectTimeout: 30 * 1000,
    will: {
      topic: topic_stub + "/lwt",
      payload: "OFFLINE",
      qos: 0,
      retain: false
    }
  };
  console.log("Connecting mqtt client");
  console.debug(host);
  console.debug(options);

  client = mqtt.connect(host, options);

  client.on("error", (err) => {
    console.error("Connection error: ", err);
    mqtt_connected = false;
    client.end();
  });

  let reconnectDelay = 1000; // Initial reconnect delay
  const maxReconnectDelay = 60000; // Maximum reconnect delay (e.g., 60 seconds)

  client.on("connect", () => {
    console.log("Connected to MQTT broker");
    startHeartbeat();
    mqtt_connected = true;
    document.getElementById("connectButton").classList.add("advantage");
    document.getElementById("connectButton").classList.remove("disadvantage");
    document.getElementById("con_state").innerText = "Connected";
    document.getElementById("joinButton").innerText = "LEAVE";
    networkPanel();
    // Reset reconnection attempts and clear reconnection interval
    reconnectAttempts = 0;
    clearInterval(reconnectInterval);

    // Subscribe to topics or perform other necessary actions
    const qos = 0;
    client.subscribe(topic_stub + "/set", { qos }, (err) => {
      if (err) {
        console.error("Failed to subscribe to topic:", err);
      } else {
        console.log("Subscribed to topic:", topic_stub + "/#");
      }
    });
    publishPulse(); //  goes before will message to ensure data is present for lwt handler
    publishMessage(topic_stub + "/lwt", "ONLINE");
  });
  let reconnectAttempts = 0;
  const maxReconnectAttempts = 5;

  client.on("reconnect", () => {
    console.log("Reconnecting...");
    mqtt_connected = false;
    document.getElementById("connectButton").classList.remove("advantage");
    document.getElementById("connectButton").classList.add("disadvantage");
    document.getElementById("con_state").innerText = "Reconnecting...";
    document.getElementById("joinButton").innerText = "...";

    reconnectAttempts++;
    if (reconnectAttempts >= 10) {
      console.log("Maximum reconnection attempts reached. Disconnecting.");
      // ... (existing code) ...
      client.end();
      reconnectAttempts = 0;
      clearInterval(reconnectInterval);
    }
  });
  client.on("close", () => {
    console.log("Connection closed");
    mqtt_connected = false;
    publishMessage(topic_stub + "/lwt", "OFFLINE");
    // Perform any necessary cleanup or reconnection logic
    document.getElementById("connectButton").classList.remove("advantage");
    document.getElementById("connectButton").classList.add("disadvantage");
    document.getElementById("con_state").innerText = "Disconnected";
    document.getElementById("joinButton").innerText = "JOIN";
    // Start the reconnection process
    scheduleReconnect();
  });
  client.on("message", (topic, message) => {
    processMqttEvent(topic, message);
  });
}

function startHeartbeat() {
  if (!heartbeatInterval) {
    heartbeatInterval = setInterval(publishPulse, HEARTBEAT_INTERVAL);
    console.log("Heartbeat started");
  }
}

function stopHeartbeat() {
  if (heartbeatInterval) {
    clearInterval(heartbeatInterval);
    heartbeatInterval = null;
    console.log("Heartbeat stopped");
  }
}
function networkPanel() {
  const networkPanel = document.getElementById("network-panel");

  if (networkPanel.style.display === "block") {
    // Hide the network panel after the fade-out transition
    networkPanel.style.display = "none";
  } else {
    // Show the network panel
    networkPanel.style.display = "block";
  }
}

function handleColorChange() {
  const color = document.getElementById("colorPickerButton").value;
  console.debug(color);
  cookieData.color = color;
  regeneratePage();
}

function roll_pool(crit_range, adv, disadv, verbose = false) {
  const prime = Math.floor(Math.random() * 20) + 1;
  if (verbose) appendFeedback(`Prime die ${prime}`, "note");

  const disadvantageDice = Array.from(
    { length: disadv },
    () => Math.floor(Math.random() * 20) + 1
  ).sort((a, b) => b - a);
  if (verbose && disadv > 0)
    appendFeedback(`Disadvatage dice ${disadvantageDice}`, "note");
  const advantageDice = Array.from(
    { length: adv },
    () => Math.floor(Math.random() * 20) + 1
  ).sort((a, b) => b - a);
  if (verbose && adv > 0)
    appendFeedback(`Advantage dice ${advantageDice}`, "note");

  const critHits =
    advantageDice.filter((d) => d >= 21 - crit_range).length +
    (prime >= 21 - crit_range ? 1 : 0);
  const critFails =
    disadvantageDice.filter((d) => d <= crit_range).length +
    (prime <= crit_range ? 1 : 0);

  if (gameSettings.crit_fishing) {
    if (critHits > 0 && critHits === critFails) {
      // Dramatic Critical
      return -1;
    } else if (critHits > critFails) {
      // Crit success
      return 20;
    } else if (critFails > critHits) {
      // Crit fail
      return 1;
    }
  }

  while (advantageDice.length > 0 && disadvantageDice.length > 0) {
    advantageDice.pop();
    disadvantageDice.shift();
  }

  if (advantageDice.length > 0) {
    return Math.max(prime, ...advantageDice);
  } else if (disadvantageDice.length > 0) {
    return Math.min(prime, ...disadvantageDice);
  }

  return prime;
}

function roll_ensemble() {
  const crit_range = gameSettings.crit_range | 1;
  const disadv = cookieData.num_disadvantage | 0;
  const adv = cookieData.num_advantage | 0;
  const negate = gameSettings.dis_adv_negate;

  document.getElementById("dice_audio").play();

  if (negate && adv > 0 && disadv > 0) {
    if (adv >= disadv) {
      cookieData.num_advantage -= cookieData.num_disadvantage;
      cookieData.num_disadvantage = 0;
    } else {
      cookieData.num_disadvantage -= cookieData.num_advantage;
      cookieData.num_advantage = 0;
    }
    drawAdvantagePool();
    drawDisadvantagePool();
  }
  // appendFeedback(`Rolling 10000 checks`, "note");
  // appendFeedback(`Crit Range is ${crit_range}`, "note");
  // appendFeedback(`Rolling ${adv} ADVANTAGE`, "note");
  // appendFeedback(`Rolling ${disadv} DISADVANTAGE`, "note");

  const result = roll_pool(crit_range, adv, disadv, true);
  console.warn(`Roll result: ${result}`);
  appendFeedback(`${result}`, "roll");

  // const histogram = Array(21).fill(0);
  // for (let i = 0; i < 10000; i++) {
  //   const rollResult = roll_pool(crit_range, adv, disadv);
  //   histogram[rollResult === -1 ? 0 : rollResult]++;
  // }

  // const histogramDiv = document.getElementById("histogram");
  // histogramDiv.innerHTML = "";
  // for (let i = 0; i <= 20; i++) {
  //   const bar = document.createElement("div");
  //   bar.className = i === result ? "redbar" : "bar";
  //   bar.id = `r${i}`;
  //   bar.style.height = `${(histogram[i] / 10000) * 1000}%`;
  //   histogramDiv.appendChild(bar);
  // }

  return result;
}

function incrementAdvantage() {
  const pool = cookieData.num_advantage;
  if (pool === undefined || isNaN(pool)) {
    cookieData.num_advantage = 0;
  }
  if (cookieData.num_advantage < gameSettings.max_dice_pool)
    cookieData.num_advantage += 1;
  console.warn(`incrementAdvantage ${cookieData.num_advantage}`);
  drawAdvantagePool();
}
function incrementDisadvantage() {
  const pool = cookieData.num_disadvantage;
  if (pool === undefined || isNaN(pool)) {
    cookieData.num_disadvantage = 0;
  }
  if (cookieData.num_disadvantage < gameSettings.max_dice_pool)
    cookieData.num_disadvantage += 1;
  console.warn(`incrementDisadvantage ${cookieData.num_disadvantage}`);
  drawDisadvantagePool();
}
function decrementAdvantage() {
  const pool = cookieData.num_advantage;
  if (pool === undefined || isNaN(pool)) {
    cookieData.num_advantage = 0;
  }
  if (cookieData.num_advantage > 0) cookieData.num_advantage -= 1;
  drawAdvantagePool();
  console.warn(`decrementAdvantage ${cookieData.num_advantage}`);
}
function decrementDisadvantage() {
  const pool = cookieData.num_disadvantage;
  if (pool === undefined || isNaN(pool)) {
    cookieData.num_disadvantage = 0;
  }
  if (cookieData.num_disadvantage > 0) cookieData.num_disadvantage -= 1;
  drawDisadvantagePool();
  console.warn(`decrementDisadvantage ${cookieData.num_disadvantage}`);
}

function drawAdvantagePool() {
  const Pool = document.getElementById("pool_adv");
  Pool.innerHTML = "";
  for (let i = 0; i < cookieData.num_advantage; i++) {
    const advSVGElement = document.createElement("span");
    advSVGElement.innerHTML = green_dice;
    Pool.appendChild(advSVGElement);
  }
}

function drawDisadvantagePool() {
  const Pool = document.getElementById("pool_disadv");
  Pool.innerHTML = "";
  for (let i = 0; i < cookieData.num_disadvantage; i++) {
    const advSVGElement = document.createElement("span");
    advSVGElement.innerHTML = red_dice;
    Pool.appendChild(advSVGElement);
  }
}

function publishMessage(topic, message) {
  if (mqtt_connected) {
    const jsonPayload = JSON.stringify(message);
    client.publish(topic, jsonPayload, { qos: 0, retain: false }, (err) => {
      if (err) {
        console.error("Failed to publish message:", err);
      } else {
        console.log("Message published to topic:", topic);
      }
    });
  } else {
    console.log("MQTT client is not connected. Cannot publish message.");
  }
}
function createDataPacket() {
  let [current_AC, evade] = get_current_ac();
  const dataPacket = {
    name: initialData.name,
    identity: initialData.identity,
    level: initialData.level,
    armor_rating: initialData.armor_rating,
    current_hp: Math.round(current.MAX_HP * (initialData.health / 100)).toFixed(
      0
    ),
    current_power: current.MAX_POWER - initialData.spent_power,
    color: cookieData.color,
    ac: current_AC,
    evade: evade,
    sr: modData.sr,
    dr: modData.dr,
    attacks: modData.attacks,
    immune: modData.immune,
    resist: modData.resist,
    vulnerable: modData.vulnerable,
    strength: current.STRENGTH,
    endurance: current.ENDURANCE,
    agility: current.AGILITY,
    intelligence: current.INTELLIGENCE,
    charisma: current.CHARISMA,
    weight: current.WEIGHT,
    move: current.MOVE,
    max_hp: current.MAX_HP,
    max_power: current.MAX_POWER,
    lift: current.MAX_LIFT,
    blocks: current.BLOCKS
  };

  return dataPacket;
}
function publishPulse() {
  const dataPacket = createDataPacket();
  const topic = `${networkSettings.game_id}/${networkSettings.player_id}`;
  if (mqtt_connected) {
    publishMessage(topic, dataPacket);
    publishMessage(topic + "/lwt", "ONLINE");
  }
  const jsonData = JSON.stringify(dataPacket, null, 2);
  document.getElementById("npcViewer").value = jsonData;
}

function processMqttEvent(topic, message) {
  try {
    const jsonMessage = JSON.parse(message.toString());
    if (jsonMessage.hasOwnProperty("damage")) {
      const damageValue = jsonMessage.damage;
      if (typeof damageValue === "number") {
        if (damageValue) damage_queue.push(damageValue);
        document.getElementById("damage_audio").play();
        flashScreen("damage");
        console.log("Damage value added to the queue:", damageValue);
        redrawDamageQueue();
      } else {
        console.log("Invalid damage value received:", damageValue);
      }
    }
    if (jsonMessage.hasOwnProperty("action")) {
      const action = jsonMessage.action;
      if (action === "reset") {
        is_evading = false;
        regeneratePage();
      }
    }
  } catch (error) {
    console.error("Failed to parse MQTT message as JSON:", error);
  }
}

function redrawDamageQueue() {
  const damageColumn = document.getElementById("damage_column");
  damageColumn.innerHTML = "";
  damage_queue = damage_queue.filter((value) => value !== 0);

  damage_queue.forEach((damageValue) => {
    const damageItem = document.createElement("div");
    damageItem.classList.add("damage-item");
    damageItem.textContent = damageValue;
    damageColumn.appendChild(damageItem);
  });

  // Update the number in damage box to be 0 or the first in queue
  document.getElementById("damage").value = damage_queue[0] || 0;
}

function disconnectFromMqttBroker() {
  if (mqtt_connected) {
    const topic_stub = `${networkSettings.game_id}/${networkSettings.player_id}`;
    publishMessage(topic_stub + "/lwt", "OFFLINE");
    stopHeartbeat();
    // Unsubscribe from all topics
    client.unsubscribe("#", (err) => {
      if (err) {
        console.error("Failed to unsubscribe from topics:", err);
      } else {
        console.log("Unsubscribed from all topics");
      }

      // Close the MQTT connection
      client.end(true, () => {
        console.log("Disconnected from MQTT broker");
        mqtt_connected = false;
        document.getElementById("connectButton").classList.remove("advantage");
        document
          .getElementById("connectButton")
          .classList.remove("disadvantage");
        document.getElementById("con_state").innerText = "Disconnected";
        document.getElementById("joinButton").innerText = "JOIN";
      });
    });
  } else {
    console.log("MQTT client is not connected.");
  }
}
