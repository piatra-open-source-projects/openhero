let client;
let mqtt_connected = false;
let reconnectInterval;
let reconnectAttempts = 0;
const maxReconnectAttempts = 10;
const initialReconnectDelay = 1000; // 1 second
const maxReconnectDelay = 60000; // 1 minute

let player_queue = [];
let active_players = [];
let active_npcs = [];
let npc_queue = [];
let current_shown = {};
let current_is_player = false;
let combatants = [];
let current_round = 0;
let initiativeRolls = {};
let combatantMovement = {};

// Variables to store the dragging state and initial position
let isDragging = false;
let initialX;
let initialY;
let offsetX = 0;
let offsetY = 0;

let networkSettings = {
  broker: "broker.emqx.io",
  port: "8084",
  username: "player",
  password: "OpenHero_v1.0",
  game_id: "default"
};

const gameSettings = {
  min_effective_weight: 25,
  max_effective_weight: 100000,
  hp_level_scaling: 0.1,
  kilo_per_block: 25,
  hp_per_flesh_block: 2,
  base_ac: 10,
  crit_range: 1,
  crit_fishing: true,
  max_dice_pool: 4,
  dis_adv_negate: false
};

const monster = {
  name: "",
  level: 4,
  armor_rating: 0,
  health: 100,
  spent_power: 0,
  ac: 0,
  sr: 0,
  dr: 0,
  strength: 10,
  endurance: 10,
  agility: 10,
  intelligence: 10,
  charisma: 10,
  weight: 77,
  move: 30,
  max_hp: 8,
  max_power: 40,
  lift: 75,
  blocks: 4
};

// Global character data structure
let initialData = {
  name: "default",
  identity: "default",
  agility: 10,
  endurance: 10,
  strength: 10,
  intelligence: 10,
  charisma: 10,
  weight: 65,
  level: 1,
  spent_power: 0,
  armor_rating: 0,
  health: 100.0,
  powers: [],
  background: "",
  notes: "",
  overrides: [],
  image: "default.png"
};

let modData = {
  agility: 0,
  endurance: 0,
  strength: 0,
  intelligence: 0,
  charisma: 0,
  mv: 0,
  ac: 0,
  sr: 0,
  dr: 0,
  wt: ""
};

let current = {
  STRENGTH: 0,
  ENDURANCE: 0,
  AGILITY: 0,
  INTELLIGENCE: 0,
  CHARISMA: 0,
  BLOCKS: 1,
  WEIGHT: 1,
  MAX_HP: 0,
  MAX_POWER: 0,
  MAX_LIFT: 0,
  MOVE: 0
};

let cookieData = {
  color: "#ff8c00",
  save: true,
  showAdmonition: false,
  num_advantage: 0,
  num_disadvantage: 0
};

const green_dice =
  '<svg class="adv" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.47 6.62L12.57 2.18C12.41 2.06 12.21 2 12 2S11.59 2.06 11.43 2.18L3.53 6.62C3.21 6.79 3 7.12 3 7.5V16.5C3 16.88 3.21 17.21 3.53 17.38L11.43 21.82C11.59 21.94 11.79 22 12 22S12.41 21.94 12.57 21.82L20.47 17.38C20.79 17.21 21 16.88 21 16.5V7.5C21 7.12 20.79 6.79 20.47 6.62M11.45 15.96L6.31 15.93V14.91C6.31 14.91 9.74 11.58 9.75 10.57C9.75 9.33 8.73 9.46 8.73 9.46S7.75 9.5 7.64 10.71L6.14 10.76C6.14 10.76 6.18 8.26 8.83 8.26C11.2 8.26 11.23 10.04 11.23 10.5C11.23 12.18 8.15 14.77 8.15 14.77L11.45 14.76V15.96M17.5 13.5C17.5 14.9 16.35 16.05 14.93 16.05C13.5 16.05 12.36 14.9 12.36 13.5V10.84C12.36 9.42 13.5 8.27 14.93 8.27S17.5 9.42 17.5 10.84V13.5M16 10.77V13.53C16 14.12 15.5 14.6 14.92 14.6C14.34 14.6 13.86 14.12 13.86 13.53V10.77C13.86 10.18 14.34 9.71 14.92 9.71C15.5 9.71 16 10.18 16 10.77Z" /></svg>';
const red_dice =
  '<svg class="disadv" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.47 6.62L12.57 2.18C12.41 2.06 12.21 2 12 2S11.59 2.06 11.43 2.18L3.53 6.62C3.21 6.79 3 7.12 3 7.5V16.5C3 16.88 3.21 17.21 3.53 17.38L11.43 21.82C11.59 21.94 11.79 22 12 22S12.41 21.94 12.57 21.82L20.47 17.38C20.79 17.21 21 16.88 21 16.5V7.5C21 7.12 20.79 6.79 20.47 6.62M11.45 15.96L6.31 15.93V14.91C6.31 14.91 9.74 11.58 9.75 10.57C9.75 9.33 8.73 9.46 8.73 9.46S7.75 9.5 7.64 10.71L6.14 10.76C6.14 10.76 6.18 8.26 8.83 8.26C11.2 8.26 11.23 10.04 11.23 10.5C11.23 12.18 8.15 14.77 8.15 14.77L11.45 14.76V15.96M17.5 13.5C17.5 14.9 16.35 16.05 14.93 16.05C13.5 16.05 12.36 14.9 12.36 13.5V10.84C12.36 9.42 13.5 8.27 14.93 8.27S17.5 9.42 17.5 10.84V13.5M16 10.77V13.53C16 14.12 15.5 14.6 14.92 14.6C14.34 14.6 13.86 14.12 13.86 13.53V10.77C13.86 10.18 14.34 9.71 14.92 9.71C15.5 9.71 16 10.18 16 10.77Z" /></svg>';
const shield =
  '<svg class="shield" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,1L3,5V11C3,16.55 6.84,21.74 12,23C17.16,21.74 21,16.55 21,11V5M11,7H13V13H11M11,15H13V17H11" /></svg>';
const smalldot =
  '<svg class="smalldot" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,10A2,2 0 0,0 10,12C10,13.11 10.9,14 12,14C13.11,14 14,13.11 14,12A2,2 0 0,0 12,10Z" /></svg>';
const roll_with_icon =
  '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="m 16.532695,7.6818285 c 0.943968,0.545 2.192051,0.2032567 2.732051,-0.7320508 0.56,-0.9699484 0.211917,-2.1870508 -0.732051,-2.7320508 -0.961288,-0.555 -2.172051,-0.2378976 -2.732051,0.7320508 -0.54,0.9353075 -0.229237,2.1770508 0.732051,2.7320508 M 6.4663432,17.897261 9.5223686,14.60407 l 0.8273134,2.787051 -2.9999998,5.196152 1.7320508,1 3.75,-6.49519 -0.827314,-2.787051 2.028276,-2.293076 c 0.367173,1.944038 1.599223,3.810063 3.51314,4.915063 l 1,-1.732051 c -1.654109,-0.955 -2.531089,-2.616025 -2.52257,-4.250781 L 15.94724,9.0758666 C 15.91083,8.3389309 15.581214,7.7098412 14.983657,7.3648412 14.715189,7.2098412 14.510644,7.1841233 14.242176,7.0291233 l -5.5946716,-0.6897442 -2.36,4.0876399 1.7320508,1 1.71,-2.9618068 L 11.629741,8.7539944 6.1841,14.986121 2.4405755,11.670095 1.0941654,13.202146 Z" id="path427" /><path d="m 17.000895,21.683214 v -1.5 c 2.21,0 4,-1.79 4,-4 0,-0.82 -0.25,-1.58 -0.67,-2.21 l -1.09,1.09 c 0.17,0.34 0.26,0.72 0.26,1.12 0,1.38 -1.12,2.5 -2.5,2.5 v -1.5 l -2.25,2.25 2.25,2.25 M 5.0345203,9.6845095 l 1.09,-1.09 c -0.17,-0.34 -0.26,-0.72 -0.26,-1.12 0,-1.38 1.12,-2.5000005 2.5,-2.5000005 V 6.4745095 L 10.61452,4.224509 c -1.4467317,-1.047876 -2.2499997,-3.47626201 -2.2499997,-0.75 -2.21,0 -4,1.79 -4,4.0000005 0,0.82 0.25,1.58 0.67,2.21 z" id="path702" /></svg>';
const punch_icon =
  '<svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 670 670"><path d="m 328.17306,646.33051 c -2.88957,-6.14167 -7.93211,-16.71667 -11.20563,-23.5 -3.27352,-6.78333 -8.43344,-17.58333 -11.46648,-24 -3.03304,-6.41667 -7.53244,-15.86667 -9.99868,-21 -2.46624,-5.13333 -7.32136,-15.33333 -10.78916,-22.66667 -3.4678,-7.33333 -6.56372,-13.59828 -6.87982,-13.92211 -0.32973,-0.33781 -2.17384,0.31371 -4.32628,1.52847 -5.38732,3.04042 -30.42471,17.04547 -46.00419,25.73316 -26.85182,14.97356 -43.54039,24.28862 -52.32273,29.20498 -4.94416,2.76776 -9.07085,4.95082 -9.17041,4.85125 -0.0996,-0.0996 0.42964,-8.40434 1.17601,-18.45505 0.74637,-10.05072 2.24383,-31.32403 3.32768,-47.27403 1.08385,-15.95 2.31405,-33.58583 2.73377,-39.19072 0.41972,-5.60489 0.60488,-10.34897 0.41146,-10.54239 -0.39909,-0.39908 -5.9901,1.23074 -26.48912,7.7218 -7.88333,2.49627 -23.18333,7.2987 -33.99999,10.67207 -10.81667,3.37337 -25.216668,7.877 -31.999998,10.00807 -6.78334,2.13106 -18.78334,5.87382 -26.66667,8.31725 -7.88333,2.44343 -15.49578,4.88369 -16.91655,5.42282 -1.42076,0.53913 -2.72162,0.84182 -2.89078,0.67266 -0.16916,-0.16917 9.41789,-14.80672 21.30457,-32.5279 35.59337,-53.06415 52.525758,-78.50142 52.842868,-79.385 0.18061,-0.50327 -2.41127,-2.4132 -6.52016,-4.80463 -3.750978,-2.18312 -10.419948,-6.10958 -14.819948,-8.72546 -4.4,-2.61588 -16.25,-9.64796 -26.33333,-15.62685 -10.08334,-5.97889 -20.43334,-12.12562 -23,-13.65941 -2.56667,-1.53378 -8.36397,-4.98249 -12.8829,-7.6638 -6.66518,-3.95479 -8.04955,-5.03115 -7.33333,-5.70174 0.48559,-0.45464 10.3329,-9.16191 21.88289,-19.34948 39.63028,-34.9555 46.66667,-41.22828 46.66667,-41.60227 0,-0.38296 -10.94701,-12.30819 -35.22104,-38.36835 -7.51379,-8.06667 -15.48788,-16.62805 -17.7202,-19.02529 C 16.838792,225.51362 8.1371524,215.9974 7.9472724,215.45974 c -0.12852,-0.36391 2.2608596,-0.6283 5.6666596,-0.62704 3.23889,0.001 24.03889,-0.29935 46.22222,-0.66791 22.18334,-0.36856 45.642418,-0.75363 52.131298,-0.85571 6.48888,-0.10208 11.98462,-0.37226 12.21274,-0.60039 0.36163,-0.36163 -6.98817,-39.46548 -15.05389,-80.09269 -2.87206,-14.46662 -3.02256,-15.79249 -1.79015,-15.77087 0.45834,0.008 25.42141,4.94497 55.47349,10.97095 30.05209,6.02598 54.71321,10.88329 54.80249,10.794 0.22113,-0.22112 3.36676,-15.00564 8.96016,-42.112908 2.57241,-12.46666 5.85862,-28.36666 7.30268,-35.33333 1.44406,-6.96667 4.22466,-20.39025 6.17912,-29.83019 1.95445,-9.43994 3.73958,-17.34955 3.96694,-17.57692 0.22738,-0.22737 1.10379,0.38068 1.9476,1.35122 17.52983,20.16283 89.73317,100.967988 90.25276,101.004868 0.39522,0.0281 9.44145,-8.87398 20.10273,-19.782308 10.66129,-10.90834 28.18792,-28.79707 38.94809,-39.75274 l 19.56393,-19.9194 16.92354,50.9194 c 9.30795,28.005668 17.01757,51.029028 17.1325,51.163018 0.11493,0.13399 5.09933,-2.01207 11.07646,-4.76902 14.94218,-6.89208 32.50657,-14.95608 51.8675,-23.81294 18.63751,-8.52593 32.80658,-15.038638 53.33333,-24.514298 8.79285,-4.059 14.40817,-6.31224 14.52695,-5.82919 0.1065,0.43309 -4.14256,14.26367 -9.44234,30.734638 -31.21414,97.00902 -29.46229,91.46287 -29.02476,91.8885 0.26283,0.25567 19.30515,3.72307 47.77348,8.69904 54.88725,9.59371 62.30341,10.94085 62.76344,11.40087 0.21567,0.21567 -0.0554,0.64564 -0.60233,0.95549 -0.54694,0.30985 -10.44444,7.87267 -21.99444,16.80628 -11.55,8.9336 -30.9,23.86805 -43,33.18768 -12.1,9.31962 -24.05055,18.56745 -26.55676,20.55074 l -4.55677,3.60597 48.8901,32.59281 c 26.88955,17.92604 48.73179,32.59288 48.53831,32.59298 -0.19348,9e-5 -4.39348,1.823 -9.33334,4.0509 -4.93985,2.2279 -12.88154,5.79197 -17.64821,7.92014 -4.76667,2.12818 -10.31667,4.62357 -12.33333,5.5453 -3.90271,1.78378 -14.71487,6.6713 -25.66667,11.60234 -21.13318,9.51522 -23.04353,10.47312 -22.69546,11.38016 0.19469,0.50737 11.25492,17.17773 24.57827,37.04524 48.57892,72.4399 55.27194,82.52356 54.97318,82.82232 -0.29246,0.29246 -9.24047,-0.86576 -56.52265,-7.31618 -11.91667,-1.62572 -27.51667,-3.73468 -34.66667,-4.68658 -7.15,-0.95189 -15.62112,-2.11819 -18.82471,-2.59177 -3.20358,-0.47358 -6.07574,-0.69095 -6.38257,-0.48305 -0.30683,0.20791 -1.684,6.52802 -3.06037,14.04468 -1.37638,7.51667 -3.3521,18.31667 -4.39049,24 -1.0384,5.68334 -3.28473,17.83334 -4.99185,27 -1.70712,9.16667 -4.29781,23.26524 -5.75709,31.33016 -1.45927,8.06492 -2.86466,14.87346 -3.12307,15.13011 -0.25841,0.25665 -5.18,-2.59192 -10.93685,-6.33015 -5.75686,-3.73823 -13.70686,-8.88624 -17.66667,-11.44001 -19.96309,-12.87465 -34.37587,-22.19705 -39.18794,-25.34735 -18.26136,-11.9551 -34.56612,-22.34276 -35.06983,-22.34276 -0.32259,0 -16.08795,23.25 -35.03413,51.66667 -18.94617,28.41667 -34.64322,51.66667 -34.88232,51.66667 -0.2391,0 -2.79893,-5.025 -5.68852,-11.16667 z m 87.18552,-142.18143 c 32.79515,-2.19194 58.45491,-4.25638 66.75671,-5.37089 7.08393,-0.95101 12.19653,-3.60497 17.73063,-9.20403 2.39753,-2.42568 5.32991,-6.25939 6.5164,-8.51936 4.48874,-8.54998 8.12017,-27.22426 9.50677,-48.88762 0.84433,-13.1912 0.5486,-16.97077 -1.85011,-23.64601 -1.7332,-4.8232 -7.32887,-16.37018 -9.02605,-18.62579 -0.8642,-1.14853 -0.99846,-0.78256 -1.42014,3.87084 -0.76928,8.48923 -3.37062,17.4064 -6.82298,23.38851 -3.79586,6.57735 -8.65314,13.21551 -12.03208,16.44357 -6.30015,6.01882 -17.07678,11.00622 -27.76398,12.84912 -20.87226,3.5992 -38.75011,0.71155 -49.09282,-7.92953 -1.59179,-1.32991 -3.08973,-2.17861 -3.32876,-1.88603 -0.23903,0.29259 -2.12929,2.64811 -4.20058,5.23448 -10.39185,12.97608 -23.81254,19.88718 -41.91112,21.58251 -4.17138,0.39074 -8.07675,0.71146 -8.67861,0.71271 -2.08902,0.005 -3.43116,5.03287 -3.75564,14.07101 -0.23088,6.43138 -0.0523,9.25958 0.76121,12.05811 2.08155,7.1603 9.19497,12.68624 19.08871,14.82876 6.58998,1.42709 16.58186,1.2313 49.52244,-0.97036 z m -51.85577,-56.3191 c 2.56667,-0.49765 5.91059,-1.35184 7.43093,-1.8982 7.60802,-2.73408 16.91528,-10.89158 22.02644,-19.30545 l 2.39624,-3.94464 -0.77159,-4.42559 c -2.67122,-15.32136 -3.91591,-62.16204 -2.90774,-109.42559 0.73796,-34.59593 1.05666,-38.37435 3.86531,-45.82617 1.17975,-3.13004 3.84228,-5.63856 5.36469,-5.05436 1.65121,0.63364 3.45335,5.01875 4.63239,11.27196 0.98888,5.24465 1.13722,12.90438 1.2642,65.27524 0.17704,73.0142 0.58712,79.18613 5.7779,86.9594 5.50715,8.247 14.0789,11.77291 28.25456,11.62222 20.77914,-0.22088 32.00289,-5.7782 40.89412,-20.24829 5.53674,-9.01079 6.87365,-15.11182 8.03473,-36.66667 1.55243,-28.8201 1.46502,-82.45733 -0.16079,-98.66666 -3.01511,-30.06072 -10.29897,-65.93958 -15.08465,-74.30407 -4.32009,-7.55073 -13.61971,-14.20463 -23.35008,-16.70704 -7.5883,-1.95152 -12.65897,-1.9501 -24.66666,0.007 -14.67039,2.39098 -26.38555,2.89662 -31.70733,1.36854 -5.14838,-1.47832 -10.46888,-4.47234 -18.29267,-10.29388 -12.10057,-9.0039 -21.49061,-12.73722 -32.03643,-12.73722 -6.42394,0 -9.53606,1.36583 -21.93349,9.62605 -13.99885,9.32723 -19.5851,11.85862 -28.68836,13.00008 -6.57454,0.82438 -14.42458,-0.005 -22.42642,-2.37 -3.25341,-0.9615 -8.09341,-1.90211 -10.75554,-2.09026 -4.47787,-0.31649 -5.17664,-0.1774 -9.33334,1.8577 -3.77438,1.84793 -6.35926,4.07925 -16.15975,13.94942 -12.57194,12.66134 -14.52473,14.02716 -22.45882,15.7083 -6.12574,1.29798 -11.45748,1.21313 -22.52196,-0.35838 -5.12277,-0.7276 -10.09986,-1.32291 -11.06019,-1.32291 -5.89586,0 -18.53163,10.52874 -22.40227,18.66667 -4.55459,9.57591 -9.00337,26.21968 -11.14006,41.67719 -3.66821,26.53702 -2.74422,61.11888 2.62707,98.32281 2.31961,16.0666 7.11176,40.04626 9.63648,48.22035 5.88166,19.04261 30.87403,25.66822 51.75597,13.72079 3.25484,-1.86224 3.65266,-2.31752 3.3724,-3.85955 -3.33318,-18.33988 -3.97906,-24.74459 -5.11768,-50.74826 -0.70202,-16.03231 -0.32212,-63.79345 0.60466,-76.01956 1.11656,-14.72979 4.8357,-26.2246 8.94085,-27.63367 1.62759,-0.55866 2.08479,-0.42888 3.15796,0.89644 2.67467,3.30307 2.95595,6.11961 2.60559,26.09012 -0.56982,32.47995 -0.28308,77.0594 0.58533,91 1.852,29.73022 4.61817,40.09852 12.25117,45.9205 5.73311,4.37285 17.77941,7.74617 27.66199,7.74617 11.55695,0 17.37268,-2.62865 27.28676,-12.33333 l 6.81051,-6.66667 -0.96241,-6 c -1.58193,-9.86219 -2.14186,-32.97455 -1.56818,-64.73028 0.78751,-43.59293 1.87033,-77.88212 2.6401,-83.60305 1.31014,-9.73708 4.61796,-17.33334 7.54784,-17.33334 1.49954,0 3.65995,3.15806 4.74653,6.93839 1.78684,6.21664 2.18786,13.96373 1.63167,31.52158 -0.29978,9.46369 -0.91327,34.9067 -1.3633,56.54003 -1.308,62.87631 0.26716,78.73953 8.73025,87.9209 3.50001,3.79707 7.03302,5.87757 13.12082,7.72655 9.03342,2.74361 20.40396,3.11469 31.21425,1.01869 z" /></svg>';
const left_arrow =
  '<svg class="arrow" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M20 11v2H8l5.5 5.5-1.42 1.42L4.16 12l7.92-7.92L13.5 5.5 8 11h12z"/></svg>';
const right_arrow =
  '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M4 11v2h12l-5.5 5.5 1.42 1.42L19.84 12l-7.92-7.92L10.5 5.5 16 11H4z"/></svg>';

function adj(X, Y) {
  return (Y * (X - 10)) / 10 + 1;
}

function debounce(func, delay) {
  let timeoutId;
  return function (...args) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => func.apply(this, args), delay);
  };
}

// Function to load npc data from npcs.json
function loadPowers() {
  return new Promise((resolve, reject) => {
    // Load data from npcs.json
    fetch("../npcs.json")
      .then((response) => response.json())
      .then((data) => {
        data.forEach((npc) => {
          if (!npc.label) {
            const nameWithoutWhitespace = npc.name.replace(/\s/g, "");
            const randomNumber = Math.floor(Math.random() * 900) + 100;
            npc.label = `${nameWithoutWhitespace}${randomNumber}`;
          }
        });
        npc_queue = npc_queue.concat(data);
        console.log("NPCs loaded. Updated npc_queue:", npc_queue);
        resolve();
      })
      .catch((error) => {
        console.error("Error fetching npcs.json:", error);
        reject(error);
      });
  });
}

// Function to load the JSON data from a cookie
function loadJsonFromCookie() {
  loadDefaultData();
  initializeNetworkSettings();
  // Check if data exists in sessionStorage prefer it
  const sessionData = sessionStorage.getItem("open_hero_encounter");
  if (sessionData) {
    try {
      const parsedData = JSON.parse(sessionData);
      // Move session Data into libraryData and initialData
      if (parsedData.libraryData) {
        libraryData = parsedData.libraryData;
        initialData = { ...libraryData[0], ...libraryData[currentIndex] };
      }
      if (parsedData.cookieData) {
        cookieData = parsedData.cookieData;
      }
      if (parsedData.networkSettings) {
        networkSettings = parsedData.networkSettings;
      }
      document.getElementById("useCookie").checked = cookieData.save;

      return; // Exit the function if data is loaded from sessionStorage
    } catch (error) {
      console.error("Error parsing JSON data in sessionStorage:", error);
    }
    // Let JSON Viewer know changes
    regeneratePage();
    initializeNetworkSettings();
    return;
  }

  // OK we are still here - so use a cookie
  const cookieValue = document.cookie.replace(
    /(?:(?:^|.*;\s*)jsonData\s*\=\s*([^;]*).*$)|^.*$/,
    "$1"
  );
  if (cookieValue) {
    const decodedData = decodeURIComponent(cookieValue);
    try {
      const parsedData = JSON.parse(decodedData);
      if (parsedData.libraryData) {
        libraryData = parsedData.libraryData;
        initialData = { ...libraryData[0], ...libraryData[currentIndex] };
      }
      if (parsedData.cookieData) {
        cookieData = parsedData.cookieData;
      }
      document.getElementById("useCookie").checked = cookieData.save;
    } catch (error) {
      console.error("Error parsing JSON data in cookie:", error);
    }
    // Let JSON Viewer know changes
    regeneratePage();
    initializeNetworkSettings();
    return;
  }
}

// Function to load the default data
function loadDefaultData() {
  initialData = {
    name: "default",
    identity: "default",
    agility: 10,
    endurance: 10,
    strength: 10,
    intelligence: 10,
    charisma: 10,
    weight: 75,
    level: 1,
    background: " ",
    notes: " ",
    spent_power: 0,
    armor_rating: 0,
    health: 100,
    powers: [],
    overrides: [],
    image: "default.png"
  };

  libraryData = [initialData];
  currentIndex = 0;
}

function regeneratePage() {
  // Get the color from the cookie before drawing
  document.documentElement.style.setProperty(
    "--md-primary-fg-color",
    cookieData.color
  );
  pushDataToBuilderInputs();
  updatePlayerList();
  updateEncounterList();
  updateCombatantsList();
  displaySummary(current_shown, current_is_player);
  appendInitiativeRolls();
}

function pushDataToBuilderInputs() {
  document.getElementById("useCookie").checked = cookieData.save;
  document.getElementById("broker").value = networkSettings.broker;
  document.getElementById("port").value = networkSettings.port;
  document.getElementById("username").value = networkSettings.username;
  document.getElementById("password").value = networkSettings.password;
  document.getElementById("game_id").value = networkSettings.game_id;
}

const debouncedSaveJsonToCookie = debounce(saveJsonToCookie, 500); // Adjust the delay as needed

// Function to save the JSON data to a cookie
function saveJsonToCookie() {
  // console.debug(`saveJsonToCookie() called with ${cookieData.save}`)

  let sessionJson;
  let storedJson;
  let storedData;
  let sessionData;
  if (cookieData.save) {
    sessionData = { libraryData, cookieData, networkSettings };
    storedData = { libraryData, cookieData, networkSettings };

    // Lets encde this and quickly test for size before proceeding
    // It is likely too big so we revert to cookieData alone
    storedJson = JSON.stringify(storedData, null, 2);
    const encodedData = encodeURIComponent(storedJson);
    if (encodedData.length >= 4096) {
      console.warn(`Cookie TOO BIG ${encodedData.length} save only cookieData`);
      storedData = { cookieData, networkSettings };
    }
  } else {
    sessionData = { cookieData, networkSettings };
    storedData = { cookieData, networkSettings };
  }
  // Write data to session storage
  sessionJson = JSON.stringify(sessionData, null, 2);
  sessionStorage.setItem("open_hero_character", sessionJson);

  // Write to cookie
  storedJson = JSON.stringify({ storedData }, null, 2);
  const encodedData = encodeURIComponent(storedJson);
  const expirationDate = new Date();
  expirationDate.setDate(expirationDate.getDate() + 60);
  document.cookie = `jsonData=${encodedData}; expires=${expirationDate.toUTCString()}; path=/; SameSite=Lax;`;
  // console.log('Save JSON cookie:', storedJson)
}

function formatPower(inpuz) {
  // return parseInt(inpuz).toFixed(0)
  return inpuz;
}

// Function to generate checkboxes using the global powers data
function buildCheckboxes() {
  const checkboxTable = document.getElementById("checkboxTable");
  const weakCheckboxTable = document.getElementById("weakCheckboxTable");
  const equipCheckboxTable = document.getElementById("equipCheckboxTable");
  const skillCheckboxTable = document.getElementById("skillCheckboxTable");
  const magicCheckboxTable = document.getElementById("magicCheckboxTable");

  // Clear existing checkboxes
  checkboxTable.innerHTML = "";
  weakCheckboxTable.innerHTML = "";
  equipCheckboxTable.innerHTML = "";
  skillCheckboxTable.innerHTML = "";
  magicCheckboxTable.innerHTML = "";

  let row, cell, checkbox, label;

  // Sort the array by name so that checkboxes are in order
  powers.sort((a, b) => {
    const aName = a.name ? a.name.toLocaleLowerCase() : "";
    const bName = b.name ? b.name.toLocaleLowerCase() : "";
    return aName.localeCompare(bName);
  });

  const tables = [
    { table: checkboxTable, powers: [], className: "power-checkbox" },
    { table: weakCheckboxTable, powers: [], className: "weak-checkbox" },
    { table: equipCheckboxTable, powers: [], className: "equip-checkbox" },
    { table: skillCheckboxTable, powers: [], className: "skill-checkbox" },
    { table: magicCheckboxTable, powers: [], className: "magic-checkbox" }
  ];

  for (let i = 0; i < powers.length; i++) {
    const power = powers[i];
    const type = power.type ? power.type.toLowerCase() : "";

    let tableIndex;
    if (type.includes("equip")) {
      tableIndex = 2; // equipCheckboxTable
    } else if (type.includes("weak")) {
      tableIndex = 1; // weakCheckboxTable
    } else if (type.includes("skill")) {
      tableIndex = 3; // skillCheckboxTable
    } else if (type.includes("magic")) {
      tableIndex = 4; // magicCheckboxTable
    } else {
      tableIndex = 0; // checkboxTable
    }

    tables[tableIndex].powers.push(power);
  }

  tables.forEach(({ table, powers, className }) => {
    for (let i = 0; i < powers.length; i++) {
      const power = powers[i];

      if (i % 4 === 0) {
        row = document.createElement("tr");
        row.classList.add("check-row");
        table.appendChild(row);
      }

      cell = document.createElement("td");
      cell.classList.add("check-row");

      row.appendChild(cell);

      checkbox = document.createElement("input");
      checkbox.type = "checkbox";
      checkbox.value = i.toString(16).padStart(2, "0");
      checkbox.setAttribute("data-label", power.name);
      checkbox.onchange = function () {
        handleCheckboxChange(this);
      };
      checkbox.classList.add(className);

      label = document.createElement("label");
      label.classList.add("check-cell");
      label.textContent = power.name;

      cell.appendChild(checkbox);
      cell.appendChild(label);
    }
  });
}

function drawDefenseMatrixByType(powerArray, prefix, containerElement) {
  if (Array.isArray(powerArray)) {
    powerArray.forEach((p) => {
      const powerName = p.split(" ")[0].toLowerCase();
      const elementId = `${prefix}_${powerName}`;
      const element = containerElement.querySelector(`#${elementId}`);

      if (element) {
        let svgSource;
        let fillColor;

        // const isSlateMode = document.body.classList.contains('slate')
        if (prefix === "imm") {
          svgSource = shield;
        } else if (prefix === "vuln") {
          svgSource = green_dice;
        } else {
          svgSource = red_dice;
        }
        element.innerHTML = `<span class="twemoji ${prefix}">${svgSource}</span>`;

        // const svgBlob = new Blob([modifiedSvgSource], { type: 'image/svg+xml' })
        // const svgUrl = URL.createObjectURL(svgBlob)

        // const img = document.createElement('img')
        // img.classList.add(prefix)
        // img.src = svgUrl

        // element.replaceChildren(img)
      }
    });
  }
}

function loadJsonFile(files) {
  return new Promise((resolve) => {
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      if (file.type === "application/json") {
        const reader = new FileReader();
        reader.onload = function (e) {
          resolve(e.target.result);
        };
        reader.readAsText(file);
        return;
      }
    }
    resolve(null);
  });
}

function mergeData(data) {
  const mergedData = { ...initialData };
  for (const key in data) {
    if (mergedData.hasOwnProperty(key)) {
      mergedData[key] = data[key];
    }
  }
  return mergedData;
}

// Function to handle stats input change
function handleNetworkChange(event) {
  const inputName = event.target.name;
  const inputValue = event.target.value;
  networkSettings[inputName] = inputValue;
  debouncedSaveJsonToCookie();
}

function handleCookieCheckbox(event) {
  console.log(`Use Cookies?: ${event.target.checked}`);
  cookieData.save = event.target.checked;
  // Update cookie and refresh
  saveJsonToCookie();
}

// Function to apply overrides to the powers array
function pushOverridesToPowers() {
  if (initialData.overrides) {
    initialData.overrides.forEach((override) => {
      const existingPower = powers.find((p) => p.name === override.name);
      if (existingPower) {
        Object.assign(existingPower, override);
      } else {
        powers.push(override);
      }
    });
  }
}

function togglePasswordVisibility() {
  const passwordInput = document.getElementById("password");
  const passwordToggleIcon = document.querySelector(".password-toggle-icon");

  if (passwordInput.type === "password") {
    passwordInput.type = "text";
    passwordToggleIcon.classList.add("visible");
  } else {
    passwordInput.type = "password";
    passwordToggleIcon.classList.remove("visible");
  }
}

// Function to save the JSON data to a cookie
function saveJsonToCookie() {
  // console.debug(`saveJsonToCookie() called with ${cookieData.save}`)

  let sessionJson;
  let storedJson;
  let storedData;
  let sessionData;
  if (cookieData.save) {
    sessionData = { libraryData, cookieData, networkSettings };
    storedData = { libraryData, cookieData, networkSettings };

    // Lets encde this and quickly test for size before proceeding
    // It is likely too big so we revert to cookieData alone
    storedJson = JSON.stringify(storedData, null, 2);
    const encodedData = encodeURIComponent(storedJson);
    if (encodedData.length >= 4096) {
      console.warn(`Cookie TOO BIG ${encodedData.length} save only cookieData`);
      storedData = { cookieData, networkSettings };
    }
  } else {
    sessionData = { cookieData, networkSettings };
    storedData = { cookieData, networkSettings };
  }
  // Write data to session storage
  sessionJson = JSON.stringify(sessionData, null, 2);
  sessionStorage.setItem("open_hero_character", sessionJson);

  // Write to cookie
  storedJson = JSON.stringify({ storedData }, null, 2);
  const encodedData = encodeURIComponent(storedJson);
  const expirationDate = new Date();
  expirationDate.setDate(expirationDate.getDate() + 60);
  document.cookie = `jsonData=${encodedData}; expires=${expirationDate.toUTCString()}; path=/; SameSite=Lax;`;
  // console.log('Save JSON cookie:', storedJson)
}

function connectToMqttBroker() {
  let time_to_go = false;
  if (mqtt_connected) {
    disconnectFromMqttBroker();
    return;
  }
  const clientId = "mqttjs_" + Math.random().toString(16).substr(2, 8);
  document.getElementById("con_state").innerText = "...";
  const topic_stub = `${networkSettings.game_id}`;
  const host = `wss://${networkSettings.broker}:${networkSettings.port}/mqtt`;
  const options = {
    keepalive: 60,
    clientId: clientId,
    protocolId: "MQTT",
    protocolVersion: 4,
    username: networkSettings.username,
    password: networkSettings.password,
    clean: true,
    reconnectPeriod: 1000,
    connectTimeout: 30 * 1000,
    will: {
      topic: topic_stub + "/lwt",
      payload: "CLOSED",
      qos: 0,
      retain: false
    }
  };
  console.log("Connecting mqtt client");
  console.log(host);
  console.log(options);

  client = mqtt.connect(host, options);

  client.on("connect", () => {
    console.log("Connected to MQTT broker");
    mqtt_connected = true;
    feedbackConnected();
    // Reset reconnection attempts and clear reconnection interval
    reconnectAttempts = 0;
    clearInterval(reconnectInterval);

    client.subscribe(topic_stub + "/#", { qos: 0 }, (err) => {
      if (err) {
        console.error("Failed to subscribe to topic:", err);
      } else {
        console.log("Subscribed to topic:", topic_stub + "/#");
      }
    });
    publishMessage(topic_stub + "/lwt", "OPEN");
  });

  client.on("error", (err) => {
    console.error("Connection error:", err);
    mqtt_connected = false;
    feedbackDisconnected();
    scheduleReconnect();
  });

  client.on("close", () => {
    console.log("Connection closed");
    mqtt_connected = false;
    feedbackDisconnected();
    scheduleReconnect();
  });

  client.on("message", (topic, message) => {
    processMqttEvent(topic, message);
  });
}

function scheduleReconnect() {
  clearInterval(reconnectInterval);

  if (reconnectAttempts < maxReconnectAttempts) {
    const delay = Math.min(
      initialReconnectDelay * Math.pow(2, reconnectAttempts),
      maxReconnectDelay
    );
    console.log(`Scheduling reconnection attempt in ${delay}ms`);
    reconnectInterval = setTimeout(attemptReconnect, delay);
    reconnectAttempts++;
  } else {
    console.log(
      "Max reconnection attempts reached. Please check your connection and try again manually."
    );
    // Optionally, you can add code here to notify the user or update the UI
  }
}

function attemptReconnect() {
  console.log("Attempting to reconnect to MQTT broker...");
  connectToMqttBroker();
}

function disconnectFromMqttBroker() {
  if (mqtt_connected) {
    client.end(true, () => {
      console.log("Disconnected from MQTT broker");
      mqtt_connected = false;
      feedbackDisconnected();
      // Clear any ongoing reconnection attempts
      clearInterval(reconnectInterval);
      reconnectAttempts = 0;
    });
  } else {
    console.log("MQTT client is not connected.");
  }
}

function initializeNetworkSettings() {
  // Set game_id to current date in YYYY-MM-DD format
  const currentDate = new Date();
  networkSettings.game_id = currentDate.toISOString().split("T")[0];
}

function feedbackDisconnected() {
  document.getElementById("connected").style.display = "none";
  document.getElementById("disconnected").style.display = "inline";
  document.getElementById("con_state").innerText = "Disconnected";
  document.getElementById("joinButton").innerText = "JOIN";
  document.getElementById("warning").style.display = "block";
  document.getElementById("warning").innerText =
    "Connection lost. Attempting to reconnect...";
}

function feedbackConnected() {
  document.getElementById("connected").style.display = "inline";
  document.getElementById("disconnected").style.display = "none";
  document.getElementById("con_state").innerText = "Connected";
  document.getElementById("joinButton").innerText = "STOP";
  document.getElementById("warning").style.display = "none";
}

function networkPanel() {
  // Show our panel and hide others
  document.getElementById("network-panel").classList.toggle("open");
  document.getElementById("tool-add-panel").classList.remove("open");
  document.getElementById("tool-npc-panel").classList.remove("open");
}

function addPanel() {
  // Show our panel and hide others
  document.getElementById("network-panel").classList.remove("open");
  document.getElementById("tool-add-panel").classList.toggle("open");
  document.getElementById("tool-npc-panel").classList.remove("open");
}

function npcPanel() {
  // Show our panel and hide others
  document.getElementById("network-panel").classList.remove("open");
  document.getElementById("tool-add-panel").classList.remove("open");
  document.getElementById("tool-npc-panel").classList.toggle("open");
}

function resetEncounter() {
  initiativeRolls = {};
  current_round = 0;
  runEncounter();
}

function runEncounter() {
  // Close other open panels
  document.getElementById("tool-npc-panel").classList.remove("open");
  document.getElementById("network-panel").classList.remove("open");
  document.getElementById("tool-add-panel").classList.remove("open");

  // Check if there are any combatants (players or NPCs) in the queue
  if (active_players.length === 0 && active_npcs.length === 0) {
    return; // Exit the function if both arrays are empty
  }

  // Combine active players and NPCs into a single array of combatants
  combatants = [...active_players, ...active_npcs];

  initializeEncounter(combatants);
  drawTimeline();
  setupArrowButtons();
}

function rollInitiative() {
  // Initialize window.encounterData if it doesn't exist
  if (!window.encounterData) {
    window.encounterData = {};
  }

  // Initialize initiativeRolls if it doesn't exist
  if (!initiativeRolls) {
    initiativeRolls = {};
  }

  const initiativeScores = combatants.map((combatant) => {
    let d10Roll = initiativeRolls[combatant.label];

    // Roll a new d10 only if the existing roll is undefined or 6 or less
    if (d10Roll === undefined || d10Roll <= 6) {
      console.warn(`reroll ${d10Roll} for ${combatant.label}`);
      d10Roll = Math.floor(Math.random() * 10) + 1;
      initiativeRolls[combatant.label] = d10Roll;
    }

    const initiativeScore = d10Roll + combatant.agility + combatant.level;
    return {
      ...combatant,
      initiativeScore
    };
  });
  appendInitiativeRolls();
}

function resetCombatantMovement() {
  // Check if combatantMovement exists and is an object
  if (combatantMovement && typeof combatantMovement === "object") {
    for (const label in combatantMovement) {
      combatantMovement[label] = 0;
    }
    debouncedSaveJsonToCookie();
  }
}

function initializeEncounter(combatants) {
  rollInitiative();
  resetCombatantMovement();

  const initiativeScores = combatants.map((combatant) => {
    d10Roll = initiativeRolls[combatant.label];
    const initiativeScore = d10Roll + combatant.agility + combatant.level;
    return {
      ...combatant,
      initiativeScore,
      d10Roll
    };
  });
  // max TICKs for each combatant
  const maxTicks = initiativeScores.map((combatant) => {
    return Math.floor(combatant.level / 5) + 2;
  });

  // Sort the combatants based on initiative score, agility, and level
  const sortedCombatants = initiativeScores.sort((a, b) => {
    if (b.initiativeScore !== a.initiativeScore) {
      return b.initiativeScore - a.initiativeScore;
    } else if (b.agility !== a.agility) {
      return b.agility - a.agility;
    } else {
      return b.level - a.level;
    }
  });

  // Create an array to store the encounter TICKs
  const encounterTicks = [];

  // Assign TICKs to each combatant based on their initiative scores
  sortedCombatants.forEach((combatant, index) => {
    let tickCount = 0;
    let tickValue = combatant.initiativeScore;

    while (tickValue > 0 && tickCount < maxTicks[index]) {
      encounterTicks.push({
        combatant,
        tick: tickValue
      });
      tickValue -= 10;
      tickCount++;
    }
  });

  // Add the final TICK at tick=0
  encounterTicks.push({
    combatant: null,
    tick: 0
  });

  // Sort the encounterTicks array based on the tick value in descending order
  encounterTicks.sort((a, b) => b.tick - a.tick);

  window.encounterData = {
    ...window.encodedData,
    encounterTicks,
    initiativeScores,
    currentFocusIndex: 0,
    hasActedYet: {}
  };
  if (current_round === 0) current_round = 1;

  // START OF ROUND, CLEAR has_acted
  active_players.forEach((player) => {
    window.encounterData.hasActedYet[player.label] = false;
  });
}

function drawTimeline() {
  const timelineContainer = document.getElementById("timeline-container");
  timelineContainer.innerHTML = "";

  const timeline = document.createElement("div");
  timeline.id = "timeline";
  timelineContainer.appendChild(timeline);

  const focusIndicator = document.createElement("div");
  focusIndicator.classList.add("focus-indicator");
  timeline.appendChild(focusIndicator);

  const highestTick = Math.max(
    ...window.encounterData.encounterTicks.map((tick) => tick.tick)
  );

  const tickCounts = {};
  window.encounterData.encounterTicks.forEach((tick) => {
    if (!tickCounts[tick.tick]) {
      tickCounts[tick.tick] = 0;
    }
    tickCounts[tick.tick]++;
  });

  window.encounterData.encounterTicks.forEach((tick) => {
    const tickDot = document.createElement("div");
    tickDot.classList.add("tick-dot");
    tickDot.setAttribute("data-tick", tick.tick);

    if (tick.combatant) {
      tickDot.style.left = `${
        ((highestTick - tick.tick) / highestTick) * 100
      }%`;
      tickDot.style.backgroundColor = tick.combatant.color || "#990000";
      tickDot.style.setProperty(
        "--tick-position",
        `${1 - tick.tick / highestTick}`
      );
      tickDot.style.setProperty("--tick-count", tickCounts[tick.tick] - 1);
    } else {
      tickDot.style.backgroundColor = "#000";
      tickDot.style.left = "100%";
    }

    const tickNumber = document.createElement("span");
    tickNumber.textContent = tick.tick;
    tickDot.appendChild(tickNumber);

    timeline.appendChild(tickDot);
  });

  updateFocus();
}

function createMovementSlider(combatant) {
  const sliderContainer = document.createElement("div");
  sliderContainer.classList.add("movement-slider");

  const slider = document.createElement("input");
  slider.type = "range";
  slider.min = 0;
  slider.max = combatant.move;
  slider.value = combatantMovement[combatant.label] || 0;

  const valueDisplay = document.createElement("span");
  valueDisplay.textContent = `${slider.value} / ${combatant.move}`;

  slider.addEventListener("input", (event) => {
    updateMovement(combatant.label, event.target.value);
    valueDisplay.textContent = `${event.target.value} / ${combatant.move}`;
  });

  sliderContainer.appendChild(slider);
  sliderContainer.appendChild(valueDisplay);

  return sliderContainer;
}

function setupArrowButtons() {
  const leftButton = document.createElement("button");
  leftButton.classList.add("arrow-button", "left");
  leftButton.addEventListener("click", () => {
    moveFocus(-1);
  });

  const rightButton = document.createElement("button");
  rightButton.classList.add("arrow-button", "right");
  rightButton.addEventListener("click", () => {
    moveFocus(1);
  });

  const buttonContainer = document.createElement("div");
  buttonContainer.classList.add("button-container");
  buttonContainer.appendChild(leftButton);
  buttonContainer.appendChild(rightButton);

  const timelineContainer = document.getElementById("timeline-container");
  timelineContainer.appendChild(buttonContainer);

  // Add event listener for arrow key presses
  document.addEventListener("keydown", handleArrowKeys);
}

function handleArrowKeys(event) {
  const leftArrowCode = 37;
  const rightArrowCode = 39;

  if (event.keyCode === leftArrowCode) {
    moveFocus(-1);
  } else if (event.keyCode === rightArrowCode) {
    moveFocus(1);
  }
}

function moveFocus(direction) {
  const newIndex = window.encounterData.currentFocusIndex + direction;
  if (newIndex >= 0 && newIndex < window.encounterData.encounterTicks.length) {
    window.encounterData.currentFocusIndex = newIndex;
    updateFocus();
    // Check if the focus is on the final TICK (tick=0)
    if (window.encounterData.encounterTicks[newIndex].tick === 0) {
      betweenTurns();
    }
  } else if (newIndex >= window.encounterData.encounterTicks.length) {
    current_round += 1;
    runEncounter();
  }
}
function betweenTurns() {
  console.log("Between turns phase triggered!");
  // Add your logic for the between turns phase here
}

function appendInitiativeRolls() {
  console.warn(initiativeRolls);
  for (const [label, roll] of Object.entries(initiativeRolls)) {
    const initiativeRollSpan = document.querySelector(
      `.initiative-roll[data-label="${label}"]`
    );
    if (initiativeRollSpan) {
      initiativeRollSpan.textContent = roll;
    }
  }
}
function setCurrentTick(tick) {
  // Clear current ticks
  const tickDots = document.querySelectorAll(".tick-dot");
  tickDots.forEach((dot) => dot.classList.remove("current"));
  // set the right one current
  const currentTickDot = document.querySelector(
    `.tick-dot[data-tick="${tick}"]`
  );
  currentTickDot.classList.add("current");
}

function updateFocus() {
  const currentTick =
    window.encounterData.encounterTicks[window.encounterData.currentFocusIndex];
  if (currentTick.tick === 0) {
    displaySummary(false);
    highlightCombatant(false);
    document.getElementById("between-turns").style.display = "block";
    document.getElementById("timeline-title").innerText =
      current_round + " > BETWEEN TURNS < " + (current_round + 1);
    document.querySelector(".focus-indicator").style.width = "100%";
    setCurrentTick(0);
    return;
  }
  // Find the combatant data for the current tick
  const currentCombatant = combatants.find(
    (combatant) => combatant.label === currentTick.combatant.label
  );
  const isCurrentCombatantPlayer = active_players.includes(currentCombatant);

  //Update the encounter title with TURN/TICK
  document.getElementById("timeline-title").innerText =
    "TURN " +
    current_round +
    " | TICK " +
    currentTick.tick +
    " | " +
    currentCombatant.name;

  // this is the progress bar
  const focusIndicator = document.querySelector(".focus-indicator");
  const focusPosition =
    1 -
    currentTick.tick /
      Math.max(...window.encounterData.encounterTicks.map((tick) => tick.tick));
  focusIndicator.style.width = `${focusPosition * 100}%`;
  setCurrentTick(currentTick.tick);
  // First action triggers MQTT reset
  if (
    isCurrentCombatantPlayer &&
    !window.encounterData.hasActedYet[currentCombatant.label]
  ) {
    publishPlayerReset(currentCombatant.label);
    window.encounterData.hasActedYet[currentCombatant.label] = true;
  }
  // Display the selected player.
  displaySummary(currentCombatant, isCurrentCombatantPlayer);
  highlightCombatant(currentCombatant.label);
  document.getElementById("between-turns").style.display = "none";
}

function publishMessage(topic, message) {
  if (mqtt_connected) {
    const jsonPayload = JSON.stringify(message);
    client.publish(topic, jsonPayload, { qos: 0, retain: false }, (err) => {
      if (err) {
        console.error("Failed to publish message:", err);
      } else {
        console.log("Message published to topic:", topic);
      }
    });
  } else {
    console.log("MQTT client is not connected. Cannot publish message.");
  }
}

function publishPlayerReset(label) {
  console.debug(`publishPlayerReset "reset" to ${label}`);
  message = { action: "reset" };
  topic = `${networkSettings.game_id}/${label}/set`;
  publishMessage(topic, message);
}

function publishDamage(value, player) {
  console.debug(`publishDamage ${value} to ${player}`);
  message = { damage: Number(value) };
  topic = `${networkSettings.game_id}/${player}/set`;
  publishMessage(topic, message);
}

function processMqttEvent(topic, message) {
  try {
    const topicSegments = topic.split("/");
    console.warn(topicSegments);
    if (
      topicSegments.length === 3 &&
      topicSegments[0] === networkSettings.game_id &&
      topicSegments[2] === "lwt"
    ) {
      const label = topicSegments[1];
      const status = message.toString();

      if (status === "ONLINE") {
        // Add the player to player_queue if not already present
        const existingPlayer = player_queue.find(
          (player) => player.label === label
        );
        if (!existingPlayer) {
          const newPlayer = { label, online: true };
          player_queue.push(newPlayer);
          console.log("Player added to player_queue:", newPlayer);
        } else {
          existingPlayer.online = true;
          console.log("Player status updated in player_queue:", existingPlayer);
        }
      } else if (status === "OFFLINE") {
        // Update the player's online status in player_queue
        const player = player_queue.find((player) => player.label === label);
        if (player) {
          player.online = false;
          console.log("Player status updated in player_queue:", player);
        }

        // Update the player's online status in active_players
        const activePlayer = active_players.find(
          (player) => player.label === label
        );
        if (activePlayer) {
          activePlayer.online = false;
          console.log("Player status updated in active_players:", activePlayer);
        }
      }

      updatePlayerList();
      updateCombatantsList();
    } else if (
      topicSegments.length === 2 &&
      topicSegments[0] === networkSettings.game_id &&
      topicSegments[1] !== "lwt"
    ) {
      const label = topicSegments[1];
      const jsonMessage = JSON.parse(message.toString());

      // Check if an object with the same label already exists in player_queue
      const existingObject = player_queue.find((obj) => obj.label === label);

      if (existingObject) {
        // If an object with the same label exists, update its contents
        Object.assign(existingObject, jsonMessage);
      } else {
        // If an object with the same label doesn't exist, create a new object and add it to player_queue
        const newObject = { ...jsonMessage, label, online: true };
        player_queue.push(newObject);
      }

      console.log("Updated player_queue:", player_queue);
      updatePlayerList();
      regeneratePage();
    } else {
      console.log("Ignoring message on topic:", topic);
    }
  } catch (error) {
    console.error("Failed to parse MQTT message:", error);
  }
}

function disconnectFromMqttBroker() {
  if (mqtt_connected) {
    // Unsubscribe from all topics
    client.unsubscribe("#", (err) => {
      if (err) {
        console.error("Failed to unsubscribe from topics:", err);
      } else {
        console.log("Unsubscribed from all topics");
      }

      // Close the MQTT connection
      client.end(true, () => {
        console.log("Disconnected from MQTT broker");
        mqtt_connected = false;
        feedbackDisconnected();
      });
    });
  } else {
    console.log("MQTT client is not connected.");
  }
}

function updatePlayerList() {
  const playerList = document.getElementById("player_list");
  playerList.innerHTML = "";

  player_queue.forEach((player) => {
    const playerDiv = generateCombatantHTML(player, true);
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.classList.add("power-checkbox");
    checkbox.dataset.label = player.label;
    checkbox.addEventListener("change", handleCheckboxChange);

    const isActive = active_players.some((p) => p.label === player.label);
    checkbox.checked = isActive;

    playerDiv.appendChild(checkbox);
    playerList.appendChild(playerDiv);

    // Set the opacity based on the player's online status
    if (!player.online) {
      playerDiv.style.opacity = "0.5";
    } else {
      playerDiv.style.opacity = "1";
    }

    // Attach click event listener to the player div
    playerDiv.addEventListener("click", handlePlayerDivClick);
  });
}

function updateCombatantsList() {
  const combatantsDiv = document.getElementById("combatants");
  combatantsDiv.innerHTML = "";

  const sortedPlayers = [...active_players].sort((a, b) =>
    a.name.localeCompare(b.name)
  );
  const sortedNPCs = [...active_npcs].sort((a, b) =>
    a.name.localeCompare(b.name)
  );

  sortedPlayers.forEach((player) => {
    const playerDiv = generateCombatantHTML(player, true, true);
    combatantsDiv.appendChild(playerDiv);

    // Set the opacity based on the player's online status
    if (!player.online) {
      playerDiv.style.opacity = "0.5";
    } else {
      playerDiv.style.opacity = "1";
    }
  });

  sortedNPCs.forEach((npc) => {
    const npcDiv = generateCombatantHTML(npc, false, true);
    combatantsDiv.appendChild(npcDiv);
  });

  if (sortedPlayers.length === 0 && sortedNPCs.length === 0) {
    combatantsDiv.style.display = "none"; // Hide the div
  } else {
    combatantsDiv.style.display = "block"; // Show the div
  }
}

function updateEncounterList() {
  const npcList = document.getElementById("encounter_list");
  npcList.innerHTML = "";

  npc_queue.forEach((npc) => {
    const npcDiv = document.createElement("div");
    npcDiv.classList.add("player-item");

    const colorDiv = document.createElement("div");
    colorDiv.classList.add("color-icon");
    colorDiv.style.backgroundColor = npc.color ?? "#990000";

    const infoDiv = document.createElement("div");
    infoDiv.classList.add("player-item-flex");

    const nameHeading = document.createElement("h4");
    nameHeading.classList.add("player-item-title");
    nameHeading.textContent = npc.name;

    const identityFootnote = document.createElement("span");
    identityFootnote.classList.add("player-item-footnote");
    identityFootnote.textContent = npc.description;
    identityFootnote.style.fontSize = "12px";

    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.classList.add("power-checkbox");
    checkbox.dataset.label = npc.label;
    checkbox.addEventListener("change", handleCheckboxChange);

    const isActive = active_npcs.some(
      (activeNpc) => activeNpc.label === npc.label
    );
    checkbox.checked = isActive;

    infoDiv.appendChild(nameHeading);
    infoDiv.appendChild(identityFootnote);

    npcDiv.appendChild(colorDiv);
    npcDiv.appendChild(infoDiv);
    npcDiv.appendChild(checkbox);

    npcList.appendChild(npcDiv);

    // Attach click event listener to the NPC div
    npcDiv.addEventListener("click", handleNPCDivClick);
  });
}

function handleNPCDivClick(event) {
  const checkbox = event.currentTarget.querySelector('input[type="checkbox"]');
  checkbox.checked = !checkbox.checked;
  handleCheckboxChange({ target: checkbox });
}

function generateCombatantHTML(combatant, isPlayer, needs_drawer = false) {
  const combatantDiv = document.createElement("div");
  combatantDiv.classList.add("player-item");
  combatantDiv.setAttribute("data-label", combatant.label);

  // Attach click event listener to the combatant div
  combatantDiv.addEventListener("click", () => {
    handleCombatantClick(combatant, isPlayer);
  });

  const colorDiv = document.createElement("div");
  colorDiv.classList.add("color-icon");
  colorDiv.style.backgroundColor = combatant.color || "#990000";
  // Add click event listener to the color icon div
  colorDiv.addEventListener("click", () => {
    if (isPlayer) {
      const player = active_players.find((p) => p.label === combatant.label);
      if (player) {
        current_shown = player;
        current_is_player = true;
        displaySummary(current_shown, current_is_player);
      }
    } else {
      const npc = active_npcs.find((n) => n.label === combatant.label);
      if (npc) {
        current_shown = npc;
        current_is_player = false;
        displaySummary(current_shown, current_is_player);
      }
    }
    return combatantDiv;
  });

  // Add an empty span for the initiative roll
  const initiativeRollSpan = document.createElement("span");
  initiativeRollSpan.classList.add("initiative-roll");
  initiativeRollSpan.setAttribute("data-label", combatant.label);
  if (initiativeRolls[combatant.label]) {
    initiativeRollSpan.textContent = initiativeRolls[combatant.label];
  }
  colorDiv.appendChild(initiativeRollSpan);

  const infoDiv = document.createElement("div");
  infoDiv.classList.add("player-item-flex");

  const nameHeading = document.createElement("h4");
  nameHeading.classList.add("player-item-title");
  nameHeading.textContent = combatant.name;

  const identityFootnote = document.createElement("span");
  identityFootnote.classList.add("player-item-footnote");
  identityFootnote.textContent = combatant.identity || combatant.description;
  identityFootnote.style.fontSize = "12px";

  infoDiv.appendChild(nameHeading);
  infoDiv.appendChild(identityFootnote);

  const hpDiv = document.createElement("div");
  hpDiv.classList.add("player-item-flex");

  const hpBox = document.createElement("div");
  hpBox.classList.add("player-item-box");
  const hpLabel = document.createElement("label");
  const hpValue = document.createElement("div");
  hpValue.classList.add("player-item-num");
  hpLabel.innerText = "HP";
  hpValue.innerText = combatant.current_hp;
  const powerBox = document.createElement("div");
  powerBox.classList.add("player-item-box");
  const powerLabel = document.createElement("label");
  const powerValue = document.createElement("div");
  powerValue.classList.add("player-item-num");
  powerLabel.innerText = "POW";
  powerValue.innerText = combatant.current_power;
  hpBox.appendChild(hpValue);
  hpBox.appendChild(hpLabel);
  powerBox.appendChild(powerValue);
  powerBox.appendChild(powerLabel);

  hpDiv.appendChild(hpBox);
  hpDiv.appendChild(powerBox);

  combatantDiv.appendChild(colorDiv);
  combatantDiv.appendChild(infoDiv);
  combatantDiv.appendChild(hpDiv);
  if (needs_drawer) {
    const drawerDiv = document.createElement("div");
    drawerDiv.classList.add("drawer");

    const numberInput = document.createElement("input");
    numberInput.type = "number";
    numberInput.classList.add("number-input");

    if (isPlayer) {
      const button = document.createElement("button");
      button.classList.add("tiny-button");

      // Create a container element for the SVG
      const svgContainer = document.createElement("span");
      svgContainer.classList.add("twemoji");
      svgContainer.innerHTML = punch_icon;
      button.appendChild(svgContainer);

      button.addEventListener("click", () => {
        drawerPlayer(numberInput.value, combatant.label);
      });
      drawerDiv.appendChild(numberInput);
      drawerDiv.appendChild(button);
    } else {
      const buttonA = document.createElement("button");
      buttonA.classList.add("tiny-button");
      // Create a container element for the SVG
      const svgContainer = document.createElement("span");
      svgContainer.classList.add("twemoji", "dark-button");
      svgContainer.innerHTML = roll_with_icon;
      buttonA.appendChild(svgContainer);
      buttonA.addEventListener("click", () => {
        drawerNpcA(numberInput.value, combatant.label);
      });
      const buttonB = document.createElement("button");
      buttonB.classList.add("tiny-button");
      // Create a container element for the SVG
      const svgContainer2 = document.createElement("span");
      svgContainer2.classList.add("twemoji");
      svgContainer2.innerHTML = punch_icon;
      buttonB.appendChild(svgContainer2);
      buttonB.addEventListener("click", () => {
        drawerNpcB(numberInput.value, combatant.label);
      });
      drawerDiv.appendChild(numberInput);
      drawerDiv.appendChild(buttonA);
      drawerDiv.appendChild(buttonB);
    }

    combatantDiv.appendChild(drawerDiv);

    combatantDiv.addEventListener("mouseenter", () => {
      drawerDiv.style.display = "flex";
    });

    combatantDiv.addEventListener("mouseleave", () => {
      drawerDiv.style.display = "none";
    });
  }

  return combatantDiv;
}

function handleCombatantClick(combatant, isPlayer) {
  if (isPlayer) {
    current_shown = active_players.find((p) => p.label === combatant.label);
  } else {
    current_shown = active_npcs.find((n) => n.label === combatant.label);
  }

  // Highlight the selected combatant in the combatants_list
  highlightCombatant(combatant.label);
  displaySummary(combatant, isPlayer);
}

function highlightCombatant(label) {
  const combatantDivs = document.querySelectorAll("#combatants .player-item");
  combatantDivs.forEach((div) => {
    if (div.getAttribute("data-label") === label) {
      div.classList.add("selected");
    } else {
      div.classList.remove("selected");
    }
  });
}

function appendFeedback(message, type) {
  console.warn(message);

  // Create a new speech bubble element
  const feedbackBubble = document.createElement("div");
  feedbackBubble.className = "process-bubble";

  // Set the class based on the message type
  feedbackBubble.classList.add(type);

  // Set the text content of the speech bubble
  feedbackBubble.textContent = message;

  // Append the speech bubble to the feedback window
  const feedbackWindow = document.getElementById("combat_info");
  feedbackWindow.appendChild(feedbackBubble);

  // Scroll to the bottom of the feedback window
  feedbackWindow.scrollTop = feedbackWindow.scrollHeight;
}

function handleClickableSpanClick(event) {
  event.stopPropagation();

  const clickedSpan = event.target;
  const textContent = clickedSpan.textContent;
  const powerName = clickedSpan.dataset.power;

  // Extract the first separate integer from the textContent
  const regex = /[^\d]*(\d+)/;
  const match = textContent.match(regex);

  if (match) {
    const value = Number(match[1]);
    appendFeedback(
      `${current_shown.label} using ${powerName} for ${value} PR`,
      "note"
    );
    appendFeedback(`-${value} PR`, "pr");
    current_shown.current_power -= value;
    // console.log('Updated power_spent:', initialData.spent_power)
  } else {
    appendFeedback(`${powerName} has no usable PR cost`, "alert");
    console.log("Invalid value in clickable span:", textContent);
  }
  regeneratePage();
}

function buildAttackRow(
  powerName,

  damage,
  range,
  attackMod,
  powerCost,
  type,
  label
) {
  const tableBody = document.querySelector("#attack_array tbody");
  const row = document.createElement("tr");

  const powerNameCell = document.createElement("td");
  powerNameCell.textContent = powerName;
  const attackModCell = document.createElement("td");
  attackModCell.textContent = attackMod;

  const typeCell = document.createElement("td");
  typeCell.textContent = type;

  const damageCell = document.createElement("td");
  damageCell.textContent = damage;

  const rangeCell = document.createElement("td");
  rangeCell.textContent = range;

  const powerCostCell = document.createElement("td");
  const clickableSpan = document.createElement("span");
  clickableSpan.classList.add("clickable");
  clickableSpan.textContent = formatPower(powerCost);
  clickableSpan.setAttribute("data-power", powerName);
  clickableSpan.addEventListener("click", handleClickableSpanClick);
  powerCostCell.appendChild(clickableSpan);

  row.appendChild(powerNameCell);
  row.appendChild(typeCell);
  row.appendChild(attackModCell);
  row.appendChild(damageCell);
  row.appendChild(rangeCell);
  row.appendChild(powerCostCell);

  tableBody.appendChild(row);
}

function drawDefenseMatrixByType(powerArray, prefix, containerElement) {
  if (Array.isArray(powerArray)) {
    powerArray.forEach((p) => {
      const powerName = p.split(" ")[0].toLowerCase();
      const elementId = `${prefix}_${powerName}`;
      const element = containerElement.querySelector(`#${elementId}`);

      if (element) {
        let svgSource;
        let fillColor;

        // const isSlateMode = document.body.classList.contains('slate')
        if (prefix === "imm") {
          svgSource = shield;
        } else if (prefix === "vuln") {
          svgSource = green_dice;
        } else {
          svgSource = red_dice;
        }
        element.innerHTML = `<span class="twemoji ${prefix}">${svgSource}</span>`;

        // const svgBlob = new Blob([modifiedSvgSource], { type: 'image/svg+xml' })
        // const svgUrl = URL.createObjectURL(svgBlob)

        // const img = document.createElement('img')
        // img.classList.add(prefix)
        // img.src = svgUrl

        // element.replaceChildren(img)
      }
    });
  }
}

function resetDefenseMatrix() {
  const matrixElements = document.querySelectorAll(
    "#imm_physical, #resist_physical, #vuln_physical, " +
      "#imm_mental, #resist_mental, #vuln_mental, " +
      "#imm_spiritual, #resist_spiritual, #vuln_spiritual, " +
      "#imm_photonic, #resist_photonic, #vuln_photonic, " +
      "#imm_electric, #resist_electric, #vuln_electric, " +
      "#imm_magnetic, #resist_magnetic, #vuln_magnetic, " +
      "#imm_thermal, #resist_thermal, #vuln_thermal, " +
      "#imm_vibration, #resist_vibration, #vuln_vibration, " +
      "#imm_disruption, #resist_disruption, #vuln_disruption"
  );

  matrixElements.forEach((element) => {
    element.innerHTML = `<span class="twemoji imm">${smalldot}</span>`;
  });
}

function resetAttackArray() {
  const tableBody = document.querySelector("#attack_array tbody");
  tableBody.innerHTML = ""; // Clear the contents of the table body
}

function displaySummary(data, is_player = true) {
  if (data === undefined) return;
  if (data.label) document.getElementById("summary").style.display = "block";
  else document.getElementById("summary").style.display = "none";

  // Create a modal or dialog to display the summary information
  // DR box
  if (data.dr === 0) document.getElementById("DR_box").style.display = "none";
  else document.getElementById("DR_box").style.display = "flex";
  // AR box
  if (data.armor_rating === 0)
    document.getElementById("AR_box").style.display = "none";
  else document.getElementById("AR_box").style.display = "flex";
  // SR box
  if (data.sr === 0) document.getElementById("SR_box").style.display = "none";
  else document.getElementById("SR_box").style.display = "flex";

  document.getElementById("summary_name").textContent = data.name;
  document.getElementById("HP_out").textContent = data.current_hp;
  document.getElementById("POW_out").textContent = data.current_power;
  document.getElementById("MV_out").textContent = data.move;
  document.getElementById("AC_out").textContent = data.ac;
  document.getElementById("AR_out").textContent = data.armor_rating;
  document.getElementById("SR_out").textContent = data.sr;
  document.getElementById("DR_out").textContent = data.dr;
  // stats
  document.getElementById("STR_out").textContent = data.strength;
  document.getElementById("END_out").textContent = data.endurance;
  document.getElementById("AGIL_out").textContent = data.agility;
  document.getElementById("INT_out").textContent = data.intelligence;
  document.getElementById("CHA_out").textContent = data.charisma;
  // Attacks
  resetAttackArray();
  if (data.attacks) {
    data.attacks.forEach((attackObj) => {
      const { name, damage, range, th, pr, type } = attackObj;
      buildAttackRow(name, damage, range, th, pr, type, data.label);
    });
  }
  // Defenses
  resetDefenseMatrix();
  const defenseCol1 = document.getElementById("DC1");
  const defenseCol2 = document.getElementById("DC2");
  const defenseCol3 = document.getElementById("DC3");
  if (data.immune) {
    drawDefenseMatrixByType(data.immune, "imm", defenseCol1);
    drawDefenseMatrixByType(data.immune, "imm", defenseCol2);
    drawDefenseMatrixByType(data.immune, "imm", defenseCol3);
  }
  if (data.resist) {
    drawDefenseMatrixByType(data.resist, "resist", defenseCol1);
    drawDefenseMatrixByType(data.resist, "resist", defenseCol2);
    drawDefenseMatrixByType(data.resist, "resist", defenseCol3);
  }
  if (data.vulnerable) {
    drawDefenseMatrixByType(data.vulnerable, "vuln", defenseCol1);
    drawDefenseMatrixByType(data.vulnerable, "vuln", defenseCol2);
    drawDefenseMatrixByType(data.vulnerable, "vuln", defenseCol3);
  }
  // Movement
  const movementSliderContainer = document.getElementById(
    "movement-slider-container"
  );
  if (movementSliderContainer) {
    movementSliderContainer.innerHTML = "";
    if (data.label) {
      const movementSlider = createMovementSlider(data);
      movementSliderContainer.appendChild(movementSlider);
    }
  }
}
function updateMovement(label, value) {
  combatantMovement[label] = parseInt(value);
  debouncedSaveJsonToCookie();
}

function handlePlayerDivClick(event) {
  const checkbox = event.currentTarget.querySelector('input[type="checkbox"]');
  checkbox.checked = !checkbox.checked;
  handleCheckboxChange({ target: checkbox });
}

function handleCheckboxChange(event) {
  const label = event.target.dataset.label;
  const isChecked = event.target.checked;

  if (isChecked) {
    // Add the player to active_players if the checkbox is checked and not already present
    const player = player_queue.find((player) => player.label === label);
    if (player && !active_players.some((p) => p.label === label)) {
      active_players.push(player);
    }

    // Add the NPC to active_npcs if the checkbox is checked and not already present
    const npc = npc_queue.find((npc) => npc.label === label);
    if (npc && !active_npcs.some((n) => n.label === label)) {
      if (!npc.current_hp) npc.current_hp = npc.max_hp;
      if (!npc.current_power) npc.current_power = npc.max_power;
      active_npcs.push(npc);
    }
  } else {
    // Remove the player from active_players if the checkbox is unchecked
    const playerIndex = active_players.findIndex(
      (player) => player.label === label
    );
    if (playerIndex !== -1) {
      active_players.splice(playerIndex, 1);
    }

    // Remove the NPC from active_npcs if the checkbox is unchecked
    const npcIndex = active_npcs.findIndex((npc) => npc.label === label);
    if (npcIndex !== -1) {
      active_npcs.splice(npcIndex, 1);
    }
  }

  console.log("Updated active_players:", active_players);
  console.log("Updated active_npcs:", active_npcs);

  updateCombatantsList();
}

function get_level_mod(level) {
  return Math.floor(level / 2);
}

function loadEncounter() {
  loadEncounterFile(regeneratePage);
}

function loadEncounterFile(callback) {
  const fileInput = document.createElement("input");
  fileInput.type = "file";
  fileInput.accept = ".json";

  fileInput.addEventListener("change", function (event) {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = function (event) {
      const jsonData = event.target.result;

      try {
        const parsedData = JSON.parse(jsonData);

        if (Array.isArray(parsedData)) {
          parsedData.forEach((npc) => {
            if (!npc.label) {
              const nameWithoutWhitespace = npc.name.replace(/\s/g, "");
              const randomNumber = Math.floor(Math.random() * 900) + 100;
              npc.label = `${nameWithoutWhitespace}${randomNumber}`;
            }
          });
          npc_queue = npc_queue.concat(parsedData);

          // Pre-select the loaded NPCs
          parsedData.forEach((npc) => {
            if (!active_npcs.some((n) => n.label === npc.label)) {
              if (!npc.current_hp) npc.current_hp = npc.max_hp;
              if (!npc.current_power) npc.current_power = npc.max_power;
              active_npcs.push(npc);
            }
          });
        } else if (typeof parsedData === "object") {
          if (!parsedData.label) {
            const nameWithoutWhitespace = parsedData.name.replace(/\s/g, "");
            const randomNumber = Math.floor(Math.random() * 900) + 100;
            parsedData.label = `${nameWithoutWhitespace}${randomNumber}`;
          }
          npc_queue.push(parsedData);

          // Pre-select the loaded NPC
          if (!active_npcs.some((n) => n.label === parsedData.label)) {
            if (!parsedData.current_hp)
              parsedData.current_hp = parsedData.max_hp;
            if (!parsedData.current_power)
              parsedData.current_power = parsedData.max_power;
            active_npcs.push(parsedData);
          }
        } else {
          console.error("Invalid encounter data format.");
          callback(false);
          return;
        }

        console.log("Encounter loaded. Updated npc_queue:", npc_queue);
        console.log("Updated active_npcs:", active_npcs);
        callback(true);
      } catch (error) {
        console.error("Error parsing JSON data:", error);
        callback(false);
      }
    };

    reader.onerror = function (event) {
      console.error("Error loading file:", event.target.error);
      callback(false);
    };

    reader.readAsText(file);
  });

  fileInput.click();
}

function saveEncounter() {
  if (active_npcs.length === 0) {
    console.error("No active NPCs to save.");
    return;
  }

  const gameId = networkSettings.game_id.replace(/[^a-zA-Z0-9]/g, "");
  const fileName = `${gameId}.json`;
  const jsonData = JSON.stringify(active_npcs, null, 2);

  const blob = new Blob([jsonData], { type: "application/json" });
  const url = URL.createObjectURL(blob);

  const link = document.createElement("a");
  link.href = url;
  link.download = fileName;
  link.click();

  URL.revokeObjectURL(url);

  console.log("Encounter saved as", fileName);
}

function drawerPlayer(value, id) {
  const player = active_players.find((p) => p.label === id);
  if (player) {
    // Perform the desired action with the value and player object
    console.log(`Player Action: Value = ${value}, Player = ${player.label}`);
    publishDamage(value, player.label);
  }
}

function drawerNpcA(DAM, id) {
  if (DAM > 0) {
    const npc = active_npcs.find((n) => n.label === id);
    if (npc) {
      // Perform the desired action with the DAM and NPC object
      console.log(`NPC Action A: DAM = ${DAM}, NPC = ${npc.name}`);
      // if (!npc.current_hp) npc.current_hp = npc.max_hp;
      // npc.current_hp -= DAM;

      // Damage and overflow logic all here
      if (!npc.current_power) npc.current_power = npc.max_power;
      const ROLLS_WITH = Math.floor(npc.current_power / 10);
      if (DAM <= ROLLS_WITH) {
        appendFeedback(`-${DAM} POW`, "pr");
        npc.current_power -= DAM;
      } else {
        npc.current_power -= ROLLS_WITH;
        appendFeedback(`-${ROLLS_WITH} POW`, "pr");
        drawerNpcB(DAM - ROLLS_WITH, id);
      }
    }
  }
  regeneratePage();
}

function knockoutCheck(damage) {
  const d100 = Math.floor(Math.random() * 100) + 1;
  if (d100 <= damage)
    appendFeedback(`NPCS: KO ${d100}% under ${damage}`, "error");
  else appendFeedback(`NPCS: OK ${d100}% over ${damage}`, "relax");
}
function drawerNpcB(DAM, id) {
  if (DAM > 0) {
    const npc = active_npcs.find((n) => n.label === id);
    if (npc) {
      const d100 = Math.floor(Math.random() * 100) + 1;
      const AR = npc.armor_rating || 0;
      const SR = npc.sr || 0;
      const DR = npc.dr || 0;
      if (d100 <= AR && DAM >= 0) {
        appendFeedback(`Armor wins ${d100}% under AR ${AR}`, "note");
        // hurtArmor(DAM);
        if (DAM > AR) {
          appendFeedback("Armor is depleted!", "alert");
          appendFeedback(`-${AR} ARMOR`, "ar");
          npc.armor_rating = 0;
          // srToHitPoints(DAM - AR);
          const DAM2 = DAM - AR;
          if (DAM2 >= SR) {
            if (DAM2 <= DR && DAM >= 0) {
              appendFeedback(`All damage ${DAM2} stopped by DR ${DR}`, "note");
              appendFeedback(`${DAM2}<DR`, "sr");
            } else {
              appendFeedback(`-${DAM2} HP`, "hp");
              npc.current_hp -= DAM2;
              knockoutCheck(DAM2);
            }
          } else {
            appendFeedback(`All damage ${DAM2} stopped by SR ${SR}`, "note");
            appendFeedback(`${DAM2}<SR`, "sr");
          }
        } else {
          npc.armor_rating -= DAM;
          appendFeedback(`-${DAM} ARMOR`, "ar");
        }
      } else {
        if (AR > 0)
          appendFeedback(`Armor loses ${d100}% over AR ${AR}`, "note");
        // srToHitPoints(DAM);
        if (DAM < SR && DAM > 0) {
          appendFeedback(`All damage ${DAM} stopped by SR ${SR}`, "note");
          appendFeedback(`${DAM}<SR`, "sr");
        } else {
          if (DAM <= DR && DAM > 0) {
            appendFeedback(`All damage ${DAM} stopped by DR ${DR}`, "note");
            appendFeedback(`${DAM}<DR`, "sr");
          } else {
            appendFeedback(`-${DAM} HP`, "hp");
            npc.current_hp -= DAM;
            knockoutCheck(DAM);
          }
        }
      }
    }
  }
  regeneratePage();
}
// Dice rolling functions
function roll_pool(crit_range, adv, disadv, verbose = false) {
  const prime = Math.floor(Math.random() * 20) + 1;
  if (verbose) appendFeedback(`Prime die ${prime}`, "note");

  const disadvantageDice = Array.from(
    { length: disadv },
    () => Math.floor(Math.random() * 20) + 1
  ).sort((a, b) => b - a);
  if (verbose && disadv > 0)
    appendFeedback(`Disadvatage dice ${disadvantageDice}`, "note");
  const advantageDice = Array.from(
    { length: adv },
    () => Math.floor(Math.random() * 20) + 1
  ).sort((a, b) => b - a);
  if (verbose && adv > 0)
    appendFeedback(`Advantage dice ${advantageDice}`, "note");

  const critHits =
    advantageDice.filter((d) => d >= 21 - crit_range).length +
    (prime >= 21 - crit_range ? 1 : 0);
  const critFails =
    disadvantageDice.filter((d) => d <= crit_range).length +
    (prime <= crit_range ? 1 : 0);

  if (gameSettings.crit_fishing) {
    if (critHits > 0 && critHits === critFails) {
      // Dramatic Critical
      return -1;
    } else if (critHits > critFails) {
      // Crit success
      return 20;
    } else if (critFails > critHits) {
      // Crit fail
      return 1;
    }
  }

  while (advantageDice.length > 0 && disadvantageDice.length > 0) {
    advantageDice.pop();
    disadvantageDice.shift();
  }

  if (advantageDice.length > 0) {
    return Math.max(prime, ...advantageDice);
  } else if (disadvantageDice.length > 0) {
    return Math.min(prime, ...disadvantageDice);
  }

  return prime;
}

function roll_ensemble() {
  const crit_range = gameSettings.crit_range | 1;
  const disadv = cookieData.num_disadvantage | 0;
  const adv = cookieData.num_advantage | 0;
  const negate = gameSettings.dis_adv_negate;

  document.getElementById("dice_audio").play();

  if (negate && adv > 0 && disadv > 0) {
    if (adv >= disadv) {
      cookieData.num_advantage -= cookieData.num_disadvantage;
      cookieData.num_disadvantage = 0;
    } else {
      cookieData.num_disadvantage -= cookieData.num_advantage;
      cookieData.num_advantage = 0;
    }
    drawAdvantagePool();
    drawDisadvantagePool();
  }
  // appendFeedback(`Rolling 10000 checks`, "note");
  // appendFeedback(`Crit Range is ${crit_range}`, "note");
  // appendFeedback(`Rolling ${adv} ADVANTAGE`, "note");
  // appendFeedback(`Rolling ${disadv} DISADVANTAGE`, "note");

  const result = roll_pool(crit_range, adv, disadv, true);
  console.warn(`Roll result: ${result}`);
  appendFeedback(`${result}`, "roll");

  // const histogram = Array(21).fill(0);
  // for (let i = 0; i < 10000; i++) {
  //   const rollResult = roll_pool(crit_range, adv, disadv);
  //   histogram[rollResult === -1 ? 0 : rollResult]++;
  // }

  // const histogramDiv = document.getElementById("histogram");
  // histogramDiv.innerHTML = "";
  // for (let i = 0; i <= 20; i++) {
  //   const bar = document.createElement("div");
  //   bar.className = i === result ? "redbar" : "bar";
  //   bar.id = `r${i}`;
  //   bar.style.height = `${(histogram[i] / 10000) * 1000}%`;
  //   histogramDiv.appendChild(bar);
  // }

  return result;
}

function incrementAdvantage() {
  const pool = cookieData.num_advantage;
  if (pool === undefined || isNaN(pool)) {
    cookieData.num_advantage = 0;
  }
  if (cookieData.num_advantage < gameSettings.max_dice_pool)
    cookieData.num_advantage += 1;
  console.warn(`incrementAdvantage ${cookieData.num_advantage}`);
  drawAdvantagePool();
}
function incrementDisadvantage() {
  const pool = cookieData.num_disadvantage;
  if (pool === undefined || isNaN(pool)) {
    cookieData.num_disadvantage = 0;
  }
  if (cookieData.num_disadvantage < gameSettings.max_dice_pool)
    cookieData.num_disadvantage += 1;
  console.warn(`incrementDisadvantage ${cookieData.num_disadvantage}`);
  drawDisadvantagePool();
}
function decrementAdvantage() {
  const pool = cookieData.num_advantage;
  if (pool === undefined || isNaN(pool)) {
    cookieData.num_advantage = 0;
  }
  if (cookieData.num_advantage > 0) cookieData.num_advantage -= 1;
  drawAdvantagePool();
  console.warn(`decrementAdvantage ${cookieData.num_advantage}`);
}
function decrementDisadvantage() {
  const pool = cookieData.num_disadvantage;
  if (pool === undefined || isNaN(pool)) {
    cookieData.num_disadvantage = 0;
  }
  if (cookieData.num_disadvantage > 0) cookieData.num_disadvantage -= 1;
  drawDisadvantagePool();
  console.warn(`decrementDisadvantage ${cookieData.num_disadvantage}`);
}

function drawAdvantagePool() {
  const Pool = document.getElementById("pool_adv");
  Pool.innerHTML = "";
  for (let i = 0; i < cookieData.num_advantage; i++) {
    const advSVGElement = document.createElement("span");
    advSVGElement.innerHTML = green_dice;
    Pool.appendChild(advSVGElement);
  }
}

function drawDisadvantagePool() {
  const Pool = document.getElementById("pool_disadv");
  Pool.innerHTML = "";
  for (let i = 0; i < cookieData.num_disadvantage; i++) {
    const advSVGElement = document.createElement("span");
    advSVGElement.innerHTML = red_dice;
    Pool.appendChild(advSVGElement);
  }
}

document.addEventListener("DOMContentLoaded", function () {
  // Get all the elements with the "clickable" class

  document.getElementById("connectBtn").addEventListener("click", networkPanel);
  document.getElementById("addBtn").addEventListener("click", addPanel);
  document.getElementById("npcBtn").addEventListener("click", npcPanel);
  document.getElementById("playBtn").addEventListener("click", resetEncounter);
  document
    .getElementById("loadEncounter")
    .addEventListener("click", loadEncounter);
  document
    .getElementById("saveEncounter")
    .addEventListener("click", saveEncounter);
  document
    .getElementById("joinButton")
    .addEventListener("click", connectToMqttBroker);

  // SWIPES
  const el = document.querySelector(".powers_column");
  const hammelpus = new Hammer(el, { preventDuplicates: true });
  hammelpus.get("swipe").set({
    direction: (data) => {
      if (data.direction === Hammer.DIRECTION_LEFT) {
        return KeyCodes.LEFTARROW;
      } else if (data.direction === Hammer.DIRECTION_RIGHT) {
        return KeyCodes.RIGHTARROW;
      }
    }
  });
  // Add the event listener
  el.addEventListener("hammer:swipe", (event) => {
    const direction = event.detail.direction;
    if (direction === Hammer.DIRECTION_LEFT) {
      moveFocus(-1);
    } else if (direction === Hammer.DIRECTION_RIGHT) {
      moveFocus(1);
    }
  });
});
