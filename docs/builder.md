---
template: builder.html
hide:
  - navigation
  - toc
---
<!-- markdownlint-disable MD033 -->
<!-- These are the buttons arranged around the page -->
<div class="side_button" markdown>
  <button class="side_button_rnd" id ="hideCharacterBuilderBtn" markdown>:material-cog-outline:</button>
</div>
<div class="side_button no2" markdown>
  <button class="side_button_rnd" id ="toggleCalcPanelBtn" markdown>:material-calculator-variant:</button>
</div>
<div id="swap_parent" class="side_button on1" markdown >
<button class="side_button_rnd" id="swapButton" onclick="handleSwapButtonClick()">:material-page-next-outline:</button>
</div>
<div class="side_button on-1" markdown>
<span class="pool_adv" id="pool_adv"></span>
<span class="tray_adv" markdown>
<button class="tray_button advantage" id="adv_minus" onclick="decrementAdvantage()" markdown>
:material-minus-circle:
</button>
<button class="tray_button advantage" id="adv_plus" onclick="incrementAdvantage()" markdown>
:material-plus-circle:
</button>
</span>
<button class="side_button_rnd" id ="rollButton" onclick="roll_ensemble()" markdown>
:material-dice-d20:
</button>
<span class="tray_disadv" markdown>
<button class="tray_button disadvantage" id="dis_minus" onclick="decrementDisadvantage()" markdown>
:material-minus-circle:
</button>
<button class="tray_button disadvantage" id="dis_plus" onclick="incrementDisadvantage()" markdown>
:material-plus-circle:
</button>
</span>
<div class="pool_disadv" id="pool_disadv"></div>
</div>
<!-- Floating image for portrait --><div id="image-container"></div>

<div id="calculatorPanel" class="settings-tone modal-content">
  <div class="close-button" onclick="toggleCalcPanel()">&times;</div>
  <div id="damage_column"></div>
  <div id="damage-cpanel" class="sub-panel">
  <div class="left-column"> <div class="box_1by2 setter">
      <label for="damage" class="ability_label">DAMAGE:</label>
      <input type="number" id="damage" class="number-input" value="0">
    </div></div>
  <div class="right-column"><div class="button-column">
      <span class="left"><button class="cpanel-button" id="rollDamageBtn">
      <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#fff"
     d="m 16.532695,7.6818285 c 0.943968,0.545 2.192051,0.2032567 2.732051,-0.7320508 0.56,-0.9699484 0.211917,-2.1870508 -0.732051,-2.7320508 -0.961288,-0.555 -2.172051,-0.2378976 -2.732051,0.7320508 -0.54,0.9353075 -0.229237,2.1770508 0.732051,2.7320508 M 6.4663432,17.897261 9.5223686,14.60407 l 0.8273134,2.787051 -2.9999998,5.196152 1.7320508,1 3.75,-6.49519 -0.827314,-2.787051 2.028276,-2.293076 c 0.367173,1.944038 1.599223,3.810063 3.51314,4.915063 l 1,-1.732051 c -1.654109,-0.955 -2.531089,-2.616025 -2.52257,-4.250781 L 15.94724,9.0758666 C 15.91083,8.3389309 15.581214,7.7098412 14.983657,7.3648412 14.715189,7.2098412 14.510644,7.1841233 14.242176,7.0291233 l -5.5946716,-0.6897442 -2.36,4.0876399 1.7320508,1 1.71,-2.9618068 L 11.629741,8.7539944 6.1841,14.986121 2.4405755,11.670095 1.0941654,13.202146 Z"
     id="path427" /><path fill="#fff"  d="m 17.000895,21.683214 v -1.5 c 2.21,0 4,-1.79 4,-4 0,-0.82 -0.25,-1.58 -0.67,-2.21 l -1.09,1.09 c 0.17,0.34 0.26,0.72 0.26,1.12 0,1.38 -1.12,2.5 -2.5,2.5 v -1.5 l -2.25,2.25 2.25,2.25 M 5.0345203,9.6845095 l 1.09,-1.09 c -0.17,-0.34 -0.26,-0.72 -0.26,-1.12 0,-1.38 1.12,-2.5000005 2.5,-2.5000005 V 6.4745095 L 10.61452,4.224509 c -1.4467317,-1.047876 -2.2499997,-3.47626201 -2.2499997,-0.75 -2.21,0 -4,1.79 -4,4.0000005 0,0.82 0.25,1.58 0.67,2.21 z"
     id="path702" /></svg>
      </button><label class="cpanel-button-label">ROLL WITH</span>
      <span class="left"><button class="cpanel-button dark" id="testDamageBtn">
      <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 670 670"><path
       fill="#fff"
       d="m 328.17306,646.33051 c -2.88957,-6.14167 -7.93211,-16.71667 -11.20563,-23.5 -3.27352,-6.78333 -8.43344,-17.58333 -11.46648,-24 -3.03304,-6.41667 -7.53244,-15.86667 -9.99868,-21 -2.46624,-5.13333 -7.32136,-15.33333 -10.78916,-22.66667 -3.4678,-7.33333 -6.56372,-13.59828 -6.87982,-13.92211 -0.32973,-0.33781 -2.17384,0.31371 -4.32628,1.52847 -5.38732,3.04042 -30.42471,17.04547 -46.00419,25.73316 -26.85182,14.97356 -43.54039,24.28862 -52.32273,29.20498 -4.94416,2.76776 -9.07085,4.95082 -9.17041,4.85125 -0.0996,-0.0996 0.42964,-8.40434 1.17601,-18.45505 0.74637,-10.05072 2.24383,-31.32403 3.32768,-47.27403 1.08385,-15.95 2.31405,-33.58583 2.73377,-39.19072 0.41972,-5.60489 0.60488,-10.34897 0.41146,-10.54239 -0.39909,-0.39908 -5.9901,1.23074 -26.48912,7.7218 -7.88333,2.49627 -23.18333,7.2987 -33.99999,10.67207 -10.81667,3.37337 -25.216668,7.877 -31.999998,10.00807 -6.78334,2.13106 -18.78334,5.87382 -26.66667,8.31725 -7.88333,2.44343 -15.49578,4.88369 -16.91655,5.42282 -1.42076,0.53913 -2.72162,0.84182 -2.89078,0.67266 -0.16916,-0.16917 9.41789,-14.80672 21.30457,-32.5279 35.59337,-53.06415 52.525758,-78.50142 52.842868,-79.385 0.18061,-0.50327 -2.41127,-2.4132 -6.52016,-4.80463 -3.750978,-2.18312 -10.419948,-6.10958 -14.819948,-8.72546 -4.4,-2.61588 -16.25,-9.64796 -26.33333,-15.62685 -10.08334,-5.97889 -20.43334,-12.12562 -23,-13.65941 -2.56667,-1.53378 -8.36397,-4.98249 -12.8829,-7.6638 -6.66518,-3.95479 -8.04955,-5.03115 -7.33333,-5.70174 0.48559,-0.45464 10.3329,-9.16191 21.88289,-19.34948 39.63028,-34.9555 46.66667,-41.22828 46.66667,-41.60227 0,-0.38296 -10.94701,-12.30819 -35.22104,-38.36835 -7.51379,-8.06667 -15.48788,-16.62805 -17.7202,-19.02529 C 16.838792,225.51362 8.1371524,215.9974 7.9472724,215.45974 c -0.12852,-0.36391 2.2608596,-0.6283 5.6666596,-0.62704 3.23889,0.001 24.03889,-0.29935 46.22222,-0.66791 22.18334,-0.36856 45.642418,-0.75363 52.131298,-0.85571 6.48888,-0.10208 11.98462,-0.37226 12.21274,-0.60039 0.36163,-0.36163 -6.98817,-39.46548 -15.05389,-80.09269 -2.87206,-14.46662 -3.02256,-15.79249 -1.79015,-15.77087 0.45834,0.008 25.42141,4.94497 55.47349,10.97095 30.05209,6.02598 54.71321,10.88329 54.80249,10.794 0.22113,-0.22112 3.36676,-15.00564 8.96016,-42.112908 2.57241,-12.46666 5.85862,-28.36666 7.30268,-35.33333 1.44406,-6.96667 4.22466,-20.39025 6.17912,-29.83019 1.95445,-9.43994 3.73958,-17.34955 3.96694,-17.57692 0.22738,-0.22737 1.10379,0.38068 1.9476,1.35122 17.52983,20.16283 89.73317,100.967988 90.25276,101.004868 0.39522,0.0281 9.44145,-8.87398 20.10273,-19.782308 10.66129,-10.90834 28.18792,-28.79707 38.94809,-39.75274 l 19.56393,-19.9194 16.92354,50.9194 c 9.30795,28.005668 17.01757,51.029028 17.1325,51.163018 0.11493,0.13399 5.09933,-2.01207 11.07646,-4.76902 14.94218,-6.89208 32.50657,-14.95608 51.8675,-23.81294 18.63751,-8.52593 32.80658,-15.038638 53.33333,-24.514298 8.79285,-4.059 14.40817,-6.31224 14.52695,-5.82919 0.1065,0.43309 -4.14256,14.26367 -9.44234,30.734638 -31.21414,97.00902 -29.46229,91.46287 -29.02476,91.8885 0.26283,0.25567 19.30515,3.72307 47.77348,8.69904 54.88725,9.59371 62.30341,10.94085 62.76344,11.40087 0.21567,0.21567 -0.0554,0.64564 -0.60233,0.95549 -0.54694,0.30985 -10.44444,7.87267 -21.99444,16.80628 -11.55,8.9336 -30.9,23.86805 -43,33.18768 -12.1,9.31962 -24.05055,18.56745 -26.55676,20.55074 l -4.55677,3.60597 48.8901,32.59281 c 26.88955,17.92604 48.73179,32.59288 48.53831,32.59298 -0.19348,9e-5 -4.39348,1.823 -9.33334,4.0509 -4.93985,2.2279 -12.88154,5.79197 -17.64821,7.92014 -4.76667,2.12818 -10.31667,4.62357 -12.33333,5.5453 -3.90271,1.78378 -14.71487,6.6713 -25.66667,11.60234 -21.13318,9.51522 -23.04353,10.47312 -22.69546,11.38016 0.19469,0.50737 11.25492,17.17773 24.57827,37.04524 48.57892,72.4399 55.27194,82.52356 54.97318,82.82232 -0.29246,0.29246 -9.24047,-0.86576 -56.52265,-7.31618 -11.91667,-1.62572 -27.51667,-3.73468 -34.66667,-4.68658 -7.15,-0.95189 -15.62112,-2.11819 -18.82471,-2.59177 -3.20358,-0.47358 -6.07574,-0.69095 -6.38257,-0.48305 -0.30683,0.20791 -1.684,6.52802 -3.06037,14.04468 -1.37638,7.51667 -3.3521,18.31667 -4.39049,24 -1.0384,5.68334 -3.28473,17.83334 -4.99185,27 -1.70712,9.16667 -4.29781,23.26524 -5.75709,31.33016 -1.45927,8.06492 -2.86466,14.87346 -3.12307,15.13011 -0.25841,0.25665 -5.18,-2.59192 -10.93685,-6.33015 -5.75686,-3.73823 -13.70686,-8.88624 -17.66667,-11.44001 -19.96309,-12.87465 -34.37587,-22.19705 -39.18794,-25.34735 -18.26136,-11.9551 -34.56612,-22.34276 -35.06983,-22.34276 -0.32259,0 -16.08795,23.25 -35.03413,51.66667 -18.94617,28.41667 -34.64322,51.66667 -34.88232,51.66667 -0.2391,0 -2.79893,-5.025 -5.68852,-11.16667 z m 87.18552,-142.18143 c 32.79515,-2.19194 58.45491,-4.25638 66.75671,-5.37089 7.08393,-0.95101 12.19653,-3.60497 17.73063,-9.20403 2.39753,-2.42568 5.32991,-6.25939 6.5164,-8.51936 4.48874,-8.54998 8.12017,-27.22426 9.50677,-48.88762 0.84433,-13.1912 0.5486,-16.97077 -1.85011,-23.64601 -1.7332,-4.8232 -7.32887,-16.37018 -9.02605,-18.62579 -0.8642,-1.14853 -0.99846,-0.78256 -1.42014,3.87084 -0.76928,8.48923 -3.37062,17.4064 -6.82298,23.38851 -3.79586,6.57735 -8.65314,13.21551 -12.03208,16.44357 -6.30015,6.01882 -17.07678,11.00622 -27.76398,12.84912 -20.87226,3.5992 -38.75011,0.71155 -49.09282,-7.92953 -1.59179,-1.32991 -3.08973,-2.17861 -3.32876,-1.88603 -0.23903,0.29259 -2.12929,2.64811 -4.20058,5.23448 -10.39185,12.97608 -23.81254,19.88718 -41.91112,21.58251 -4.17138,0.39074 -8.07675,0.71146 -8.67861,0.71271 -2.08902,0.005 -3.43116,5.03287 -3.75564,14.07101 -0.23088,6.43138 -0.0523,9.25958 0.76121,12.05811 2.08155,7.1603 9.19497,12.68624 19.08871,14.82876 6.58998,1.42709 16.58186,1.2313 49.52244,-0.97036 z m -51.85577,-56.3191 c 2.56667,-0.49765 5.91059,-1.35184 7.43093,-1.8982 7.60802,-2.73408 16.91528,-10.89158 22.02644,-19.30545 l 2.39624,-3.94464 -0.77159,-4.42559 c -2.67122,-15.32136 -3.91591,-62.16204 -2.90774,-109.42559 0.73796,-34.59593 1.05666,-38.37435 3.86531,-45.82617 1.17975,-3.13004 3.84228,-5.63856 5.36469,-5.05436 1.65121,0.63364 3.45335,5.01875 4.63239,11.27196 0.98888,5.24465 1.13722,12.90438 1.2642,65.27524 0.17704,73.0142 0.58712,79.18613 5.7779,86.9594 5.50715,8.247 14.0789,11.77291 28.25456,11.62222 20.77914,-0.22088 32.00289,-5.7782 40.89412,-20.24829 5.53674,-9.01079 6.87365,-15.11182 8.03473,-36.66667 1.55243,-28.8201 1.46502,-82.45733 -0.16079,-98.66666 -3.01511,-30.06072 -10.29897,-65.93958 -15.08465,-74.30407 -4.32009,-7.55073 -13.61971,-14.20463 -23.35008,-16.70704 -7.5883,-1.95152 -12.65897,-1.9501 -24.66666,0.007 -14.67039,2.39098 -26.38555,2.89662 -31.70733,1.36854 -5.14838,-1.47832 -10.46888,-4.47234 -18.29267,-10.29388 -12.10057,-9.0039 -21.49061,-12.73722 -32.03643,-12.73722 -6.42394,0 -9.53606,1.36583 -21.93349,9.62605 -13.99885,9.32723 -19.5851,11.85862 -28.68836,13.00008 -6.57454,0.82438 -14.42458,-0.005 -22.42642,-2.37 -3.25341,-0.9615 -8.09341,-1.90211 -10.75554,-2.09026 -4.47787,-0.31649 -5.17664,-0.1774 -9.33334,1.8577 -3.77438,1.84793 -6.35926,4.07925 -16.15975,13.94942 -12.57194,12.66134 -14.52473,14.02716 -22.45882,15.7083 -6.12574,1.29798 -11.45748,1.21313 -22.52196,-0.35838 -5.12277,-0.7276 -10.09986,-1.32291 -11.06019,-1.32291 -5.89586,0 -18.53163,10.52874 -22.40227,18.66667 -4.55459,9.57591 -9.00337,26.21968 -11.14006,41.67719 -3.66821,26.53702 -2.74422,61.11888 2.62707,98.32281 2.31961,16.0666 7.11176,40.04626 9.63648,48.22035 5.88166,19.04261 30.87403,25.66822 51.75597,13.72079 3.25484,-1.86224 3.65266,-2.31752 3.3724,-3.85955 -3.33318,-18.33988 -3.97906,-24.74459 -5.11768,-50.74826 -0.70202,-16.03231 -0.32212,-63.79345 0.60466,-76.01956 1.11656,-14.72979 4.8357,-26.2246 8.94085,-27.63367 1.62759,-0.55866 2.08479,-0.42888 3.15796,0.89644 2.67467,3.30307 2.95595,6.11961 2.60559,26.09012 -0.56982,32.47995 -0.28308,77.0594 0.58533,91 1.852,29.73022 4.61817,40.09852 12.25117,45.9205 5.73311,4.37285 17.77941,7.74617 27.66199,7.74617 11.55695,0 17.37268,-2.62865 27.28676,-12.33333 l 6.81051,-6.66667 -0.96241,-6 c -1.58193,-9.86219 -2.14186,-32.97455 -1.56818,-64.73028 0.78751,-43.59293 1.87033,-77.88212 2.6401,-83.60305 1.31014,-9.73708 4.61796,-17.33334 7.54784,-17.33334 1.49954,0 3.65995,3.15806 4.74653,6.93839 1.78684,6.21664 2.18786,13.96373 1.63167,31.52158 -0.29978,9.46369 -0.91327,34.9067 -1.3633,56.54003 -1.308,62.87631 0.26716,78.73953 8.73025,87.9209 3.50001,3.79707 7.03302,5.87757 13.12082,7.72655 9.03342,2.74361 20.40396,3.11469 31.21425,1.01869 z" /></svg>
      </button><label class="cpanel-button-label">TAKE HIT</span><span class="left"><button class="cpanel-button red" id="applyDamageBtn">
       <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250 250"><path fill="#fff" d="M 113.5035,247.26531 C 71.049518,243.17006 34.146857,217.76318 15.137665,179.54203 -1.4222615,146.24557 -1.4222615,104.84394 15.137665,71.547467 31.794636,38.055852 63.18303,13.548729 99.000519,6.0698197 119.28165,1.8349988 141.77961,2.6020278 160.12169,8.153643 c 2.31712,0.7013231 4.36897,1.391544 4.55968,1.533825 0.19071,0.1422791 -5.26228,5.874937 -12.11775,12.73924 l -12.4645,12.480549 -3.58502,-0.590729 c -10.44458,-1.72103 -27.96028,-0.006 -40.423112,3.958053 -27.481497,8.740981 -49.729037,30.958533 -58.448303,58.369527 -6.072974,19.091812 -6.072974,38.709482 0,57.801292 8.719266,27.411 30.966806,49.62854 58.448303,58.36952 28.534772,9.076 60.050362,4.1459 83.152472,-13.00783 20.07292,-14.9045 32.72344,-35.91721 36.41358,-60.48355 1.225,-8.15521 1.43356,-19.53294 0.46571,-25.40561 l -0.59079,-3.58471 12.47266,-12.45432 12.47264,-12.454326 0.86034,2.252472 c 2.01897,5.285936 4.54919,18.290434 5.37304,27.615694 2.89288,32.7453 -8.50066,67.13477 -30.2377,91.26725 -21.64059,24.02543 -48.87049,37.67789 -81.24589,40.7349 -9.32857,0.88085 -12.32792,0.87676 -21.72355,-0.0296 z m 3.33982,-53.62699 c -23.521318,-3.14387 -43.37301,-16.97678 -53.185875,-37.06058 -9.643496,-19.73715 -9.643496,-42.32882 0,-62.065978 8.072569,-16.521966 24.34581,-29.957776 42.110845,-34.768308 11.63522,-3.150661 20.54875,-3.563668 32.5672,-1.508995 l 4.6404,0.793323 1.16053,6.792122 1.16055,6.792122 -7.34664,7.318387 -7.34662,7.318392 -4.96522,0.01135 c -18.76797,0.04278 -33.730683,11.601753 -37.898481,29.277225 -4.009149,17.00268 3.956986,35.23473 18.631451,42.64171 12.01991,6.06708 25.00957,6.06708 37.02949,0 11.9611,-6.0374 19.73972,-19.57601 19.77336,-34.41535 l 0.0115,-4.99362 7.13302,-7.1027 7.13301,-7.10272 6.97551,0.92864 6.97551,0.92864 0.9755,5.04033 c 1.33639,6.90488 1.13141,19.86268 -0.43109,27.25196 -4.72383,22.33961 -19.88159,40.70643 -40.4752,49.04428 -10.77208,4.36134 -24.27313,6.26389 -34.62858,4.87978 z m 3.79197,-57.68212 c -3.12066,-1.35914 -6.29917,-5.25981 -6.90132,-8.46927 -1.06225,-5.66171 0.0593,-7.11502 24.28975,-31.475899 L 160.9239,72.987632 158.99081,61.011267 c -2.31992,-14.373093 -2.38565,-17.372478 -0.46211,-21.088465 0.80904,-1.562926 7.81028,-9.131792 15.55833,-16.819703 14.87931,-14.7638359 15.51073,-15.1731967 20.14655,-13.061168 2.45154,1.116894 3.57173,3.258587 9.5822,18.320334 2.58605,6.480391 5.01418,12.161059 5.39586,12.623705 0.38169,0.462648 7.03607,3.325415 14.78752,6.361706 7.75146,3.036288 14.67577,6.163808 15.38738,6.950043 0.71158,0.786234 1.48462,2.851994 1.71783,4.590577 l 0.42403,3.161059 -14.08524,14.2189 c -14.5803,14.718655 -17.74852,17.060027 -23.12665,17.091077 -1.60241,0.0088 -8.30758,-0.853673 -14.90039,-1.917605 l -11.98692,-1.93443 -22.63305,22.577593 c -25.71917,25.65612 -27.38697,26.82156 -34.16086,23.87131 z" /></svg>
      </button><label class="cpanel-button-label">DIRECT</span>
    </div></div>
  </div>
  <div id="heal-cpanel" class="sub-panel">
    <div class="left-column">
      <label for="heal" class="cpanel-button-label">HEAL</label>
      <button class="cpanel-button" id="healBtn">
      <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z" /></svg>
      </button>
    </div>
    <div class="right-column">
    <div>
      <button class="cpanel-button" id="restoreBtn">
      <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 18C12 19 12.25 19.92 12.67 20.74L12 21.35L10.55 20.03C5.4 15.36 2 12.27 2 8.5C2 5.41 4.42 3 7.5 3C9.24 3 10.91 3.81 12 5.08C13.09 3.81 14.76 3 16.5 3C19.58 3 22 5.41 22 8.5C22 9.93 21.5 11.26 20.62 12.61C19.83 12.23 18.94 12 18 12C14.69 12 12 14.69 12 18M19 14H17V17H14V19H17V22H19V19H22V17H19V14Z" /></svg>
      </button>
      <label for="restoreBtn" class="cpanel-button-label">RESTORE</label></div>
      <div>
      <button class="cpanel-button" id="restBtn">
      <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 18C12 19 12.25 19.92 12.67 20.74L12 21.35L10.55 20.03C5.4 15.36 2 12.27 2 8.5C2 5.41 4.42 3 7.5 3C9.24 3 10.91 3.81 12 5.08C13.09 3.81 14.76 3 16.5 3C19.58 3 22 5.41 22 8.5C22 9.93 21.5 11.26 20.62 12.61C19.83 12.23 18.94 12 18 12C14.69 12 12 14.69 12 18M19 14H17V17H14V19H17V22H19V19H22V17H19V14Z" /></svg>
      </button>
      <label for="restBtn" class="cpanel-button-label">REST</label></div>
    </div>
  </div>
  <div id="inputs-cpanel" class="sub-panel">
  <div class="left-column"><div class="box_1by2 setter">
      <label for="spent_power" class="ability_label">POWER<br>SPENT:</label>
      <input type="number" id="spent_power" name="spent_power" class="number-input" min="0" value="0" oninput="handleStatsInputChange(event)">
    </div></div>
  <div class="right-column"><div class="box_1by2 setter">
      <label for="armor_rating" class="ability_label">ARMOR RATING:</label>
      <input type="number" id="armor_rating" name="armor_rating" class="number-input" min="0" value="0" oninput="handleStatsInputChange(event)">
    </div></div>
  </div>
  <div id="outputs-cpanel" class="sub-panel">
    <div class="feedback" id="combat_info"></div>
  </div>
</div>

<div id="character-builder" class="settings-panel build_panel" markdown>
<div class="personal_details_column column" markdown>
<button class="item" id="output">:material-check:</button>
<button class="side_button_rnd itme" id ="saveButton" markdown>:material-content-save:</button>
<button class="side_button_rnd item" id ="loadButton" markdown>:material-file-import-outline:</button>
<button class="side_button_rnd item" id="viewButton">:material-text-search:</button>
<button class="side_button_rnd item" id="connectButton">:material-cloud-check-variant:</button>
<button class="side_button_rnd item" id="trashButton" style="margin-left:auto;">:octicons-trash-24:</button>
<table class="details">
<tr>
<td class="detail_title"><strong>Name:</strong></td>
<td><input type="text" id="name" name="name" oninput="handleStatsInputChange(event)" required></td>
</tr>
<tr>
<td class="detail_title"><strong>Identity:</strong></td>
<td><input type="text" id="identity" name="identity" oninput="handleStatsInputChange(event)" required></td>
</tr>
<tr>
<td class="detail_title"><strong>Weight:</strong></td>
<td><input type="number" id="weight" name="weight" class="number-input small" min="1" oninput="handleStatsInputChange(event)" required>kg
</td>
</tr>
<tr>
<td class="detail_title"><strong>Level:</strong></td>
<td><input type="number" id="level" name="level" class="number-input small" min="1" oninput="handleStatsInputChange(event)" required>
</td>
</tr>
<tr class="stat">
<td class="detail_title"><strong>STRENGTH:</strong></td>
<td><input type="number" id="strength" name="strength" class="number-input small" min="1" oninput="handleStatsInputChange(event)" required></td>
</tr>
<tr class="stat">
<td class="detail_title"><strong>ENDURANCE:</strong></td>
<td><input type="number" id="endurance" name="endurance" class="number-input small" min="1" oninput="handleStatsInputChange(event)" required></td>
</tr>
<tr class="stat">
<td class="detail_title"><strong>AGILITY:</strong></td>
<td><input type="number" id="agility" name="agility" class="number-input small" min="1" oninput="handleStatsInputChange(event)" required></td>
</tr>
<tr class="stat">
<td class="detail_title"><strong>INTELLIGENCE:</strong></td>
<td><input type="number" id="intelligence" name="intelligence" class="number-input small" min="1" oninput="handleStatsInputChange(event)" required></td>
</tr>
<tr class="stat">
<td class="detail_title"><strong>CHARISMA:</strong></td>
<td><input type="number" id="charisma" name="charisma" class="number-input small" min="1" oninput="handleStatsInputChange(event)" required></td>
</tr>
<tr><td class="detail_title"><strong>BACKGROUND:</strong></td>
<td></td></tr>
<tr><td class="overflow-cell"><textarea id="background" name="background" rows="8" cols="43" oninput="handleSubmit(event)"></textarea></td>
<td></td></tr>
<tr><td class="detail_title"><strong>NOTES:</strong></td>
<td></td></tr>
<tr><td class="overflow-cell"><textarea id="notes" name="notes" rows="5" cols="43" oninput="handleSubmit(event)"></textarea></td>
<td></td></tr>
</table>
<div class="bottom-row" markdown>
<label class="materials-checkbox item">
<input type="checkbox" id="useCookie">
<span class="checkbox-box">
<i class="checkbox-icon"></i>
</span>
<span class="checkbox-label">Store Data in Cookie?</span>
</label>
<span class="example square">
<input type="text" id="colorPickerButton" class="coloris instance2 item" value="#ff8c00" data-coloris>
</span>
</div>
</div>
<div class="json_viewer_column column" id="viewerPanel" style="display:none;">

<textarea id="jsonViewer" name="jsonViewer" rows="30" cols="50" oninput="handleSubmit(event)"></textarea>
<h4>NPC/ENCOUNTER SUMMARY</h4>
<textarea id="npcViewer" name="npcViewer" rows="15" cols="50"></textarea>

</div>
<div class="powers_column column" id="build-checkboxes">
<h3> Powers</h3>
<table id="checkboxTable"></table>
<h3> Weaknesses</h3>
<table id="weakCheckboxTable"></table>
<h3> Skills</h3>
<table id="skillCheckboxTable"></table>
<h3> Equipment</h3>
<table id="equipCheckboxTable"></table>
<h3> Magic</h3>
<table id="magicCheckboxTable"></table>
</div>

<!-- The network panel is here, normally hidden -->
<div class="network-panel build_network" id="network-panel" markdown>
<h2>ONLINE PLAY</h2>
<table class="details" >
<tr>
<td class="detail_title"><strong>BROKER:</strong></td>
<td><input type="text" id="broker" name="broker" oninput="handleNetworkChange(event)" required></td>
</tr>
<tr>
<td class="detail_title"><strong>PORT:</strong></td>
<td><input type="text" id="port" name="port" oninput="handleNetworkChange(event)" required></td>
</tr>
<tr>
<td class="detail_title"><strong>USERNAME:</strong></td>
<td><input type="text" id="username" name="username" oninput="handleNetworkChange(event)" required></td>
</tr>
<tr>
<td class="detail_title"><strong>PASSWORD:</strong></td>
<td>
<!-- <input type="text" id="password" name="password"  required> -->
<div class="password-input-container">
<input type="password" id="password" placeholder="Enter password" oninput="handleNetworkChange(event)">
</td>
</tr>
<tr>
<td class="detail_title"><strong>PLAYER_ID:</strong></td>
<td><input type="text" id="player_id" name="player_id" oninput="handleNetworkChange(event)">
</td>
</tr>
<tr>
<td class="detail_title"><strong>GAME_ID:</strong></td>
<td><input type="text" id="game_id" name="game_id" oninput="handleNetworkChange(event)">
</td>
</tr>
<tr>
<td ></td>
<td><button class="md-button" id="joinButton">JOIN</button>
</td>
</tr>
</table>
### STATUS: <span id="con_state"></span>
> Use this panel to join an online encounter. Ensure you are connected to the right MQTT broker. Secure websockets uses port 8084 default. `player_id` and `game_id` are case sensitive.
</div>

</div>

<!-- Name -->
<h1 class="title" id="character_name">–</h1>
<!-- Top Matter -->
<div class="topmatter">
  <div class="left-side">
    <span class="label">IDENTITY:</span>
    <span class="label">LEVEL:</span>
    <span class="label">WEIGHT:</span>
  </div>
  <div class="right-side">
    <span class="label" id="id_out">{identity}</span>
    <span class="label" id="level_out">{level}</span>
    <span class="label"id="weight_out">{weight}</span>
  </div>
</div>
<!-- Fixed Matter -->
<!-- Fixed Matter -->
<div class="fixedmatter">
  <!-- Left Colunmn -->
  <div class="left-fixed"></div>
  <!-- Central 4 columns -->
  <div class="middle-fixed">
    <!-- 1  ATTRIBUTES -->
    <div class="stats_panel">
      <div class="box_1by1 statbox">
        <label for="str_out" class="ability_label">STR:</label>
        <span id="str_out" class="ability_num"></span>
      </div>
      <div class="box_1by1 statbox">
        <label for="end_out" class="ability_label">END:</label>
        <span id="end_out" class="ability_num"></span>
      </div>
      <div class="box_1by1 statbox">
        <label for="agil_out" class="ability_label">AGIL:</label>
        <span id="agil_out" class="ability_num"></span>
      </div>
      <div class="box_1by1 statbox">
        <label for="int_out" class="ability_label">INT:</label>
        <span id="int_out" class="ability_num"></span>
      </div>
      <div class="box_1by1 statbox">
        <label for="cha_out" class="ability_label">CHA:</label>
        <span id="cha_out" class="ability_num"></span>
      </div>
    </div>
    <!-- 2 MAX LIFT & BASE HTH-->
    <div class="data_panel">
      <div class="box_1by2 carry">
          <label class="framed_label">MAX LIFT (KG):</label>
       <span id="carry" class="framed_num"></span>
          <label  class="framed_label">BASE HTH:</label>
          <span id="baseHTHdam"  class="framed_num"></span>
      </div>
      <div class="box_1by1 clear">
        <div>
          <label for="attSMod" class="unframed_label">Melee TH:</label>
          <span id="attSMod" class="unframed_num"></span>
        </div><div>
          <label for="attAMod" class="unframed_label">Ranged TH:</label>
          <span id="attAMod" class="unframed_num"></span>
        </div>
      </div>
      <div class="box_1by1 clear">
        <div>
          <label for="damMod" class="unframed_label">HTH BONUS:</label>
          <span id="damMod" class="unframed_num"></span>
        </div>
        <div>
          <label for="ip_out" class="unframed_label">Gizmos:</label>
          <span id="ip_out" class="unframed_num"></span>
        </div>
      </div>
      <div class="box_1by1 clear">
        <div>
          <label for="reactionMod" class="unframed_label">Vibe:</label>
          <span id="reactionMod" class="unframed_num"></span>
        </div> <div>
          <label for="detect" class="unframed_label">DETECT:</label>
          <span id="detect" class="unframed_num"></span>
        </div>
      </div>
    </div>
    <!-- 3 MOVE COL-->
    <div class="data_panel">
      <div id="move_box" class="box_1by3 move scrollable">
        <label for="movement" class="framed_label">BASE MOVE:</label>
        <span id="movement" class="framed_num move"></span><span class="unframed_label orange">hex/turn</span>
        <!--  -->
      </div>
      <div class="box_1by1 clear">
       
      </div>
      <div class="box_1by1 clear"></div>
    </div>
    <!-- 4 POWER HP COL-->
    <div class="stats_panel">
      <div class="box_1by3 power">
          <label for="powerRemains" class="ability_label">POWER:</label>
          <span id="powerRemains" class="framed_num bold"></span>
          <span id="powerScore" class="fraction dim"></span>
          <label for="hpRemains" class="ability_label ">HP:</label>
          <span id="hpRemains" class="framed_num bold"></span>
          <span id="hpFinal" class="fraction dim"></span>
      </div>
      <div class="box_1by1 clear">
      <div>
      <label for="healRate" class="unframed_label">HEAL:</label>
        <span id="healRate" class="unframed_num"></span>
        </div>
        <div>
          <label for="blocks" class="unframed_label">Blocks:</label>
          <span id="blocks" class="unframed_num"></span>
        </div>
                <div>
          <label for="ticks" class="unframed_label">MAX TICKS:</label>
          <span id="ticks" class="unframed_num"></span>
        </div>
      </div>
    </div>
  </div>
  <!-- Right column -->
  <div class="right-fixed"></div>
</div>

<div class="fixed_admonition" markdown>
### Attacks

<table id="attack_array">
<thead>
<tr>
<th>ATTACK MODE</th>
<th>TYPE</th>
<th>TH</th>
<th>DAM</th>
<th>RNG</th>
<th>PR</th>
</tr>
</thead>
<tbody></tbody>
</table>
</div>
<!-- Defense Matrix -->
<div class="fixed_admonition">
  <h3 class="section-title">Defenses</h3>
  <div class="defenses-container">
  <div class="defense-boxes">
    <div id="SR_box"class="box_1by1 defbox">
    <span class="ability_label">SR:</span>
    <span id="SR_out" class="ability_num">0</span>
  </div>
  <div id="AR_box" class="box_1by1 defbox">
    <span class="ability_label">AR:</span>
    <span id="AR_out" class="ability_num">0</span>
  </div>
    <div id="AC_box" class="box_1by1 defbox">
    <span class="ability_label">AC:</span>
    <span id="AC_out" class="ability_num">0</span>
  </div>
</div>
  <div class="defense-info">
<table class="def-table">
  <tr>
    <td>BASE:</td>
    <td><span id="AC_BASE"></span></td>
  </tr>
  <tr>
    <td>LVL:</td>
    <td><span id="AC_LVL"></span></td>
  </tr>
  <tr>
    <td>EVADE:</td>
    <td><span id="AC_EVADE"></span></td>
  </tr>
  <tr>
    <td>POWERS:</td>
    <td><span id="AC_POWERS"></span></td>
  </tr>
</table>
</div>
</div>
  <label class="materials-checkbox">
    <input type="checkbox" id="evading">
    <span class="checkbox-box"><i class="checkbox-icon"></i></span>
    <span class="checkbox-label">Evading? <strong>(1PR)</strong></span>
  </label>
</div>

<div class="defense-container rounded" markdown>
<div class="defense-column" markdown>
<table id="DC1" class="defense-table">
<tr>
<td class="label-cell">Physical</td>
<td id="imm_physical" class="fixed-cell"></td>
<td id="resist_physical" class="fixed-cell "></td>
<td id="vuln_physical" class="fixed-cell"></td>
</tr>
<tr>
<td class="label-cell">Mental</td>
<td id="imm_mental" class="fixed-cell"></td>
<td id="resist_mental" class="fixed-cell "></td>
<td id="vuln_mental" class="fixed-cell"></td>
</tr>
<tr>
<td class="label-cell">Spiritual</td>
<td id="imm_spiritual" class="fixed-cell"></td>
<td id="resist_spiritual" class="fixed-cell "></td>
<td id="vuln_spiritual" class="fixed-cell"></td>
</tr>
</table>
</div>
<div class="defense-column" markdown>
<table id="DC2" class="defense-table">
  <tr>
    <td class="label-cell">Photonic</td>
    <td id="imm_photonic" class="fixed-cell"></td>
    <td id="resist_photonic" class="fixed-cell "></td>
    <td id="vuln_photonic" class="fixed-cell"></td>
  </tr>
  <tr>
    <td class="label-cell">Electric</td>
    <td id="imm_electric" class="fixed-cell"></td>
    <td id="resist_electric" class="fixed-cell "></td>
    <td id="vuln_electric" class="fixed-cell"></td>
  </tr>
  <tr>
    <td class="label-cell">Magnetic</td>
    <td id="imm_magnetic" class="fixed-cell"></td>
    <td id="resist_magnetic" class="fixed-cell "></td>
    <td id="vuln_magnetic" class="fixed-cell"></td>
  </tr>
</table>
</div>
<div class="defense-column" markdown>
<table id="DC3" class="defense-table">
  <tr>
    <td class="label-cell">Thermal</td>
    <td id="imm_thermal" class="fixed-cell"></td>
    <td id="resist_thermal" class="fixed-cell ">–</td>
    <td id="vuln_thermal" class="fixed-cell"></td>
  </tr>
  <tr>
    <td class="label-cell">Vibration</td>
    <td id="imm_vibration" class="fixed-cell"></td>
    <td id="resist_vibration" class="fixed-cell ">–</td>
    <td id="vuln_vibration" class="fixed-cell"></td>
  </tr>
  <tr>
    <td class="label-cell">Disruption</td>
    <td id="imm_disruption" class="fixed-cell"></td>
    <td id="resist_disruption" class="fixed-cell "></td>
    <td id="vuln_disruption" class="fixed-cell"></td>
  </tr>
</table>
</div>
</div>
<p class="table_footnote" markdown>:material-shield-alert:=**Immune**: critical hit required, :material-dice-d20:{.disadvantage}=**Resist**: attackers have disadvantage, :material-dice-d20:{.advantage}=**Vulnerable**: attackers gain advantage</p>

<div class="fixed_admonition" markdown>
  <!-- ### Powers -->
  <div id="powers-summary" markdown></div>
  <!-- ### Equipment -->
  <div id="equip-summary" markdown></div>
  <!-- ### Skills -->
  <div id="skills-summary" markdown></div>
  <!-- ### Magic -->
  <div id="magic-summary" markdown></div>
  <div id="sidekick-summary" markdown></div>
  <div id="vehicle-summary" markdown></div>
  <!-- ### Weaknesses -->
  <div id="weakness-summary" markdown></div>
  <!-- ### Background -->
  <div id="bkg_out" markdown><h3>Background</h3></div>
  <!-- ### Notes -->
  <div id="notes_out" markdown><h3>Notes</h3></div>
</div>

<div id="flash-overlay"></div>