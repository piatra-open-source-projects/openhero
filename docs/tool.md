---
template: tool.html
hide:
  - navigation
  - toc
---
<h1></h1>
<!-- markdownlint-disable MD033 -->
<!-- These are the buttons arranged around the page -->
<div class="side_button no-1" markdown>
  <button class="side_button_rnd" id ="connectBtn" markdown>:material-plus-network-outline:</button>
</div>
<div class="side_button no-2" markdown>
  <button class="side_button_rnd" id ="addBtn" markdown>:material-account-plus:</button>
</div>

<div class="side_button no-3" markdown>
  <button class="side_button_rnd" id ="npcBtn" markdown>:material-skull-scan:</button>
</div>
<div class="side_button no-5" markdown>
  <button class="side_button_rnd" id ="playBtn" markdown>:material-play-circle-outline:</button>
</div>
<div class="side_button on-1" markdown>
<span class="pool_adv" id="pool_adv"></span>
<span class="tray_adv" markdown>
<button class="tray_button" id="adv_minus" onclick="decrementAdvantage()" markdown>
:material-minus-circle:
</button>
<button class="tray_button" id="adv_plus" onclick="incrementAdvantage()" markdown>
:material-plus-circle:
</button>
</span>
<button class="side_button_rnd" id ="rollButton" onclick="roll_ensemble()" markdown>
:material-dice-d20:
</button>
<span class="tray_disadv" markdown>
<button class="tray_button" id="dis_minus" onclick="decrementDisadvantage()" markdown>
:material-minus-circle:
</button>
<button class="tray_button" id="dis_plus" onclick="incrementDisadvantage()" markdown>
:material-plus-circle:
</button>
</span>
<div class="pool_disadv" id="pool_disadv"></div>
</div>

<div class="setting-panel" markdown>
<!-- The network panel is here, normally hidden -->
<div class="tool-net-panel slider" id="network-panel" markdown>
<!-- Status button -->
<div class="side_button here" markdown>
  <button class="advantage" id ="connected" style="display:none;" markdown>:material-lan-connect:</button>
  <button class="disadvantage" id ="disconnected" style="display:none;" markdown>:material-lan-disconnect:</button>
</div>
<!-- Use cookie checkbox just here for now -->
<label class="materials-checkbox item" style="display:none">
<input type="checkbox" id="useCookie">
<span class="checkbox-box">
<i class="checkbox-icon"></i>
</span>
<span class="checkbox-label">Store Data in Cookie?</span>
</label>
<h3>ONLINE PLAY</h3>

<table class="details" >
<tr>
<td class="detail_title"><strong>BROKER:</strong></td>
<td><input type="text" id="broker" name="broker" oninput="handleNetworkChange(event)" required></td>
</tr>
<tr>
<td class="detail_title"><strong>PORT:</strong></td>
<td><input type="text" id="port" name="port" oninput="handleNetworkChange(event)" required></td>
</tr>
<tr>
<td class="detail_title"><strong>USERNAME:</strong></td>
<td><input type="text" id="username" name="username" oninput="handleNetworkChange(event)" required></td>
</tr>
<tr>
<td class="detail_title"><strong>PASSWORD:</strong></td>
<td>
<!-- <input type="text" id="password" name="password"  required> -->
<div class="password-input-container">
<input type="password" id="password" placeholder="Enter password" oninput="handleNetworkChange(event)">
</td>
</tr>
<tr>
<td class="detail_title">.</td>
<td>
</td>
</tr>
<tr>
<td class="detail_title"><strong>GAME_ID:</strong></td>
<td><input type="text" id="game_id" name="game_id" oninput="handleNetworkChange(event)">
</td>
</tr>
<tr>
<td ></td>
<td><button class="md-button" id="joinButton">START</button>
</td>
</tr>
</table>
### STATUS: <span id="con_state"></span>
<div class="rounded gaps faint" markdown>
Use this panel to start and manage an OpenHero encounter. 

- `game_id`: case-sensitive, unique label of your choosing. 
- Players will need `game_id` to join the encounter.
</div>
</div>

<div class="tool-add-panel slider" id="tool-add-panel" markdown>
### Add PLayers

<div id="warning" class="faint gaps" markdown>
#### Warning

You must be signed in to the MQTT broker in order to add characters.

Click the :material-plus-network-outline: button to start a game.
</div>

<div id="player_list">
Players will appear in a list here as they sign-in to the game broker. Add them to your session manually by clicking the checkbox next to their name
</div>

</div>

<div class="tool-add-panel slider" id="tool-npc-panel" markdown>
### Load/Save Encounter
<button class=" side_button_rnd" id ="loadEncounter" markdown>:material-download:</button>
<button class=" side_button_rnd" id ="saveEncounter" markdown>:material-content-save:</button>
### Add NPC
<div class="encounter-scroll">
<div class="tool-encounter-list" id="encounter_list">
</div>
</div>
</div>


<div class="powers_column" markdown>
<h2 id="timeline-title">GM TOOLS</h2>
<div id="timeline-container"markdown>



This tool is provided to help GameMasters run encounters online, with all player information brought into a central portal to manage in one location.

<div class="rounded gaps" markdown>
### :material-plus-network-outline: Start a MQTT session

There are planty of free brokers around or you can use docker to simply run one on you own home network or computer. Click the :material-plus-network-outline: button to start a session. 

Free brokers may not require a `username` or `password`. The fields are provided for if you wish to manage your own authenticated broker and will be used to sign in.

Note that the MQTT broker session will be held over websocket connection so make sure that the broker is open for those connections over port 8083. You can specify another websocket port if required.
</div>
<div class="rounded gaps" markdown>
### :material-account-plus: Add Players

Add players to the encounter. The **Add Player** panel is toggled by the :material-account-plus: button. Players that have signed into the session on the MQTT broker will appear here.

Ensure that they sign in to the right `game_id`. They should also provide a suitable `player_id` to identify themselves.
</div>
<div class="rounded gaps" markdown>
###  :material-skull-scan: Add NPCs

Clicking the :material-skull-scan: will let you add non=player characters to the encounter.

These can be loaded from JSON file. The JSON format follows the NPC summary format in the JSON viewer panel of the character builder. You can use this to quickly build and add NPCs to your encounters.

Once loaded from your list or selected from the defaults, the NPCs are added to the combatants.

</div>
<div class="rounded gaps" markdown>
### :material-play-circle-outline: Run Encounter

The :material-play-circle-outline: button will start the encounter.
Initiatives are currently handled for  you automatically but it is planned to allow manual rolls for players soon.

</div>
<div class="rounded gaps" markdown>
### Combatant List

On the right hand side you can see a summary of all combatants in the encounter. You can click on any of them to examine their details, attacks and defenses.

</div>
</div>

<div class="between-turns" id="between-turns" markdown>

### Between Turns

- Apply environmental effects and damage
- Pay for maintanence for powers and abilities
- UNCONSCIOUS characters save vs ENDURANCE to rouse
</div>
<div class="summary-box" id="summary" markdown>
<div class="fixed_admonition"  markdown>
<h2><span id="summary_name"></span></h2>
<span class="stat_summary">STR:<span id="STR_out"></span> END:<span id="END_out"></span> AGIL:<span id="AGIL_out"></span> INT:<span id="INT_out"></span> CHA:<span id="CHA_out"></span>
</span>
</div>
<div class="fixed_admonition" markdown>
<div class="defenses-container">
<div class="defense-boxes">
<div id="HP_box"class="box_1by1 defbox">
<span class="ability_label">HP:</span>
<span id="HP_out" class="ability_num">0</span>
</div>
<div id="POW_box"class="box_1by1 defbox">
<span class="ability_label">POW:</span>
<span id="POW_out" class="ability_num">0</span>
</div>
<div id="MV_box" class="box_1by1 defbox">
<span class="ability_label">MV:</span>
<span id="MV_out" class="ability_num">0</span>
</div>
</div>
</div>
<div id="movement-slider-container"></div>
<table id="attack_array">
<thead>
<tr>
<th>ATTACK MODE</th>
<th>TYPE</th>
<th>TH</th>
<th>DAM</th>
<th>RNG</th>
<th>PR</th>
</tr>
</thead>
<tbody></tbody>
</table>
</div>

<div class="fixed_admonition" markdown>
<div class="defenses-container">
<div class="defense-boxes">
<div id="DR_box"class="box_1by1 defbox">
<span class="ability_label">DR:</span>
<span id="DR_out" class="ability_num">0</span>
</div>
<div id="SR_box"class="box_1by1 defbox">
<span class="ability_label">SR:</span>
<span id="SR_out" class="ability_num">0</span>
</div>
<div id="AR_box" class="box_1by1 defbox">
<span class="ability_label">AR:</span>
<span id="AR_out" class="ability_num">0</span>
</div>
<div class="box_1by1 defbox">
<span class="ability_label">AC:</span>
<span id="AC_out" class="ability_num">0</span>
</div>
</div>
</div>

<div class="defense-container rounded" markdown>
<div class="defense-column" markdown>
<table id="DC1" class="defense-table">
<tr>
<td class="label-cell">Physical</td>
<td id="imm_physical" class="fixed-cell"></td>
<td id="resist_physical" class="fixed-cell "></td>
<td id="vuln_physical" class="fixed-cell"></td>
</tr>
<tr>
<td class="label-cell">Mental</td>
<td id="imm_mental" class="fixed-cell"></td>
<td id="resist_mental" class="fixed-cell "></td>
<td id="vuln_mental" class="fixed-cell"></td>
</tr>
<tr>
<td class="label-cell">Spiritual</td>
<td id="imm_spiritual" class="fixed-cell"></td>
<td id="resist_spiritual" class="fixed-cell "></td>
<td id="vuln_spiritual" class="fixed-cell"></td>
</tr>
</table>
</div>
<div class="defense-column" markdown>
<table id="DC2" class="defense-table">
  <tr>
    <td class="label-cell">Photonic</td>
    <td id="imm_photonic" class="fixed-cell"></td>
    <td id="resist_photonic" class="fixed-cell "></td>
    <td id="vuln_photonic" class="fixed-cell"></td>
  </tr>
  <tr>
    <td class="label-cell">Electric</td>
    <td id="imm_electric" class="fixed-cell"></td>
    <td id="resist_electric" class="fixed-cell "></td>
    <td id="vuln_electric" class="fixed-cell"></td>
  </tr>
  <tr>
    <td class="label-cell">Magnetic</td>
    <td id="imm_magnetic" class="fixed-cell"></td>
    <td id="resist_magnetic" class="fixed-cell "></td>
    <td id="vuln_magnetic" class="fixed-cell"></td>
  </tr>
</table>
</div>
<div class="defense-column" markdown>
<table id="DC3" class="defense-table">
  <tr>
    <td class="label-cell">Thermal</td>
    <td id="imm_thermal" class="fixed-cell"></td>
    <td id="resist_thermal" class="fixed-cell ">–</td>
    <td id="vuln_thermal" class="fixed-cell"></td>
  </tr>
  <tr>
    <td class="label-cell">Vibration</td>
    <td id="imm_vibration" class="fixed-cell"></td>
    <td id="resist_vibration" class="fixed-cell ">–</td>
    <td id="vuln_vibration" class="fixed-cell"></td>
  </tr>
  <tr>
    <td class="label-cell">Disruption</td>
    <td id="imm_disruption" class="fixed-cell"></td>
    <td id="resist_disruption" class="fixed-cell "></td>
    <td id="vuln_disruption" class="fixed-cell"></td>
  </tr>
</table>
</div><!-- DC3 -->
</div>
</div>
</div>

</div><!-- Summary -->

<div class="combatants" id="combatants"></div>
<div class="lower-glass"><div id="combat_info"></div></div>

</div>