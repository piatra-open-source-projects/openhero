---
title: OpenHero Rules
icon: material/book-open-page-variant
template: splash.html
hide:
  - footer
---

## What is OpenHero?

OpenHero is an open-source, modular role-playing game framework designed to provide a solid foundation for game designers to build their own campaigns while retaining control over their proprietary content. The framework is free to use and includes a character builder, encounter tools, and support for internationalization and configuration.

**** SPecifically modular

**** Configurable game options to acount for many different systems and styles. Even to the degree of flexible chacarter generation, progression.

## How to Use These Rules

The Game Master (GM) has the final say in all matters concerning the game. Specific rules take precedence over general rules. The rules are intended to facilitate an enjoyable gaming experience and should not be the sole focus of the game. The provided tools are meant to streamline the game-running process. Optional rules are presented, allowing the GM to choose based on the desired style of gameplay.

## Required Materials

To play OpenHero, you will need:

- Dice
- A computer to access the character builder (internet not required, as everything runs in the browser)
- Paper
- Friends to play with
- Imagination

## Design Philosophy

Create a simple to run game

Try to capture somewhat realistic mechanics that scale to the fantastic.

Allow extreme customisation - to allow GMs and game designers to use the framework fro their own creations. Leave the mechanics to us, you create the campaign, the flavour and the secret sauce....

Make it free

Provide resources to help people play it

Build a community to share resources